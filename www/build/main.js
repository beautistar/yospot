webpackJsonp([0],{

/***/ 145:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 145;

/***/ }),

/***/ 146:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_http__ = __webpack_require__(147);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// import { HttpClient, HttpHeaders } from '@angular/common/http';

// import { Http, Response, RequestOptions, Headers } from '@angular/http';

/*
  Generated class for the CustomeHttpServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var HttpService = (function () {
    function HttpService(http) {
        this.http = http;
    }
    // post(url) {
    //   return new Promise((resolve, reject) => {
    //     this.http.get(url, {},{}).then((result: any) => {
    //         result = result.json();
    //         resolve(result)
    //     }, (error: any) => {
    //       reject(error)
    //     })
    //   }) 
    // }
    HttpService.prototype.post = function (url) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(url, {}, {}).then(function (result) {
                resolve(result.data);
            }).catch(function (error) {
                reject(error.error);
            });
        });
    };
    HttpService.prototype.postDeleteFile = function (file) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log("passing file -----> ", file);
            var url = 'http://192.168.1.254/?custom=1&cmd=4003';
            _this.http.get(url, { str: file }, {}).then(function (result) {
                // this.http.get(url, {},{}).then((result: any) => {
                // resolve(result.data)
                resolve(result.data);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    HttpService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_http__["a" /* HTTP */]])
    ], HttpService);
    return HttpService;
}());

// post(url) {
//   return new Promise((resolve, reject) => {
//     // let headers = new Headers();
//     // headers.append('Content-Type', 'application/json');
//     // headers.append('Accept', 'application/json');
//     // headers.append('Access-Control-Allow-Origin', '*');
//     this.http.get(url, {},{}).then((result: any) => {
//       // console.log("Inside http service. API call sucessfull -->", JSON.stringify(result));
//       // if (result.json != undefined ) {
//         result = result.json();
//         resolve(result)
//       // } else {
//       //   reject('API call failed');
//       // }
//     }, (error: any) => {
//       // console.log("Inside http service. API call error");
//       reject(error)
//     })
//   })
// } 
//# sourceMappingURL=http-service.js.map

/***/ }),

/***/ 149:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VideoFeedPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__library_library__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__settings_settings__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_screen_orientation__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_dashcam_service__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_xml2js__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_xml2js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_xml2js__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







// import { ChangeDetectorRef } from '@angular/core';

// import { BackgroundMode } from '@ionic-native/background-mode';
/**
 * Generated class for the VideoFeedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var VideoFeedPage = (function () {
    function VideoFeedPage(navCtrl, navParams, 
        // private cdRef: ChangeDetectorRef, 
        loadingCtrl, screenOrientation, 
        // private backgroundMode: BackgroundMode,       
        platform, dashcamService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.screenOrientation = screenOrientation;
        this.platform = platform;
        this.dashcamService = dashcamService;
        this.videoRecording = false;
        this.muteAudio = false;
        this.cameraMode = 2;
        this.cameraModeText = "Front / Back";
        this.loaderIsPresent = false;
        this.recBlink = true;
        // Camera Settings
        this.camSettings = {
            photo: ' ',
            resolution: ' ',
            audio: 0,
            collision: ' ',
            download: false,
            capacity: 0
        };
        this.myDate = new Date().toISOString();
        setInterval(function () {
            // console.log("rec Blink = ", this.recBlink); 
            if (_this.recBlink == true) {
                _this.recBlink = false;
            }
            else {
                _this.recBlink = true;
            }
        }, 750);
        // detect orientation changes
        this.screenOrientation.onChange().subscribe(function () {
            console.log("Orientation Changed");
        });
        // this.dashcamService.reconnectWifi();
    }
    VideoFeedPage.prototype.ionViewDidLeave = function () {
        console.log("view did leave");
    };
    VideoFeedPage.prototype.ionViewWillEnter = function () {
        // Enable Background mode so App is not paused
        // this.backgroundMode.enable;
        var _this = this;
        //Get camera settings including audio
        this.getCameraSettings();
        // Change Camera Mode to Movie
        console.log("view will enter");
        console.log("ionViewWillEnter - recording status - ", this.dashcamService.videoRecording);
        // if (this.backgroundMode.isActive()) {
        //   console.log("background mode is active")
        // } else {
        //   console.log("background mode inactive");
        // }
        switch (this.dashcamService.videoEntry) {
            case 'new':
                console.log("NEW VIDEO ENTRY --------->", this.dashcamService.videoEntry);
                this.initialize();
                break;
            case 'settings':
                console.log("SETTINGS VIDEO ENTRY --------->", this.dashcamService.videoEntry);
                var load_1 = this.loadingCtrl.create({
                    content: 'Restarting Live Feed...'
                });
                load_1.present();
                this.dashcamService.liveFeedToggle('start').then(function () {
                    console.log("Livefeed restarted");
                    _this.goToToggleRecording();
                    load_1.dismiss();
                }).catch(function (err) {
                    load_1.dismiss();
                    console.log("Live feed start error", JSON.stringify(err));
                });
                break;
            case 'library':
                this.initialize();
                break;
            default:
                this.initialize();
                break;
        }
    };
    VideoFeedPage.prototype.getCameraSettings = function () {
        var _this = this;
        this.dashcamService.getSettings().then(function (data) {
            // console.log("Settings Data ->", JSON.stringify(data));
            _this.parseSettingsXML(data).then(function (cmdArray) {
                // console.log("PARSE ARRAY ->", JSON.stringify(cmdArray));
                cmdArray.forEach(function (item) {
                    switch (item.cmd) {
                        case '1002':
                            _this.camSettings.photo = item.status;
                            break;
                        case '2002':
                            _this.camSettings.resolution = item.status;
                            break;
                        case '2011':
                            _this.camSettings.collision = item.status;
                            break;
                        case '2007':
                            console.log("item status - ", item.status);
                            if (Number(item.status) == 1) {
                                _this.muteAudio = false;
                                console.log("muteAudio is FALSE");
                            }
                            else {
                                _this.muteAudio = true;
                                console.log("muteAudio is TRUE");
                            }
                            break;
                        default:
                            break;
                    }
                    // console.log("Settings Object AFTER -> ", JSON.stringify(this.camSettings));
                }).catch(function (err) { return console.log("XML error /", err); });
                // console.log("Settings Object AFTER -> ", JSON.stringify(this.camSettings));
            });
        }).catch(function (err) { return console.log("Get Settings Error / ", err); });
    };
    VideoFeedPage.prototype.initialize = function () {
        var _this = this;
        // Get the current Video Recording status
        this.dashcamService.currStatus().then(function (data) {
            console.log("Status XML Data -> ", JSON.stringify(data));
            _this.parseXML(data).then(function (status) {
                console.log("Status ->", JSON.stringify(status));
                // If current mode is Movie then check recording status
                if (status == '1') {
                    _this.dashcamService.videoRecording = true;
                }
                else {
                    _this.dashcamService.videoRecording = false;
                }
                if (status == '0') {
                    _this.dashcamService.videoRecording = true;
                    _this.dashcamService.recordVideo(1);
                }
                // If current mode is playback then switch to Movie
                if (status == '3') {
                    var loading_1 = _this.loadingCtrl.create({
                        content: 'Restarting Video...'
                    });
                    loading_1.present();
                    _this.liveFeedUrl = "";
                    _this.dashcamService.changeCameraMode('VIDEO')
                        .then(function (data) {
                        _this.liveFeedUrl = _this.dashcamService.liveFeedUrl;
                        _this.dashcamService.videoRecording = true;
                        _this.dashcamService.recordVideo(1);
                        loading_1.dismiss();
                    }).catch(function (error) { return console.log("Camera Mode Change Error / ", error); });
                }
            });
        }).catch(function (error) { return console.log("Status Error / ", error); });
        this.dashcamService.changeFeedMode(this.cameraMode);
        this.liveFeedUrl = this.dashcamService.liveFeedUrl;
    };
    VideoFeedPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad VideoFeedPage');
        this.platform.ready().then(function () {
            _this.platform.resume.subscribe(function (e) {
                console.log("Resuming in foreground. Restart video.");
                _this.initialize();
            });
            _this.platform.pause.subscribe(function (e) {
                console.log("Going in background. Change to playback mode");
                _this.dashcamService.changeCameraMode('PLAYBACK');
            });
        });
    };
    VideoFeedPage.prototype.goToSettings = function () {
        var _this = this;
        this.dashcamService.videoEntry = 'settings';
        this.dashcamService.recordVideo(0).then(function () {
            _this.dashcamService.videoRecording = false;
            _this.dashcamService.liveFeedToggle('stop').then(function () {
                console.log("Live View stopped so call Settings page");
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_1__settings_settings__["a" /* SettingsPage */]);
            }).catch(function (err) {
                console.log("Live View STOP error", JSON.stringify(err));
            });
        }).catch(function (err) {
            console.log("Recording STOP error", JSON.stringify(err));
        });
    };
    VideoFeedPage.prototype.goToCameraMode = function () {
        if (this.cameraMode == 3) {
            this.cameraMode = 0;
        }
        else {
            this.cameraMode = this.cameraMode + 1;
        }
        switch (this.cameraMode) {
            case 0:
                this.cameraModeText = 'Front';
                break;
            case 1:
                this.cameraModeText = 'Back';
                break;
            case 2:
                this.cameraModeText = 'Front / Back';
                break;
            case 3:
                this.cameraModeText = 'Back / Front';
                break;
            default:
                break;
        }
        this.dashcamService.changeFeedMode(this.cameraMode);
    };
    VideoFeedPage.prototype.goToLibrary = function () {
        console.log("Call Library Page");
        this.dashcamService.videoEntry = 'library';
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_0__library_library__["a" /* LibraryPage */]);
    };
    VideoFeedPage.prototype.goToTakePicture = function () {
        var _this = this;
        // Show the loading indicator
        this.dashcamService.videoEntry = 'picture';
        var loading = this.loadingCtrl.create({
            content: 'Taking Picture...'
        });
        loading.present().then(function (result) {
            _this.loaderIsPresent = true;
        });
        // Call the Picture API
        this.dashcamService.takePicture(this.cameraMode).then(function (data) {
            loading.setContent("Saving File...");
            _this.liveFeedUrl = "";
            _this.dashcamService.changeCameraMode('VIDEO')
                .then(function (data) {
                _this.liveFeedUrl = _this.dashcamService.liveFeedUrl;
                // console.log("Camera Mode Changed to VIDEO sucessfully- ",JSON.stringify(data));
                if (_this.dashcamService.videoRecording) {
                    _this.dashcamService.recordVideo(1).then(function (data) {
                        if (_this.loaderIsPresent) {
                            loading.dismiss();
                            _this.loaderIsPresent = false;
                        }
                    });
                }
                else {
                    if (_this.loaderIsPresent) {
                        loading.dismiss();
                        _this.loaderIsPresent = false;
                    }
                }
            }).catch(function (error) { return console.log("Camera Mode VIDEO Change Error / ", error); });
        });
    };
    VideoFeedPage.prototype.goToFullScreen = function () {
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
    };
    VideoFeedPage.prototype.goToRemoveFullScreen = function () {
        // Change Orientation
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    };
    VideoFeedPage.prototype.gotToNormalScreen = function () {
        this.screenOrientation.unlock();
    };
    VideoFeedPage.prototype.goToToggleRecording = function () {
        if (this.dashcamService.videoRecording) {
            this.dashcamService.recordVideo(0); // Stop Recording
            this.dashcamService.videoRecording = false;
        }
        else {
            this.dashcamService.recordVideo(1); // Start Recording
            this.dashcamService.videoRecording = true;
        }
    };
    VideoFeedPage.prototype.goToToggleAudio = function () {
        if (this.muteAudio) {
            this.dashcamService.muteAudio(1); // UnMute
            this.muteAudio = false;
        }
        else {
            this.dashcamService.muteAudio(0); // Mute
            this.muteAudio = true;
        }
    };
    VideoFeedPage.prototype.parseXML = function (data) {
        return new Promise(function (resolve) {
            // var k,l,
            var 
            // arr    = [],
            parser = new __WEBPACK_IMPORTED_MODULE_6_xml2js___default.a.Parser({
                trim: true,
                explicitArray: true
            });
            parser.parseString(data, function (err, result) {
                var obj = result.Function;
                var val = obj.Value[0];
                console.log("Object -> ", JSON.stringify(obj));
                console.log("value ->", obj.Value[0]);
                resolve(val);
            });
        });
    };
    // Parse CML data for Files and convert into an Array
    VideoFeedPage.prototype.parseSettingsXML = function (data) {
        return new Promise(function (resolve) {
            // var k,l,
            var arr = [], parser = new __WEBPACK_IMPORTED_MODULE_6_xml2js___default.a.Parser({
                trim: true,
                explicitArray: true
            });
            parser.parseString(data, function (err, result) {
                var obj = result.Function;
                // console.log("XML Object -> ", JSON.stringify(obj));
                arr.push({
                    cmd: obj.Cmd[0],
                    status: obj.Status[0],
                });
                arr.push({
                    cmd: obj.Cmd[3],
                    status: obj.Status[3],
                });
                arr.push({
                    cmd: obj.Cmd[13],
                    status: obj.Status[13],
                });
                arr.push({
                    cmd: obj.Cmd[15],
                    status: obj.Status[15],
                });
                // console.log("Array ", JSON.stringify(arr));
                resolve(arr);
            });
        });
    };
    VideoFeedPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["m" /* Component */])({
            selector: 'page-video-feed',template:/*ion-inline-start:"F:\01_Project\2019-04\04_YoSpot\03_Source\YoSpot\src\pages\video-feed\video-feed.html"*/'<!--\n  Generated template for the VideoFeedPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n \n      <ion-navbar >\n        <i navPop class="fa fa-reply back_icon" aria-hidden="true"></i>  \n        <ion-title text-center >\n         Video Movie\n        </ion-title>\n\n      </ion-navbar>\n  </ion-header>\n\n<ion-content class="vidContent" >\n  <!-- <img class="mainFeed" src="http://192.168.1.254:8192" /> -->\n\n  <ion-grid class="grid"> \n    <ion-row class="">\n      <ion-col class="">\n        <h4 class="pipMode col-6" style="    margin-top: 8px;">{{cameraModeText}}  </h4>      \n      </ion-col>\n      <ion-col class="">\n          <ion-label class="col-6" style="color: #796969;"  text-right > {{myDate | date: \'dd/MM/yyyy, HH:mm:ss \'}} </ion-label>\n      </ion-col> \n    </ion-row>\n    <ion-row class="videoRow">\n      <ion-col class="videoCol">\n        <div class="mainVideo">\n          <!-- <img src="https://res.cloudinary.com/indysigner/image/fetch/f_auto,q_auto/w_1600/https://cloud.netlifyusercontent.com/assets/344dbf88-fdf9-42bb-adb4-46f01eedd629/4b1d6da4-a748-4b2e-b39a-d98d842aa976/user-testing-usability-test-circle.png" />             -->\n          <img [src]="liveFeedUrl" />          \n        </div>\n      </ion-col>\n    </ion-row>\n     <img class="yospotTitle" src="./assets/imgs/yospot-title.png" />\n\n    <ion-row class="indicatorRow">\n      <ion-col class="videoCol">\n        <button ion-button clear class="indicatorButton" *ngIf="dashcamService.videoRecording == true && recBlink == true" >\n          <ion-icon style="color: red;" name="radio-button-on">&nbsp;Rec</ion-icon>\n        </button>\n      </ion-col>\n    </ion-row>\n\n    <!-- Small Toolbar -->\n  <ion-card padding class="toolbarCard" >\n    <ion-row class="smallToolBarRow">\n      <ion-col class="smallToolBarCol"> \n        <button ion-button  class="smallButton" clear (click)="goToLibrary()" (tap)="goToLibrary()">\n          <ion-icon name="images" style="color:white; font-size:2em;"></ion-icon>\n        </button>\n      </ion-col>\n      <ion-col class="smallToolBarCol"> \n        <button class="smallButton" ion-button  clear (tap)="goToCameraMode()">\n          <ion-icon style="color:white; font-size:2em;" name="reverse-camera"></ion-icon>\n        </button>\n      </ion-col>\n      <ion-col class="smallToolBarCol"> \n        <button ion-button class="smallButton"  clear (tap)="goToFullScreen()">\n          <ion-icon style="color:white; font-size:2em;" name="expand"></ion-icon>\n         <!--img src="assets/imgs/expand-button.png" /-->\n        </button>\n      </ion-col>\n\n      <ion-col class="smallToolBarCol">\n        <button class="smallButton" ion-button  clear (tap)="goToSettings()">\n          <ion-icon style="color:white; font-size:1.8em;" name="settings"></ion-icon>\n        </button>\n      </ion-col>\n    </ion-row>\n\n    <!-- Large Toolbar -->\n    <ion-row class="largeToolBarRow" align-items-center >\n      <ion-col class="largeToolBarCol" >\n        <button class="largeButton" ion-button (tap)="goToTakePicture()">\n          <ion-icon style="font-size:2em;" name="camera"></ion-icon>\n        </button>\n      </ion-col>\n      <ion-col class="largeToolBarCol" > \n        <button  ion-button class="largeButtonRec" (tap)="goToToggleRecording()">\n          <ion-icon  name="radio-button-on" style="color:white; font-size:6em;" *ngIf="dashcamService.videoRecording == false"></ion-icon>\n          <ion-icon  name="radio-button-on" style="color:red; font-size:6em;" *ngIf="dashcamService.videoRecording == true && recBlink == true"></ion-icon>          \n          <ion-icon  name="radio-button-on" style="color:#212121 ; font-size:6em;" *ngIf="dashcamService.videoRecording == true && recBlink == false"></ion-icon>          \n          \n        </button>\n      </ion-col>\n      <ion-col class="largeToolBarCol" > \n        <button  ion-button class="largeButton"  (tap)="goToToggleAudio()">\n          <!--ion-icon style="font-size:2em;" name="volume-high" *ngIf="muteAudio == false"></ion-icon-->\n          <i class="fa fa-volume-up" style="font-size:2em;" *ngIf="muteAudio == false" aria-hidden="true"></i>\n\n          <ion-icon style="font-size:2em;" name="volume-off" *ngIf="muteAudio == true"></ion-icon>\n        </button>\n      </ion-col>\n    </ion-row>\n\n  </ion-card>\n    <!-- Landscape Toolbar -->\n    <ion-row class="landscapeToolBarRow">\n        <ion-col col-2 offset-1 class="landscapeToolBarCol">\n\n          <button class="landscapeButton" ion-button  clear (tap)="goToLibrary()">\n              <ion-icon name="images"></ion-icon>\n          </button>\n          <button class="landscapeButton" ion-button  clear (tap)="goToCameraMode()" >\n            <ion-icon name="reverse-camera"></ion-icon>\n          </button>\n        </ion-col >\n        <ion-col col-3 offset-2 class="landscapeToolBarCol"> \n          <button class="landscapeButton" ion-button  clear (tap)="goToTakePicture()">\n            <ion-icon name="camera"></ion-icon>\n          </button>\n          <button class="landscapeButton" ion-button  clear (tap)="goToToggleRecording()">\n              <ion-icon name="radio-button-on" style="color:white;" *ngIf="dashcamService.videoRecording == false"></ion-icon>\n              <ion-icon name="radio-button-on" style="color:red;" *ngIf="dashcamService.videoRecording == true && recBlink == true"></ion-icon>    \n              <ion-icon name="radio-button-on" style="color:black;" *ngIf="dashcamService.videoRecording == true && recBlink == false"></ion-icon>    \n              \n            </button>\n          <button class="landscapeButton" ion-button  clear (tap)="goToToggleAudio()">\n              <ion-icon name="volume-up" *ngIf="muteAudio == false"></ion-icon>\n              <ion-icon name="volume-off" *ngIf="muteAudio == true"></ion-icon>\n          </button>\n        </ion-col>\n        <ion-col col-2 offset-2  class="landscapeToolBarCol"> \n          <!-- <button class="landscapeButton" ion-button  clear (tap)="goToTogglePIP()">\n            <ion-icon name="photos"></ion-icon>\n          </button> -->\n          <!-- <button class="landscapeButton" ion-button  clear (tap)="goToCameraMode()" >\n              <ion-icon name="reverse-camera"></ion-icon>\n          </button> -->\n          <button class="landscapeButton" ion-button  clear (tap)="goToRemoveFullScreen()">\n            <ion-icon name="contract"></ion-icon>\n          </button>\n          <button class="landscapeButton" ion-button   clear (tap)="goToSettings()">\n            <ion-icon name="settings"></ion-icon>\n          </button>\n        </ion-col>\n      </ion-row>\n  </ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"F:\01_Project\2019-04\04_YoSpot\03_Source\YoSpot\src\pages\video-feed\video-feed.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["e" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_5__providers_dashcam_service__["a" /* DashcamService */]])
    ], VideoFeedPage);
    return VideoFeedPage;
}());

//# sourceMappingURL=video-feed.js.map

/***/ }),

/***/ 150:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LibraryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__fullscreen_picture_fullscreen_picture__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_dashcam_service__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_photo_library__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_xml2js__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_xml2js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_xml2js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_file_transfer__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_platform_browser__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






// import { IonicSwipeAllModule } from 'ionic-swipe-all';

// import { HTTP } from '@ionic-native/http';


// import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';


/**
 * Generated class for the LibraryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LibraryPage = (function () {
    function LibraryPage(navCtrl, navParams, dashcamService, loadingCtrl, modalCtrl, photoLibrary, 
        // private http: HTTP,
        transfer, file, sanitizer, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dashcamService = dashcamService;
        this.loadingCtrl = loadingCtrl;
        this.modalCtrl = modalCtrl;
        this.photoLibrary = photoLibrary;
        this.transfer = transfer;
        this.file = file;
        this.sanitizer = sanitizer;
        this.viewCtrl = viewCtrl;
        this.videoFiles = [];
        this.videoFilesSource = [];
        this.filterVideoFilesSource = [];
        this.pictureFiles = [];
        this.pictureFilesSource = [];
        this.filterPictureFilesSource = [];
        this.loaderIsPresent = false;
        this.filesType = 'videos';
        console.log("Inside Library Page");
        this.photoLibrary.requestAuthorization().then(function () {
            console.log("Got photo access");
        }).catch(function (err) { return console.log('Photo Gallery permissions weren\'t granted'); });
    }
    LibraryPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.viewCtrl.setBackButtonText('');
        this.loading = this.loadingCtrl.create({
            content: 'Loading Videos...'
        });
        // Android
        this.loading.present().then(function (result) {
            _this.loaderIsPresent = true;
        });
        // change mode to playback and get files
        this.dashcamService.changeCameraMode('PLAYBACK')
            .then(function (data) {
            console.log("Camera Mode Changed to PLAYBACK - ", JSON.stringify(data));
            // alert("camera mode changed to PLAYBACK. Getting Videos");
            _this.getVideoFiles();
        }).catch(function (error) { return console.log("Playback Mode Change Error / ", error); });
    };
    // ionViewDidEnter(){
    //   this.getPictureFiles();
    // }
    LibraryPage.prototype.openCalender = function () {
    };
    LibraryPage.prototype.filter = function (date) {
        var _this = this;
        // if (this.filesType == 'videos') {
        // Filter Videos
        this.videoFiles = [];
        console.log("Date selected ->", __WEBPACK_IMPORTED_MODULE_4_moment__(date).format('DD/MM/YYYY'));
        if (date == null || date == '') {
            this.videoFiles = this.videoFilesSource.slice(0, 5);
            this.currVideoIndex = 5;
            this.videoCount = this.videoFilesSource.length;
        }
        else {
            // console.log(JSON.stringify(this.videoFilesSource));
            this.videoFilesSource = [];
            this.filterVideoFilesSource.forEach(function (video) {
                if (__WEBPACK_IMPORTED_MODULE_4_moment__(date).format('DD/MM/YYYY') == __WEBPACK_IMPORTED_MODULE_4_moment__(video.dateTime).format('DD/MM/YYYY')) {
                    console.log('\nDate Match in File ->', String(__WEBPACK_IMPORTED_MODULE_4_moment__(video.dateTime).format('DD/MM/YYYY')));
                    _this.videoFilesSource.push(_this.sanitizer.bypassSecurityTrustResourceUrl(video));
                }
                // console.log('\nDate in File ->',String(moment(file.dateTime).format('DD/MM/YYYY')));
            });
            this.videoFiles = this.videoFilesSource.slice(0, 5);
            this.currVideoIndex = 5;
            this.videoCount = this.videoFilesSource.length;
            console.log("Found ", this.videoFilesSource.length, " MOVIES");
            // console.log("VIDEO FILES ----->> ", JSON.stringify(this.videoFiles));
        }
        // } else {
        // Filter Pictures
        this.pictureFiles = [];
        console.log("Date selected ->", __WEBPACK_IMPORTED_MODULE_4_moment__(date).format('DD/MM/YYYY'));
        if (date == null || date == '') {
            this.pictureFiles = this.pictureFilesSource.slice(0, 5);
            this.currPictureIndex = 5;
            this.pictureCount = this.pictureFilesSource.length;
        }
        else {
            this.pictureFilesSource = [];
            this.filterPictureFilesSource.forEach(function (picture) {
                if (__WEBPACK_IMPORTED_MODULE_4_moment__(date).format('DD/MM/YYYY') == __WEBPACK_IMPORTED_MODULE_4_moment__(picture.dateTime).format('DD/MM/YYYY')) {
                    console.log('\nDate Match in File ->', String(__WEBPACK_IMPORTED_MODULE_4_moment__(picture.dateTime).format('DD/MM/YYYY')));
                    _this.pictureFilesSource.push(picture);
                }
                _this.pictureFiles = _this.pictureFilesSource.slice(0, 5);
                _this.currPictureIndex = 5;
                _this.pictureCount = _this.pictureFilesSource.length;
                console.log("Found ", _this.pictureFilesSource.length, " PICTURES");
                // console.log("PICTURE FILES ----->> ", this.videoFiles);
                // console.log('\nDate in File ->',String(moment(file.dateTime).format('DD/MM/YYYY')));
            });
        }
        // }
    };
    LibraryPage.prototype.showPicture = function (filename, picturepath) {
        console.log("show picture triggered - ", filename, ' / ', picturepath);
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_0__fullscreen_picture_fullscreen_picture__["a" /* FullscreenPicturePage */], { filename: filename, picturepath: picturepath });
        modal.present();
    };
    LibraryPage.prototype.prev5 = function () {
        console.log("Prev 5 called");
        // If date search is active then disable PREV button
        // if (this.searchDate != null && this.searchDate != '') {
        //   return
        // }
        if (this.filesType == 'videos') {
            console.log("Curr Index - ", this.currVideoIndex);
            console.log("Total Video Files - ", this.videoCount);
            if (this.currVideoIndex > 5) {
                var newIndex = this.currVideoIndex - 5;
                var fromIndex = newIndex - 5;
                if (fromIndex < 0) {
                    fromIndex = 0;
                    newIndex = 5;
                }
                this.videoFiles = this.videoFilesSource.slice(fromIndex, newIndex);
                this.currVideoIndex = newIndex;
                console.log("New Curr Index - ", this.currVideoIndex, "\n\n");
            }
            else {
                alert("Reached Beginning of Videos");
            }
        }
        else {
            console.log("Curr Index - ", this.currPictureIndex);
            console.log("Total Picture Files - ", this.pictureCount);
            if (this.currPictureIndex > 5) {
                var newIndex = this.currPictureIndex - 5;
                var fromIndex = newIndex - 5;
                if (fromIndex < 0) {
                    fromIndex = 0;
                    newIndex = 5;
                }
                this.pictureFiles = this.pictureFilesSource.slice(fromIndex, newIndex);
                this.currPictureIndex = newIndex;
                console.log("New Curr Index - ", this.currPictureIndex, "\n\n");
            }
            else {
                alert("Reached Beginning of Pictures");
            }
        }
    };
    // resetSearchBar() {
    // }
    LibraryPage.prototype.next5 = function () {
        console.log("Next 5 called");
        // If date search is active then disable NEXT button
        // if (this.searchDate != null && this.searchDate != '') {
        //   return
        // }
        if (this.filesType == 'videos') {
            console.log("Curr Index - ", this.currVideoIndex);
            console.log("Total Video Files - ", this.videoCount);
            if (this.currVideoIndex < this.videoCount) {
                var newIndex = this.currVideoIndex + 5;
                this.videoFiles = this.videoFilesSource.slice(this.currVideoIndex, newIndex);
                this.currVideoIndex = newIndex;
                console.log("New Curr Index - ", this.currVideoIndex, "\n\n");
            }
            else {
                alert("Reached End of Videos");
            }
        }
        else {
            console.log("Curr Index - ", this.currPictureIndex);
            console.log("Total Picture Files - ", this.pictureCount);
            if (this.currPictureIndex < this.pictureCount) {
                var newIndex = this.currPictureIndex + 5;
                this.pictureFiles = this.pictureFilesSource.slice(this.currPictureIndex, newIndex);
                this.currPictureIndex = newIndex;
                console.log("New Curr Index - ", this.currPictureIndex, "\n\n");
            }
            else {
                alert("Reached End of Pictures");
            }
        }
    };
    LibraryPage.prototype.getVideoFiles = function () {
        var _this = this;
        // Get an XML list of files
        // Android
        if (!this.loaderIsPresent) {
            this.loading.present().then(function (result) {
                _this.loaderIsPresent = true;
            });
        }
        this.videoFilesSource = [];
        this.videoFiles = [];
        this.dashcamService.fileList().then(function (mediaFilesXml) {
            _this.parseXML(mediaFilesXml)
                .then(function (filesArray) {
                filesArray.forEach(function (file) {
                    // console.log(file, '  ||  ');
                    var thumbPath = 'http://192.168.1.254/NOVATEK/MOVIE/' + file.name + '?custom=1&cmd=4001';
                    var picturePath = 'http://192.168.1.254/NOVATEK/MOVIE/' + file.name + '?custom=1&cmd=4002';
                    var fileName = file.name;
                    // let filePath = file.path;
                    var dateTime = file.time;
                    var downloadPath = 'http://192.168.1.254/NOVATEK/MOVIE/' + file.name;
                    if (fileName.search("MP4") > 0) {
                        // console.log("MOVIE file - ", fileName, " | Date Time - ", dateTime);            
                        _this.videoFilesSource.push({ "name": fileName, "downloadPath": downloadPath, "thumbPath": thumbPath, "picturePath": picturePath, "dateTime": dateTime, "filePath": file.path });
                    }
                });
                // console.log('<-------------- Library Video File List Success BEFORE SORT -------------->');
                // console.log('BEFORE',JSON.stringify(this.videoFilesSource));
                _this.videoFilesSource.sort(function (a, b) {
                    if (a.dateTime > b.dateTime) {
                        return -1;
                    }
                    else if (a.dateTime < b.dateTime) {
                        return 1;
                    }
                    else {
                        return 0;
                    }
                });
                // console.log('<-------------- Library Video File List Success AFTER SORT -------------->');
                // console.log('AFTER',JSON.stringify(this.videoFilesSource));
                _this.videoCount = _this.videoFilesSource.length;
                // this.videoFiles = this.videoFilesSource.slice(0, 50);
                // console.log('Display MOVIE Count',this.videoFiles.length);
                _this.filterVideoFilesSource = _this.videoFilesSource;
                _this.videoFiles = _this.videoFilesSource.slice(0, 5);
                console.log('Souce MOVIE Count', _this.videoFilesSource.length);
                console.log('Filter MOVIE Count', _this.filterVideoFilesSource.length);
                console.log('Display MOVIE Count', _this.videoFiles.length);
                // alert("Display Movie Count " + this.videoFiles.length);
                // alert("VIDEOS -->" + JSON.stringify(this.videoFiles));
                _this.currVideoIndex = 5;
                _this.getPictureFiles();
            });
        }).catch(function (error) {
            console.log("Library File List Error / ", error);
        });
    };
    LibraryPage.prototype.getPictureFiles = function () {
        var _this = this;
        // Get an XML list of files
        this.searchDate = '';
        this.pictureFilesSource = [];
        this.pictureFiles = [];
        this.dashcamService.fileList().then(function (mediaFilesXml) {
            _this.parseXML(mediaFilesXml)
                .then(function (filesArray) {
                filesArray.forEach(function (file) {
                    var thumbPath = 'http://192.168.1.254/NOVATEK/PHOTO/' + file.name + '?custom=1&cmd=4001';
                    var picturePath = 'http://192.168.1.254/NOVATEK/PHOTO/' + file.name + '?custom=1&cmd=4002';
                    var fileName = file.name;
                    var dateTime = file.time;
                    if (fileName.search("JPG") > 0) {
                        // console.log("PICTURE file - ", fileName, " | Date Time - ", dateTime);
                        _this.pictureFilesSource.push({ "name": fileName, "thumbPath": thumbPath, "picturePath": picturePath, "dateTime": dateTime, "filePath": file.path });
                        // this.pictureFiles.push({"name":fileName, "downloadPath": downloadPath, "thumbPath": thumbPath, "picturePath": picturePath, "dateTime": dateTime});    
                    }
                });
                console.log('<-------------- Library Picture File List Success -------------->');
                console.log('Picture Source ----->', _this.pictureFilesSource.length);
                _this.pictureFilesSource.sort(function (a, b) {
                    if (a.dateTime > b.dateTime) {
                        return -1;
                    }
                    else if (a.dateTime < b.dateTime) {
                        return 1;
                    }
                    else {
                        return 0;
                    }
                });
                _this.filterPictureFilesSource = _this.pictureFilesSource;
                _this.pictureFiles = _this.pictureFilesSource.slice(0, 5);
                _this.currPictureIndex = 5;
                _this.pictureCount = _this.pictureFilesSource.length;
                // alert("Display Picture Count " + this.videoFiles.length);
                if (_this.loaderIsPresent) {
                    _this.loading.dismiss();
                    _this.loaderIsPresent = false;
                }
            });
        }).catch(function (error) {
            console.log("Library File List Error / ", error);
        });
    };
    LibraryPage.prototype.deleteFile = function (mode, filePath) {
        var _this = this;
        console.log("Delete File ->", filePath);
        // let fileUrl =  "http://192.168.1.254/?custom=1&cmd=4003&str=" + filePath;
        // A:\\Novatek\\Movie\\" + fileName;
        // console.log("FILE TO DELETE -> ", fileUrl);
        this.dashcamService.deleteFile(filePath).then(function (data) {
            // this.post2(fileUrl).then((data: any) =>{
            console.log("File deleted sucessfully - ", JSON.stringify(data));
            if (mode == "video") {
                _this.getVideoFiles();
            }
            else if (mode == "picture") {
                _this.getPictureFiles();
            }
        }).catch(function (error) {
            console.log("File DELETE Error / ", JSON.stringify(error));
            // alert(JSON.stringify(error))
        });
    };
    LibraryPage.prototype.downloadFile = function (mode, fileName, fileUrl) {
        var _this = this;
        var fileTransfer;
        var downloading = this.loadingCtrl.create({
            content: 'Downloading Please wait...'
        });
        downloading.present();
        // console.log("mode -> ", mode);
        console.log("Download File Name ->", fileName);
        console.log("Download File URL ->", fileUrl);
        fileTransfer = this.transfer.create();
        var newFile = this.file.dataDirectory + fileName;
        console.log("new file -> ", newFile);
        fileTransfer.download(fileUrl, newFile).then(function (entry) {
            console.log('download complete: ' + entry.toURL());
            if (mode == "video") {
                _this.photoLibrary.saveVideo(entry.toURL(), 'YoSpot').then(function (data) {
                    console.log("Video download to Album successfull - ", JSON.stringify(data));
                    downloading.dismiss();
                }).catch(function (error) {
                    console.log("Video download to Album error / ", JSON.stringify(error));
                    downloading.dismiss();
                });
            }
            else {
                _this.photoLibrary.saveImage(entry.toURL(), 'YoSpot').then(function (data) {
                    console.log("Picture download to Album successfull - ", JSON.stringify(data));
                    downloading.dismiss();
                }).catch(function (error) {
                    console.log("Picture download to Album error / ", JSON.stringify(error));
                    downloading.dismiss();
                });
            }
        }, function (error) {
            // handle error
            downloading.dismiss();
            console.log("File download error", JSON.stringify(error));
        });
    };
    // Parse CML data for Files and convert into an Array
    LibraryPage.prototype.parseXML = function (data) {
        return new Promise(function (resolve) {
            var k, l, arr = [], parser = new __WEBPACK_IMPORTED_MODULE_7_xml2js___default.a.Parser({
                trim: true,
                explicitArray: true
            });
            parser.parseString(data, function (err, result) {
                var obj = result.LIST;
                for (k in obj.ALLFile) {
                    var item = obj.ALLFile[k];
                    // console.log("Item ->", item);
                    for (l in item.File) {
                        var fl = item.File[l];
                        // console.log("FL ->", JSON.stringify(fl.NAME[0]));
                        // console.log("FL ->", JSON.stringify(fl.FPATH[0]));
                        arr.push({
                            name: fl.NAME[0],
                            path: fl.FPATH[0],
                            time: fl.TIME[0]
                        });
                    }
                }
                resolve(arr);
            });
        });
    };
    LibraryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LibraryPage');
    };
    LibraryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-library',template:/*ion-inline-start:"F:\01_Project\2019-04\04_YoSpot\03_Source\YoSpot\src\pages\library\library.html"*/'<!--\n  Generated template for the LibraryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <meta http-equiv="Content-Security-Policy" content="default-src *; style-src * \'unsafe-inline\'; script-src * \'unsafe-inline\' \'unsafe-eval\'; media-src *; img-src * filesystem: data:"> -->\n\n<ion-header>\n  <ion-navbar class="ion-text-center">\n    <i navPop class="fa fa-reply back_icon" aria-hidden="true"></i>  \n    <ion-title text-center> Library</ion-title>\n    <!-- <ion-buttons *ngIf="searchDate == null" style="padding-top: 0px; padding-bottom: 0px" end > -->\n    <!--ion-buttons style="padding-top: 0px; padding-bottom: 0px" end >\n        <button class="numBtn"  ion-button solid (click)="prev5()">\n            <ion-icon name="arrow-back" right>&nbsp;5</ion-icon>\n        </button>\n        &nbsp;\n        <button  class="numBtn"  ion-button solid (click)="next5()">\n            <ion-icon name="arrow-forward">&nbsp;5</ion-icon>\n        </button>\n        &nbsp;\n    </ion-buttons-->\n  </ion-navbar>\n    <div style="background-color: #fe684e; padding-left: .5em;padding-right: .5em; padding-top: 0em; padding-bottom: 1em;" >\n    <ion-item style="color: black; background-color: white; margin: auto;border-radius: 2em;">\n        <ion-datetime displayFormat="YYYY-MM-DD" pickerFormat="MMMM DD YYYY" [(ngModel)]="searchDate" (ngModelChange)="filter(searchDate)"></ion-datetime>\n        <ion-label class="cal_parent">  \n            <!--ion-icon style="border-radius: 1em;" class="calnder_style" name="calendar"></ion-icon--> \n         \n            <i class="fa fa-calendar calnder_style" aria-hidden="true"></i>\n\n        </ion-label>\n       \n    </ion-item>\n    </div>\n</ion-header>\n\n<ion-content no-padding class="libContent">\n <div [ngSwitch]="filesType" no-padding class="no-gap">\n    <div *ngSwitchCase="\'videos\'" no-padding class="switch" >\n        <ion-list class="list" no-lines no-padding *ngFor="let video of videoFiles">  \n        <!-- <ion-list class="list" no-lines no-padding [virtualScroll]="videoFiles">   -->\n            <!-- <ion-item-sliding *virtualItem="let video"> -->\n            <ion-item-sliding>                \n                <ion-item class="item" no-padding >\n                    <!-- <p style="font-size: 6px">{{video.downloadPath}}</p>\n                    <p style="font-size: 6px">{{video.thumbPath}}</p> -->\n                    <video controls="controls" preload="none" webkit-playsinline="webkit-playsinline" class="videoPlayer" poster={{video.thumbPath}}>\n                        <source src={{video.downloadPath}} type="video/mp4" />\n                        \n                    </video> \n                </ion-item>\n                <ion-item-options side="right">\n                    <button ion-button expandable (click)="deleteFile(\'video\', video.filePath)">\n                        <ion-icon name="trash"></ion-icon>\n                    </button>\n                    <button ion-button expandable (click)="downloadFile(\'video\', video.name, video.downloadPath)">\n                        <ion-icon name="download"></ion-icon>\n                    </button>\n                </ion-item-options>\n            </ion-item-sliding>\n        </ion-list> \n    </div>\n    \n    <div *ngSwitchCase="\'pictures\'" no-padding  class="no-gap">\n        <ion-list class="list no-gap" no-lines *ngFor="let picture of pictureFiles">                             \n        <!-- <ion-list class="list no-gap" no-lines [virtualScroll]="pictureFiles"> -->\n            <!-- <ion-item-sliding *virtualItem="let picture">  -->\n            <ion-item-sliding>            \n                <ion-item class="no-gap">\n                    <!-- <p style="font-size: 6px">{{picture.thumbPath}}</p>\n                    <p style="font-size: 6px">{{picture.picturePath}}</p> -->\n                    <!-- <img  src="http://192.168.1.254/novatek/photo/2018_0322_043352_004.JPG"  (click)="showPicture(picture.name, picture.picturePath)" tappable/>                     -->\n                    <!-- <img class="picturePlayer" src={{picture.picturePath}}  (click)="showPicture(picture.name, picture.picturePath)" tappable/>                     -->\n                    \n                    <img class="picturePlayer" src={{picture.thumbPath}}  (click)="showPicture(picture.name, picture.picturePath)" tappable/>\n                </ion-item>\n                <ion-item-options side="right">\n                    <button large ion-button expandable (click)="deleteFile(\'picture\', picture.filePath)">\n                        <ion-icon name="trash"></ion-icon>\n                    </button>\n                    <button large ion-button expandable (click)="downloadFile(\'picture\', picture.name, picture.picturePath)">\n                        <ion-icon name="download"></ion-icon>\n                    </button>\n                </ion-item-options>\n            </ion-item-sliding>\n        </ion-list> \n    </div>\n</div>\n</ion-content>\n<ion-footer  >\n    <div no-padding  >\n        <ion-segment class="segment" [(ngModel)]="filesType">\n            <ion-segment-button class="segmentBtn" value="videos">\n                <!-- <ion-icon name="md-arrow-dropdown" item-end></ion-icon>                     -->\n                <!-- Videos -->\n                <!--ion-icon  class="tab_icon"  name="film">&nbsp;</ion-icon-->\n                <i class="fa fa-file-video-o tab_icon" style="    margin-top: 8px;" aria-hidden="true"></i>&nbsp; Videos\n            </ion-segment-button>\n            <ion-segment-button class="segmentBtn" value="pictures">\n                <ion-icon class="tab_icon" name="images">&nbsp;</ion-icon> Photos\n            </ion-segment-button>\n        </ion-segment>\n    </div>\n</ion-footer>\n'/*ion-inline-end:"F:\01_Project\2019-04\04_YoSpot\03_Source\YoSpot\src\pages\library\library.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_dashcam_service__["a" /* DashcamService */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_photo_library__["a" /* PhotoLibrary */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_9__angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* ViewController */]])
    ], LibraryPage);
    return LibraryPage;
}());

//# sourceMappingURL=library.js.map

/***/ }),

/***/ 151:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FullscreenPicturePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the FullscreenPicturePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FullscreenPicturePage = (function () {
    function FullscreenPicturePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.picturePath = this.navParams.get('picturepath');
        this.fileName = this.navParams.get('filename');
        // this.fileName = '2018_200_12_233.jpg';
        // this.picturePath = '/assets/icon/icon.png';
    }
    FullscreenPicturePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FullscreenPicturePage');
    };
    FullscreenPicturePage.prototype.cancel = function () {
        this.navCtrl.pop();
    };
    FullscreenPicturePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-fullscreen-picture',template:/*ion-inline-start:"F:\01_Project\2019-04\04_YoSpot\03_Source\YoSpot\src\pages\fullscreen-picture\fullscreen-picture.html"*/'<!--\n  Generated template for the FullscreenPicturePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-buttons style="padding-top: 0px; padding-bottom: 0px" start >\n        <button ion-button (click)="cancel()">\n            <ion-icon name="arrow-back" left></ion-icon>\n        </button>\n    </ion-buttons>  \n\n    <ion-title>{{fileName}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding style="background-color: black;">\n  <img class="picture" src={{picturePath}} />\n\n</ion-content>\n'/*ion-inline-end:"F:\01_Project\2019-04\04_YoSpot\03_Source\YoSpot\src\pages\fullscreen-picture\fullscreen-picture.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], FullscreenPicturePage);
    return FullscreenPicturePage;
}());

//# sourceMappingURL=fullscreen-picture.js.map

/***/ }),

/***/ 193:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 193;

/***/ }),

/***/ 373:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_utility_service__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_dashcam_service__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_barcode_scanner__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_xml2js__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_xml2js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_xml2js__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






// import { VideoFeedPage } from './../video-feed/video-feed';


/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SettingsPage = (function () {
    // camSettings: {
    // photo: String = '';
    // resolution: String = '';
    // audio: Number = 0;
    // collision: String = '';
    // downloadFlag = false;
    // capacity = 0;
    // }
    function SettingsPage(navCtrl, viewCtrl, navParams, dashcamService, utilityService, barcode, storage, alertCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.dashcamService = dashcamService;
        this.utilityService = utilityService;
        this.barcode = barcode;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.scanInProgress = false;
        // Wi Fi setting
        this.wifi = ''; // 'WIFI_CARDV77fc';
        this.password = ''; //'12345678';
        this.loaderIsPresent = false;
        // Camera Settings
        this.camSettings = {
            photo: ' ',
            resolution: ' ',
            audio: 0,
            collision: ' ',
            download: false,
            capacity: 0,
            recordingTime: ' ',
            movieDate: 0,
        };
    }
    SettingsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        console.log("Inside Setting Page");
        // console.log("Settings Object BEFORE -> ", JSON.stringify(this.camSettings));
        // console.log("BEFORE this.wifi ->", this.wifi);
        // console.log("BEFORE this.password ->", this.password);  
        this.viewCtrl.setBackButtonText('');
        // Get current Camera Settings
        this.dashcamService.getSettings().then(function (data) {
            console.log("Settings Data ->", JSON.stringify(data));
            _this.parseSettingsXML(data).then(function (cmdArray) {
                console.log("PARSE ARRAY ->", JSON.stringify(cmdArray));
                cmdArray.forEach(function (item) {
                    switch (item.cmd) {
                        case '1002':
                            console.log("1002 photo  - ", item.status);
                            _this.camSettings.photo = item.status;
                            break;
                        case '2002':
                            console.log("2002 resolution - ", item.status);
                            _this.camSettings.resolution = item.status;
                            break;
                        case '2011':
                            console.log("2011 collision status - ", item.status);
                            _this.camSettings.collision = item.status;
                            break;
                        case '2007':
                            console.log("2007 audio status - ", item.status);
                            _this.camSettings.audio = Number(item.status);
                            break;
                        case '2003':
                            console.log("2003 recording status - ", item.status);
                            _this.camSettings.recordingTime = item.status;
                            break;
                        case '2008':
                            console.log("2008 movie date - ", item.status);
                            _this.camSettings.movieDate = Number(item.status);
                            break;
                        default:
                            break;
                    }
                    // console.log("Settings Object AFTER -> ", JSON.stringify(this.camSettings));
                }).catch(function (err) { return console.log("XML error /", err); });
                // console.log("Settings Object AFTER -> ", JSON.stringify(this.camSettings));
            });
        }).catch(function (err) { return console.log("Get Settings Error / ", err); });
    };
    SettingsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SettingsPage');
    };
    SettingsPage.prototype.qrScan = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.scanInProgress) {
                            return [2 /*return*/];
                        }
                        else {
                            this.scanInProgress = true;
                        }
                        this.options = {
                            prompt: 'Scan code for info',
                            preferFrontCamera: false,
                            showTorchButton: true,
                            disableSuccessBeep: true,
                            disableAnimations: false
                        };
                        return [4 /*yield*/, this.barcode.scan(this.options).then(function (barcodeData) {
                                // Success! Barcode data is here
                                _this.scanInProgress = false;
                                console.log('barcodeData text - ', JSON.stringify(barcodeData.text));
                                if (barcodeData.text == null || barcodeData.text == "") {
                                }
                                else {
                                    console.log('Connect to WiFi');
                                    // this.connect();
                                    alert(barcodeData.text);
                                }
                            }, function (err) {
                                // An error occurred
                                _this.scanInProgress = false;
                                alert(JSON.stringify(err));
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    SettingsPage.prototype.connect = function () {
        return __awaiter(this, void 0, void 0, function () {
            var loading, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        loading = this.loadingCtrl.create({
                            content: 'Connecting...'
                        });
                        // Present loader
                        loading.present();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        // Call Connection method
                        return [4 /*yield*/, this.connectToCamera()];
                    case 2:
                        // Call Connection method
                        _a.sent();
                        return [4 /*yield*/, this.timeout(12000)];
                    case 3:
                        _a.sent();
                        // this.storage.set('wifi',this.wifi);
                        // this.storage.set('password',this.password);
                        // Check if connection established
                        loading.setContent("Checking Connection...");
                        // navigate to Video feed page
                        loading.dismiss();
                        // this.navCtrl.push(VideoFeedPage);
                        this.navCtrl.pop();
                        return [3 /*break*/, 5];
                    case 4:
                        error_1 = _a.sent();
                        loading.dismiss();
                        console.log('WiFi Error / ', error_1.message);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    SettingsPage.prototype.connectToCamera = function () {
        return __awaiter(this, void 0, void 0, function () {
            var connectingWiFi, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        console.log("Connecting WiFi");
                        return [4 /*yield*/, WifiWizard2.iOSConnectNetworkAsync(this.wifi, this.password)];
                    case 1:
                        connectingWiFi = _a.sent();
                        console.log("Connecting WiFi - Success ->", connectingWiFi);
                        return [2 /*return*/, true];
                    case 2:
                        e_1 = _a.sent();
                        throw new Error("Connecting Wifi. Failed to connect " + this.wifi);
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SettingsPage.prototype.setResolution = function () {
        var _this = this;
        console.log('setResolution triggered - ', this.camSettings.resolution);
        // let resolution: any;
        var alert = this.alertCtrl.create();
        alert.setTitle('Resolution Setting');
        var checked0 = false, checked1 = false, checked2 = false, checked3 = false;
        switch (this.camSettings.resolution) {
            case '0':
                checked0 = true;
                break;
            case '1':
                checked1 = true;
                break;
            case '2':
                checked2 = true;
                break;
            case '3':
                checked3 = true;
                break;
            default:
                break;
        }
        alert.addInput({
            type: 'radio',
            label: '1920 x 1080',
            value: '0',
            checked: checked0
        });
        alert.addInput({
            type: 'radio',
            label: '1280 x 720',
            value: '1',
            checked: checked1
        });
        alert.addInput({
            type: 'radio',
            label: '854 x 480',
            value: '2',
            checked: checked2
        });
        alert.addInput({
            type: 'radio',
            label: '640 x 480',
            value: '3',
            checked: checked3
        });
        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: function (data) {
                // this.testRadioOpen = false;
                console.log("Resolution Ok -> ", data);
                if (data != '') {
                    _this.camSettings.resolution = data;
                    _this.dashcamService.setResolution(data).then(function (resp) {
                        console.log("Resolution updated - ", JSON.stringify(resp));
                    }).catch(function (err) {
                        console.log("Resolution update error - ", JSON.stringify(err));
                    });
                }
            }
        });
        alert.present();
    };
    SettingsPage.prototype.setPhotoQuality = function () {
        var _this = this;
        console.log('setPhotoQuality triggered, photo -> ', this.camSettings.photo);
        var alert = this.alertCtrl.create();
        alert.setTitle('Picture Quality');
        var checked0 = false, checked1 = false, checked2 = false, checked3 = false, checked4 = false, checked5 = false;
        switch (this.camSettings.photo) {
            case '0':
                checked0 = true;
                break;
            case '1':
                checked1 = true;
                break;
            case '2':
                checked2 = true;
                break;
            case '3':
                checked3 = true;
                break;
            case '4':
                checked4 = true;
                break;
            case '5':
                checked5 = true;
                break;
            default:
                break;
        }
        alert.addInput({
            type: 'radio',
            label: '12M',
            value: '0',
            checked: checked0
        });
        alert.addInput({
            type: 'radio',
            label: '10M',
            value: '1',
            checked: checked1
        });
        alert.addInput({
            type: 'radio',
            label: '8M',
            value: '2',
            checked: checked2
        });
        alert.addInput({
            type: 'radio',
            label: '5M',
            value: '3',
            checked: checked3
        });
        alert.addInput({
            type: 'radio',
            label: '3M',
            value: '4',
            checked: checked4
        });
        alert.addInput({
            type: 'radio',
            label: 'VGA',
            value: '5',
            checked: checked5
        });
        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: function (data) {
                // this.testRadioOpen = false;
                console.log("Picture Quality Ok -> ", data);
                if (data != '') {
                    _this.camSettings.photo = data;
                    _this.dashcamService.setPhotoQuality(data).then(function (resp) {
                        console.log("Photo Quality updated - ", JSON.stringify(resp));
                    }).catch(function (err) {
                        console.log("Photo Quality update error - ", JSON.stringify(err));
                    });
                }
            }
        });
        alert.present();
    };
    // setRecordingTime() {
    //   console.log('setRecordingTime triggered');
    //   this.dashcamService.movieCapacity()
    //   .then((movieCapacityData) => {
    //     console.log('Video Capacity -> ', movieCapacityData);
    //     this.parseXML(movieCapacityData).then((movieCapacity: any)=> {
    //       let mcap = movieCapacity / 3600;
    //       let mcapStr = mcap.toFixed(2);
    //       let msg = mcapStr + ' Hrs';
    //       alert(msg)
    //     }).catch ((error) => {
    //       console.log('Video Capacity Parse Error / ', error);      
    //     })
    //   }).catch ((error) => {
    //     console.log('Movie Capacity Error / ', error); 
    //   })
    // }
    SettingsPage.prototype.setRecordingTime = function (parm) {
        var _this = this;
        console.log('setRecordingTime triggered');
        var alert = this.alertCtrl.create();
        alert.setTitle('Recording Time');
        var checked0 = false, checked1 = false, checked2 = false, checked3 = false, checked4 = false;
        switch (this.camSettings.recordingTime) {
            case '0':
                checked0 = true;
                break;
            case '1':
                checked1 = true;
                break;
            case '2':
                checked2 = true;
                break;
            case '3':
                checked3 = true;
                break;
            case '4':
                checked4 = true;
                break;
            default:
                break;
        }
        alert.addInput({
            type: 'radio',
            label: 'Off',
            value: '0',
            checked: checked0
        });
        alert.addInput({
            type: 'radio',
            label: '3 Min',
            value: '1',
            checked: checked1
        });
        alert.addInput({
            type: 'radio',
            label: '5 Min',
            value: '2',
            checked: checked2
        });
        alert.addInput({
            type: 'radio',
            label: '10 Min',
            value: '3',
            checked: checked3
        });
        alert.addInput({
            type: 'radio',
            label: 'Max',
            value: '4',
            checked: checked4
        });
        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: function (data) {
                console.log("Recording Time Ok -> ", data);
                if (data != '') {
                    _this.camSettings.recordingTime = data;
                    _this.dashcamService.setRecordingTime(data).then(function (resp) {
                        console.log("Recording Time updated - ", JSON.stringify(resp));
                    }).catch(function (err) {
                        console.log("Recording Time update error - ", JSON.stringify(err));
                    });
                }
            }
        });
        alert.present();
    };
    SettingsPage.prototype.setCollision = function () {
        var _this = this;
        console.log('setCollision triggered - ', this.camSettings.collision);
        var alert = this.alertCtrl.create();
        alert.setTitle('Colission Sensitivity');
        var checked0 = false, checked1 = false, checked2 = false, checked3 = false, checked4 = false;
        switch (this.camSettings.collision) {
            case '0':
                checked0 = true;
                break;
            case '1':
                checked1 = true;
                break;
            case '2':
                checked2 = true;
                break;
            case '3':
                checked3 = true;
                break;
            case '4':
                checked4 = true;
                break;
            default:
                break;
        }
        alert.addInput({
            type: 'radio',
            label: 'Off',
            value: '0',
            checked: checked0
        });
        alert.addInput({
            type: 'radio',
            label: 'Low',
            value: '1',
            checked: checked1
        });
        alert.addInput({
            type: 'radio',
            label: 'Medium',
            value: '2',
            checked: checked2
        });
        alert.addInput({
            type: 'radio',
            label: 'High',
            value: '3',
            checked: checked3
        });
        alert.addInput({
            type: 'radio',
            label: 'Maximum',
            value: '4',
            checked: checked4
        });
        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: function (data) {
                // this.testRadioOpen = false;
                console.log("Collision Ok -> ", data);
                if (data != '' && data != null) {
                    _this.camSettings.collision = data;
                    _this.dashcamService.setCollision(Number(data)).then(function (resp) {
                        console.log("Collision updated - ", JSON.stringify(resp));
                    }).catch(function (err) {
                        console.log("Collision update error - ", JSON.stringify(err));
                    });
                }
            }
        });
        alert.present();
    };
    /* own */
    SettingsPage.prototype.muteAudio1 = function () {
        console.log('toggleAudio triggered audio -> ', this.camSettings.audio);
        var data;
        if (this.camSettings.audio == 0) {
            data = 1;
        }
        else {
            data = 0;
        }
        this.camSettings.audio = data;
        this.dashcamService.muteAudio(data).then(function (resp) {
            console.log("Audio updated - ", JSON.stringify(resp));
        }).catch(function (err) {
            console.log("Audio update error - ", JSON.stringify(err));
        });
    };
    SettingsPage.prototype.muteAudio = function () {
        var _this = this;
        console.log('toggleAudio triggered audio -> ', this.camSettings.audio);
        // if (this.camSettings.audio == 0 ) {
        //   this.dashcamService.muteAudio(1);
        // } else {
        //   this.dashcamService.muteAudio(0);
        // }
        var alert = this.alertCtrl.create();
        alert.setTitle('Audio Setting');
        var checkedMute = false, checkedUnmute = false;
        switch (this.camSettings.audio) {
            case 0:
                checkedUnmute = true;
                break;
            case 1:
                checkedMute = true;
                console.log("CheckedMute in Switch = ", checkedMute);
                break;
            default:
                break;
        }
        console.log("CheckMute = ", checkedMute);
        alert.addInput({
            type: 'radio',
            label: 'Mute',
            value: '1',
            checked: checkedMute
        });
        alert.addInput({
            type: 'radio',
            label: 'Unmute',
            value: '0',
            checked: checkedUnmute
        });
        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: function (data) {
                console.log("Pressed Ok -> ", data);
                if (data != '' && data != null) {
                    _this.camSettings.audio = Number(data);
                    _this.dashcamService.muteAudio(data).then(function (resp) {
                        console.log("Audio updated - ", JSON.stringify(resp));
                    }).catch(function (err) {
                        console.log("Audio update error - ", JSON.stringify(err));
                    });
                }
            }
        });
        alert.present();
    };
    SettingsPage.prototype.movieDate = function () {
        var _this = this;
        console.log('movieDate triggered -> ', this.camSettings.movieDate);
        var alert = this.alertCtrl.create();
        alert.setTitle('Movie Date Setting');
        var enableDate = false, disableDate = false;
        switch (this.camSettings.movieDate) {
            case 0:
                disableDate = true;
                break;
            case 1:
                enableDate = true;
                console.log("enableDate in Switch = ", enableDate);
                break;
            default:
                break;
        }
        console.log("EnableDate = ", enableDate);
        alert.addInput({
            type: 'radio',
            label: 'Enable',
            value: '1',
            checked: enableDate
        });
        alert.addInput({
            type: 'radio',
            label: 'Disable',
            value: '0',
            checked: disableDate
        });
        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: function (data) {
                console.log("Pressed Ok -> ", data);
                if (data != '' && data != null) {
                    console.log('update movie date status to ', data);
                    _this.camSettings.movieDate = Number(data);
                    _this.dashcamService.movieDate(_this.camSettings.movieDate).then(function (resp) {
                        console.log("movie date updated - ", JSON.stringify(resp));
                    }).catch(function (err) {
                        console.log("movie date update error - ", JSON.stringify(err));
                    });
                }
            }
        });
        alert.present();
    };
    SettingsPage.prototype.changeWifiNetwork = function () {
        var _this = this;
        // console.log('Connecting to WIFI')
        var loading = this.loadingCtrl.create({
            content: 'Changing WiFi...'
        });
        // get current wifi / password
        this.storage.get('wifi')
            .then(function (wdata) {
            _this.wifi = wdata;
            _this.storage.get('password')
                .then(function (pdata) {
                _this.password = pdata;
                // this.dashcamService.getWIFI().then ((data) => {
                //   // parse XML
                //   console.log("getWIFI data ->", JSON.stringify(data));
                //   this.parseXMLWifi(data).then ((key) => {
                //     console.log("AFTER this.wifi ->", this.wifi);
                //     console.log("AFTER this.password ->", this.password);  
                var alert = _this.alertCtrl.create({
                    title: 'WiFi Network and Password',
                    inputs: [
                        {
                            name: 'wifiname',
                            placeholder: 'Network Name',
                            value: _this.wifi
                        },
                        {
                            name: 'wifipassword',
                            placeholder: 'Password',
                            value: _this.password
                        }
                    ],
                    buttons: [
                        {
                            text: 'Cancel',
                            role: 'cancel',
                            handler: function (data) {
                                console.log('Cancel clicked');
                            }
                        },
                        {
                            text: 'Change',
                            handler: function (data) {
                                console.log('data - ', data.wifiname, ' / ', data.wifipassword);
                                // Check if thre is any change in network or password
                                if (_this.wifi == data.wifiname && _this.password == data.wifipassword) {
                                    return;
                                }
                                _this.wifi = data.wifiname;
                                _this.password = data.wifipassword;
                                console.log('this.wifi', _this.wifi, ' | this.password', _this.password);
                                var confMsg = '<p>Network: ' + _this.wifi + '</p><p>Password: ' + _this.password + '</p>';
                                var calert = _this.alertCtrl.create({
                                    title: 'Do you want to change?',
                                    message: confMsg,
                                    buttons: [
                                        {
                                            text: 'No',
                                            role: 'cancel',
                                            handler: function () {
                                                console.log('Cancel clicked');
                                            }
                                        },
                                        {
                                            text: 'Yes',
                                            handler: function () {
                                                // Present loader
                                                loading.present();
                                                // Change WIFI network and Password then reconnect
                                                _this.dashcamService.setSSID(_this.wifi).then(function (data) {
                                                    console.log("Network change sucessfull", JSON.stringify(data));
                                                    _this.dashcamService.setPassphrase(_this.password).then(function (data) {
                                                        console.log("Password change sucessfull", JSON.stringify(data));
                                                        // this.dashcamService.startWifiMode().then((data) => {
                                                        //   console.log("WIFI mode started",JSON.stringify(data));
                                                        // this.dashcamService.sendWifiToDevice(this.wifi,this.password).then((data) => {
                                                        //   console.log("Sent Wifi and password to device",JSON.stringify(data));
                                                        // this.dashcamService.selectWifiStationMode().then((data) => {
                                                        //   console.log("WIFI station mode selected ",JSON.stringify(data));   
                                                        _this.dashcamService.reconnectWifi().then(function (data) {
                                                            console.log("Re-connect sucessfull", JSON.stringify(data));
                                                            // this.dashcamService.stopWifiMode().then((data) => {
                                                            //   console.log("WIFI mode stopped and normal operation started",JSON.stringify(data));
                                                            // Store wifi and pwd in Storage
                                                            _this.storage.set('wifi', _this.wifi);
                                                            _this.storage.set('password', _this.password);
                                                            _this.dashcamService.videoEntry = 'new';
                                                            _this.timeout(10000).then(function () {
                                                                loading.dismiss();
                                                                _this.navCtrl.popToRoot();
                                                            });
                                                            // Now connect using the new wifi and password
                                                            // this.connect();
                                                            // this.navCtrl.pop();
                                                            // }).catch ((err) => {
                                                            //   console.log("Stop WIFI error", JSON.stringify(err));
                                                            // })
                                                        }).catch(function (err) {
                                                            console.log("Reconnect Error", JSON.stringify(err));
                                                        });
                                                        // }).catch ((err) => {
                                                        //   console.log("WIFI Station Error", JSON.stringify(err));
                                                        // })
                                                        // }).catch ((err) => {
                                                        //   console.log("Wifi and pass Sent Error", JSON.stringify(err));
                                                        // })
                                                        // }).catch ((err) => {
                                                        //   console.log("Wifi mode start error", JSON.stringify(err));
                                                        // })
                                                    }).catch(function (err) {
                                                        console.log("Change Password Error", JSON.stringify(err));
                                                    });
                                                }).catch(function (err) {
                                                    console.log("Network Change Error", JSON.stringify(err));
                                                });
                                            }
                                        }
                                    ]
                                });
                                calert.present();
                            }
                        }
                    ]
                });
                alert.present();
                //   }).catch ((err) => {
                //     console.log("error parsing XML ", JSON.stringify(err));
                //   });
                // }).catch((err) => {
                //     console.log("error reading current WIFI ", JSON.stringify(err));
                // });
            }).catch(function (err) {
                console.log("error reading password from storage");
            });
        }).catch(function (err) {
            console.log("error reading wifi from storage");
        });
        // this.storage.get('password')
        // .then((data) => {
        //     this.password = data;
        //     console.log("Wifi ->", this.wifi," / Password ->", this.password);
        // }).catch((err) => {
        //     console.log("error reading user name from storage");
        // });
    };
    SettingsPage.prototype.changeWifiPassword = function () {
    };
    SettingsPage.prototype.download1 = function () {
        var downloadchk;
        if (this.camSettings.download) {
            downloadchk = true;
        }
        else {
            downloadchk = false;
        }
        this.storage.set('download', downloadchk);
        console.log('download triggered' + this.camSettings.download);
    };
    SettingsPage.prototype.download = function () {
        var _this = this;
        console.log('download triggered');
        var alert = this.alertCtrl.create({
            title: 'File Download',
            message: 'Allow Downloads?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () {
                        _this.camSettings.download = false;
                        _this.storage.set('download', false);
                        console.log('No clicked');
                    }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        _this.camSettings.download = true;
                        _this.storage.set('download', true);
                        console.log('Yes clicked');
                    }
                }
            ]
        });
        alert.present();
    };
    SettingsPage.prototype.formatCard = function () {
        var _this = this;
        console.log("format Card triggered");
        var alert = this.alertCtrl.create({
            title: 'Format Card',
            message: 'Are you sure? This will delete all files.',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        _this.dashcamService.formatCard();
                    }
                }
            ]
        });
        alert.present();
    };
    SettingsPage.prototype.resetSystem = function () {
        var _this = this;
        console.log("System reset triggered");
        var alert = this.alertCtrl.create({
            title: 'System Reset',
            message: 'Are you sure? This will reset All Settings.',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        _this.dashcamService.resetSystem().then(function (resp) {
                            console.log("System Reset Sucessfull - ", JSON.stringify(resp));
                        }).catch(function (err) {
                            console.log("System Reset error - ", JSON.stringify(err));
                        });
                        ;
                    }
                }
            ]
        });
        alert.present();
    };
    SettingsPage.prototype.cardCapacity = function () {
        var _this = this;
        console.log("card capacity clicked");
        this.dashcamService.cardCapacity()
            .then(function (capacityData) {
            console.log('Capacity -> ', capacityData);
            _this.parseXML(capacityData).then(function (capacity) {
                var cap = capacity / 1024 / 1024 / 1024;
                var capStr = cap.toFixed(2) + ' GB';
                // get the video time 
                _this.dashcamService.movieCapacity()
                    .then(function (movieCapacityData) {
                    console.log('Video Capacity -> ', movieCapacityData);
                    _this.parseXML(movieCapacityData).then(function (movieCapacity) {
                        // let mcap = movieCapacity / 3600;
                        // let mcapStr = mcap.toFixed(2);
                        // let msg = capStr +' / '+ mcapStr + ' Hrs';
                        var mcapHrs = Math.floor(movieCapacity / 3600);
                        var mcapMin = Math.floor((movieCapacity - (mcapHrs * 3600)) / 60);
                        var mcapSec = movieCapacity - (mcapHrs * 3600) - (mcapMin * 60);
                        // let mcapStr = mcap.toFixed(2);
                        var msg = capStr + ' / ' + mcapHrs + ' Hrs, ' + mcapMin + ' Mins, ' + mcapSec + ' Secs';
                        alert(msg);
                    }).catch(function (error) {
                        console.log('Video Capacity Parse Error / ', error);
                    });
                }).catch(function (error) {
                    console.log('Movie Capacity Error / ', error);
                });
            }).catch(function (error) {
                console.log('Disk Capacity Parse Error / ', error);
            });
        }).catch(function (error) { return console.log("Disk Capacity Error / ", error); });
    };
    SettingsPage.prototype.setupDateTime = function () {
        var _this = this;
        console.log("setup date triggered");
        var alert = this.alertCtrl.create({
            title: 'Reset Camera Date/Time',
            message: 'Would you like to set Current Date?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        var ldate = __WEBPACK_IMPORTED_MODULE_6_moment__(new Date()).format('YYYY-MM-DD');
                        var ltime = __WEBPACK_IMPORTED_MODULE_6_moment__(new Date()).format('hh:mm:ss');
                        console.log("Date ->", ldate);
                        console.log("Time ->", ltime);
                        _this.dashcamService.setupDate(ldate, ltime);
                    }
                }
            ]
        });
        alert.present();
    };
    SettingsPage.prototype.presentConfirm = function () {
        var alert = this.alertCtrl.create({
            title: 'Confirm purchase',
            message: 'Do you want to buy this book?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Buy',
                    handler: function () {
                        console.log('Buy clicked');
                    }
                }
            ]
        });
        alert.present();
    };
    SettingsPage.prototype.parseXML = function (data) {
        return new Promise(function (resolve) {
            var parser = new __WEBPACK_IMPORTED_MODULE_7_xml2js___default.a.Parser({
                trim: true,
                explicitArray: true
            });
            parser.parseString(data, function (err, result) {
                var obj = result.Function;
                var val = obj.Value[0];
                console.log("Object -> ", JSON.stringify(obj));
                console.log("value ->", obj.Value[0]);
                resolve(val);
            });
        });
    };
    SettingsPage.prototype.parseXMLWifi = function (data) {
        return new Promise(function (resolve) {
            console.log("inside parseXMLWifi");
            var parser = new __WEBPACK_IMPORTED_MODULE_7_xml2js___default.a.Parser({
                trim: true,
                explicitArray: true
            });
            parser.parseString(data, function (err, result) {
                var obj = result.LIST;
                this.wifi = obj.SSID;
                this.password = obj.PASSPHRASE;
                console.log("Object -> ", JSON.stringify(obj));
                // console.log("ssid ->", JSON.stringify(obj.SSID[0]));
                // console.log("passphrase ->", JSON.stringify(obj.PASSPHRASE[0]));
                console.log("n ssid ->", JSON.stringify(obj.SSID));
                console.log("n passphrase ->", JSON.stringify(obj.PASSPHRASE));
                resolve(true);
            });
        });
    };
    SettingsPage.prototype.timeout = function (delay) {
        return new Promise(function (resolve, reject) {
            setTimeout(resolve, delay);
        });
    };
    // Parse CML data for Files and convert into an Array
    SettingsPage.prototype.parseSettingsXML = function (data) {
        return new Promise(function (resolve) {
            // var k,l,
            var arr = [], parser = new __WEBPACK_IMPORTED_MODULE_7_xml2js___default.a.Parser({
                trim: true,
                explicitArray: true
            });
            parser.parseString(data, function (err, result) {
                var obj = result.Function;
                // console.log("XML Object -> ", JSON.stringify(obj));
                arr.push({
                    cmd: obj.Cmd[0],
                    status: obj.Status[0],
                });
                arr.push({
                    cmd: obj.Cmd[3],
                    status: obj.Status[3],
                });
                arr.push({
                    cmd: obj.Cmd[4],
                    status: obj.Status[4],
                });
                arr.push({
                    cmd: obj.Cmd[13],
                    status: obj.Status[13],
                });
                arr.push({
                    cmd: obj.Cmd[14],
                    status: obj.Status[14],
                });
                arr.push({
                    cmd: obj.Cmd[15],
                    status: obj.Status[15],
                });
                // console.log("Array ", JSON.stringify(arr));
                resolve(arr);
            });
        });
    };
    SettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-settings',template:/*ion-inline-start:"F:\01_Project\2019-04\04_YoSpot\03_Source\YoSpot\src\pages\settings\settings.html"*/'<!--\n  Generated template for the SettingsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n \n  <ion-navbar >\n    <i navPop class="fa fa-reply back_icon" aria-hidden="true"></i>  \n    <ion-title text-center > Settings</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding style="background-color: #2f2f2f;">\n\n  <ion-list no-lines>\n    <ion-item-divider class="divider">\n        WIFI\n    </ion-item-divider>\n    <div class="section">\n      <!-- <ion-item class="items"  (tap)="qrScan()" tappable>\n        QR Scan\n      </ion-item> -->\n      <ion-item class="items" (tap)="changeWifiNetwork()" tappable>\n        Network and Password\n        <ion-icon name="md-arrow-dropright" item-end></ion-icon>\n\n    </ion-item>\n    <!-- <ion-item class="items" (tap)="changeWifiPassword()" tappable>\n        Password\n    </ion-item> -->\n    </div>\n\n    <ion-item-divider class="divider">\n        YoSpot\n    </ion-item-divider>\n    <div class="section">\n      <ion-item class="items"  (tap)="setResolution()" tappable>\n          Resolution\n          <ion-icon name="md-arrow-dropdown" item-end></ion-icon>\n          \n      </ion-item>\n      <ion-item class="items"   (tap)="setPhotoQuality()" tappable>\n          Photo Quality\n          <ion-icon name="md-arrow-dropdown" item-end></ion-icon>\n      </ion-item>\n      <ion-item class="items"   (tap)="movieDate()" tappable>\n          Print Movie Date\n          <ion-icon name="md-arrow-dropdown" item-end></ion-icon>\n      </ion-item>\n      <ion-item class="items" (tap)="setRecordingTime()" tappable>\n          Recording Time\n          <ion-icon name="md-arrow-dropdown" item-end></ion-icon>\n      </ion-item>\n      <ion-item class="items"  (tap)="setCollision()" tappable>\n          Collision Sensitivity\n          <ion-icon name="md-arrow-dropdown" item-end></ion-icon>\n          \n      </ion-item>\n      <!--ion-item class="items"  (tap)="muteAudio()" tappable>\n          Audio\n          <ion-icon name="md-arrow-dropdown" item-end></ion-icon>\n         \n      </ion-item-->\n\n       <ion-item class="items"  >\n        <ion-label> Audio</ion-label>\n         \n          <ion-toggle  [(ngModel)]="!camSettings.audio" (ionChange)="muteAudio1()" color="dark" item-end ></ion-toggle>\n      </ion-item>\n\n      <!--ion-item  class="items" (tap)="download()" tappable>\n        Download\n        \n      </ion-item-->\n\n      <ion-item  class="items"  tappable>\n          <ion-label>Download</ion-label> \n\n          <ion-toggle  [(ngModel)]="camSettings.download" (ionChange)="download1()" color="dark" item-end ></ion-toggle>\n      </ion-item>\n\n    </div>\n\n    <ion-item-divider class="divider">\n        Storage\n    </ion-item-divider>\n    <div class="section">\n      <ion-item class="items"   (tap)="formatCard()" tappable>\n          Format SD Card\n          <ion-icon name="md-arrow-dropright" item-end></ion-icon>\n      </ion-item>\n      <ion-item class="items"   (tap)="cardCapacity()" tappable>\n          Available Capacity\n          <ion-icon name="md-arrow-dropright" item-end></ion-icon>\n      </ion-item>\n    </div>\n\n    <ion-item-divider class="divider">\n        More\n    </ion-item-divider>\n    <div class="section">\n        <ion-item class="items"   (tap)="resetSystem()" tappable>\n            System Reset\n            <ion-icon name="md-arrow-dropright" item-end></ion-icon>\n        </ion-item>\n        <ion-item class="items"   tappable (tap)="setupDateTime()">\n            Setup Date and Time\n            <ion-icon name="md-arrow-dropright" item-end></ion-icon>\n        </ion-item>\n    </div>\n  </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"F:\01_Project\2019-04\04_YoSpot\03_Source\YoSpot\src\pages\settings\settings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_dashcam_service__["a" /* DashcamService */],
            __WEBPACK_IMPORTED_MODULE_0__providers_utility_service__["a" /* UtilityService */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* LoadingController */]])
    ], SettingsPage);
    return SettingsPage;
}());

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 374:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UtilityService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UtilityService = (function () {
    function UtilityService(alertCtrl) {
        this.alertCtrl = alertCtrl;
    }
    UtilityService.prototype.showErrorAlert = function (header, msg) {
        if (header === void 0) { header = null; }
        if (msg === void 0) { msg = null; }
        var title = 'Error';
        var subTitle = 'Something happened. Please try again';
        if (header) {
            title = header;
        }
        if (msg) {
            subTitle = msg;
        }
        var alert = this.alertCtrl.create({
            title: title,
            subTitle: msg,
            buttons: ['OK']
        });
        alert.present();
    };
    UtilityService.prototype.showAlert = function (header, msg) {
        if (header === void 0) { header = null; }
        if (msg === void 0) { msg = null; }
        var title = 'Success';
        var subTitle = 'Successfully completed';
        if (header) {
            title = header;
        }
        if (msg) {
            subTitle = msg;
        }
        var alert = this.alertCtrl.create({
            title: title,
            subTitle: msg,
            buttons: ['OK']
        });
        alert.present();
    };
    UtilityService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["a" /* AlertController */]])
    ], UtilityService);
    return UtilityService;
}());

//# sourceMappingURL=utility-service.js.map

/***/ }),

/***/ 380:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(402);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 402:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_custom_splash_custom_splash__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_fullscreen_picture_fullscreen_picture__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_library_library__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_settings_settings__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_video_feed_video_feed__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_splash_screen__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_status_bar__ = __webpack_require__(379);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_barcode_scanner__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__app_component__ = __webpack_require__(477);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_home_home__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_in_app_browser__ = __webpack_require__(478);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_common_http__ = __webpack_require__(479);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_screen_orientation__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__providers_utility_service__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__providers_http_service__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__providers_dashcam_service__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_http__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_storage__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21_ionic_swipe_all__ = __webpack_require__(485);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_background_mode__ = __webpack_require__(489);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_photo_library__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_file__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_file_transfer__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_native_geolocation__ = __webpack_require__(377);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










// import { StreamingMedia } from '@ionic-native/streaming-media';















// import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';


var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_6__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_11__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_12__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_4__pages_video_feed_video_feed__["a" /* VideoFeedPage */],
                __WEBPACK_IMPORTED_MODULE_3__pages_settings_settings__["a" /* SettingsPage */],
                __WEBPACK_IMPORTED_MODULE_1__pages_fullscreen_picture_fullscreen_picture__["a" /* FullscreenPicturePage */],
                __WEBPACK_IMPORTED_MODULE_0__pages_custom_splash_custom_splash__["a" /* CustomSplashPage */],
                __WEBPACK_IMPORTED_MODULE_2__pages_library_library__["a" /* LibraryPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_14__angular_common_http__["a" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_21_ionic_swipe_all__["a" /* IonicSwipeAllModule */],
                __WEBPACK_IMPORTED_MODULE_7_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_11__app_component__["a" /* MyApp */], {}, {
                    links: []
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_7_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_11__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_12__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_4__pages_video_feed_video_feed__["a" /* VideoFeedPage */],
                __WEBPACK_IMPORTED_MODULE_3__pages_settings_settings__["a" /* SettingsPage */],
                __WEBPACK_IMPORTED_MODULE_1__pages_fullscreen_picture_fullscreen_picture__["a" /* FullscreenPicturePage */],
                __WEBPACK_IMPORTED_MODULE_0__pages_custom_splash_custom_splash__["a" /* CustomSplashPage */],
                __WEBPACK_IMPORTED_MODULE_2__pages_library_library__["a" /* LibraryPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_splash_screen__["a" /* SplashScreen */],
                // StreamingMedia,
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
                __WEBPACK_IMPORTED_MODULE_16__providers_utility_service__["a" /* UtilityService */],
                __WEBPACK_IMPORTED_MODULE_17__providers_http_service__["a" /* HttpService */],
                __WEBPACK_IMPORTED_MODULE_19__ionic_native_http__["a" /* HTTP */],
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_background_mode__["a" /* BackgroundMode */],
                __WEBPACK_IMPORTED_MODULE_18__providers_dashcam_service__["a" /* DashcamService */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_photo_library__["a" /* PhotoLibrary */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_25__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_25__ionic_native_file_transfer__["b" /* FileTransferObject */],
                __WEBPACK_IMPORTED_MODULE_26__ionic_native_geolocation__["a" /* Geolocation */],
                { provide: __WEBPACK_IMPORTED_MODULE_6__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_7_ionic_angular__["c" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 403:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomSplashPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__home_home__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the CustomSplashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CustomSplashPage = (function () {
    function CustomSplashPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.videoUrl = false;
    }
    CustomSplashPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad CustomSplashPage');
        this.timeout(6000).then(function () {
            _this.playVideo();
        });
    };
    CustomSplashPage.prototype.playVideo = function () {
        var _this = this;
        this.videoUrl = true;
        this.timeout(1000).then(function () {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__home_home__["a" /* HomePage */]);
        });
    };
    CustomSplashPage.prototype.timeout = function (delay) {
        return new Promise(function (resolve, reject) {
            setTimeout(resolve, delay);
        });
    };
    CustomSplashPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-custom-splash',template:/*ion-inline-start:"F:\01_Project\2019-04\04_YoSpot\03_Source\YoSpot\src\pages\custom-splash\custom-splash.html"*/'<!--\n  Generated template for the CustomSplashPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>custom-splash</ion-title>\n  </ion-navbar>\n\n</ion-header> -->\n\n\n<ion-content padding class="backgroundImg">\n  <!-- <img class="logoImage" src=\'./assets/imgs/logo.png\' /> -->\n  <img class="logoImage" src=\'./assets/imgs/logoAnim.gif\' alt="YoSpot" />\n  <!-- <div class="videoPlayer"> -->\n   <!-- <video *ngIf="videoUrl == true" controls=false preload=true autoplay=true webkit-playsinline="webkit-playsinline" > -->\n  <!-- <video *ngIf="videoUrl == true" poster playsinline preload="auto" autoplay>    \n    <source src="./assets/videos/YoSpotVideo.mov" type="video/mp4" />\n  </video> -->\n  <!-- </div> -->\n</ion-content>\n'/*ion-inline-end:"F:\01_Project\2019-04\04_YoSpot\03_Source\YoSpot\src\pages\custom-splash\custom-splash.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavParams */]])
    ], CustomSplashPage);
    return CustomSplashPage;
}());

//# sourceMappingURL=custom-splash.js.map

/***/ }),

/***/ 42:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashcamService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__http_service__ = __webpack_require__(146);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// import { UtilityService } from './utility-service';


// import { UtilityService } from './utility-service'
var DashcamService = (function () {
    // private resetWifiUrl = 'http://192.168.1.254/?custom=1&cmd=3018';  // parm = hh:mm:ss
    function DashcamService(httpService) {
        this.httpService = httpService;
        // Camera Settings
        // public camSetting = {
        //     photo: '',
        //     resolution: '',
        //     audio: 0,
        //     collision: '',
        //     download: false,
        //     capacity: 0
        // }
        this.liveFeedUrl = 'http://192.168.1.254:8192';
        this.videoRecording = false;
        this.videoEntry = 'new';
        this.feedModeUrl = 'http://192.168.1.254/?custom=1&cmd=3028&par=';
        this.baseUrl = 'http://192.168.1.254/?custom=1&cmd=';
        // Status Commands
        this.currStatusUrl = 'http://192.168.1.254/?custom=1&cmd=3037';
        this.systemResetUrl = 'http://192.168.1.254/?custom=1&cmd=3011';
        // Video & Photo Commands to change mode to Picture, Video, Live View or File download
        this.liveViewStartUrl = 'http://192.168.1.254/?custom=1&cmd=2015&par=1';
        this.liveViewStopUrl = 'http://192.168.1.254/?custom=1&cmd=2015&par=0';
        this.pictureModeUrl = 'http://192.168.1.254/?custom=1&cmd=3001&par=0';
        this.videoModeUrl = 'http://192.168.1.254/?custom=1&cmd=3001&par=1';
        this.takePictureCommand = '1001'; // No parm
        this.recordVideoCommand = '2001'; // parm: 1 = start, 0 = stop
        this.muteAudioCommand = '2007'; // parm: 1 = ON, 0 = OFF
        // Librabry URLs and commands
        this.playbackModeUrl = 'http://192.168.1.254/?custom=1&cmd=3001&par=2';
        this.fileListUrl = 'http://192.168.1.254/?custom=1&cmd=3015';
        // Settings functions
        this.formatCardUrl = 'http://192.168.1.254/?custom=1&cmd=3010&par=1';
        this.setupDateUrl = 'http://192.168.1.254/?custom=1&cmd=3005&str='; // parm = yyyy-mm-dd
        this.setupTimeUrl = 'http://192.168.1.254/?custom=1&cmd=3006&str='; // parm = hh:mm:ss
    }
    DashcamService.prototype.liveFeedToggle = function (mode) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (mode == 'start') {
                // Start Live Video
                _this.httpService.post(_this.liveViewStartUrl)
                    .then(function (videoStatus) {
                    resolve(videoStatus);
                }).catch(function (error) {
                    reject(error);
                });
            }
            else if (mode == 'stop') {
                // Stop Live Video
                _this.httpService.post(_this.liveViewStopUrl)
                    .then(function (videoStatus) {
                    resolve(videoStatus);
                }).catch(function (error) {
                    reject(error);
                });
            }
        });
    };
    DashcamService.prototype.changeCameraMode = function (mode) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url;
            switch (mode) {
                case 'PLAYBACK':
                    url = _this.playbackModeUrl;
                    break;
                case 'VIDEO':
                    url = _this.videoModeUrl;
                    break;
                case 'PHOTO':
                    url = _this.pictureModeUrl;
                    break;
                default:
                    break;
            }
            _this.httpService.post(url)
                .then(function (status) {
                resolve(status);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    // Submit Video Feed commands with 4 different modes 0-Front, 1-Back, 2-BackOnFront, 3-FrontOnBack
    DashcamService.prototype.changeFeedMode = function (mode) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var fullUrl = _this.feedModeUrl + mode;
            // console.log("Change Feed Mode URL ->", fullUrl);
            _this.httpService.post(fullUrl)
                .then(function (status) {
                // console.log("Change Feed successful");
                resolve(status);
            }).catch(function (error) {
                // console.log("Change Feed error / ", error);
                reject(error);
            });
        });
    };
    // System reset
    DashcamService.prototype.systemReset = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.httpService.post(_this.systemResetUrl)
                .then(function (status) {
                // console.log("Change Feed successful");
                resolve(status);
            }).catch(function (error) {
                // console.log("Change Feed error / ", error);
                reject(error);
            });
        });
    };
    // Check the current mode Video, Photo or Playback
    DashcamService.prototype.currStatus = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // console.log("currStatusURL ->", this.currStatusUrl);
            // Get the current status
            _this.httpService.post(_this.currStatusUrl)
                .then(function (status) {
                // console.log("\nstatus received", JSON.stringify(status));
                resolve(status);
            })
                .catch(function (error) {
                // console.log(" status recevie error / ", JSON.stringify(error));
                reject(error);
            });
        });
    };
    DashcamService.prototype.takePicture = function (cameraMode) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // Setup URL to take pictures
            var takePictureUrl = _this.baseUrl + _this.takePictureCommand;
            // Change the mode to picture mode 
            _this.httpService.post(_this.pictureModeUrl)
                .then(function (pictureStatus) {
                console.log("Picture Mode changed successfully", JSON.stringify(pictureStatus));
                // PIP MODE
                if (cameraMode == 2 || cameraMode == 3) {
                    console.log("PIP Mode");
                    console.log("change Picture mode");
                    _this.changeFeedMode(0).then(function (jsonData) {
                        console.log("Picture mode changed to 0");
                        _this.httpService.post(takePictureUrl).then(function (jsonData) {
                            console.log("Take Picture Command successful");
                            _this.changeFeedMode(1).then(function (jsonData) {
                                console.log("Picture mode changed to 1");
                                _this.httpService.post(takePictureUrl).then(function (jsonData) {
                                    console.log("Take Picture Command successful");
                                    _this.changeFeedMode(cameraMode).then(function (jsonData) {
                                        console.log("Restore camera mode / ", cameraMode);
                                        resolve(jsonData);
                                    }).catch(function (error) {
                                        console.log("Restore camera mode error  / ", JSON.stringify(error));
                                        reject(error);
                                    });
                                }).catch(function (error) {
                                    console.log("Take Picture Command error  / ", JSON.stringify(error));
                                    reject(error);
                                });
                            }).catch(function (error) {
                                console.log("Picture mode 1 change error  / ", JSON.stringify(error));
                                reject(error);
                            });
                        }).catch(function (error) {
                            console.log("Take Picture error / ", JSON.stringify(error));
                            reject(error);
                        });
                    }).catch(function (error) {
                        console.log("Picture mode 0 change error / ", JSON.stringify(error));
                        reject(error);
                    });
                }
                else {
                    console.log("NO PIP Mode / ", cameraMode);
                    _this.httpService.post(takePictureUrl)
                        .then(function (jsonData) {
                        console.log("Take Picture Command successful", JSON.stringify(jsonData));
                        resolve(jsonData);
                    }).catch(function (error) {
                        console.log("Take Picture error / ", JSON.stringify(error));
                        reject(error);
                    });
                }
            })
                .catch(function (error) {
                console.log("Picture Mode change error / ", JSON.stringify(error));
                reject(error);
            });
        });
    };
    DashcamService.prototype.recordVideo = function (parm) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // Setup URL to take video
            var recordVideoUrl = _this.baseUrl + _this.recordVideoCommand + '&par=' + parm;
            // console.log("videoModeURL ->", this.videoModeUrl);
            // console.log("recordVideoURL ->", recordVideoUrl);
            // Change the mode to video mode first
            if (parm == 1) {
                // this.httpService.post(this.videoModeUrl)
                // .then((videoStatus: any) => {
                //     console.log("\nVideo Mode change sucessfully", JSON.stringify(videoStatus));
                // Submit command to record video
                _this.httpService.post(recordVideoUrl)
                    .then(function (jsonData) {
                    console.log("Video recording ON Command successful", JSON.stringify(jsonData));
                    resolve(jsonData);
                }).catch(function (error) {
                    console.log("Video Recording error / ", JSON.stringify(error));
                    reject(error);
                });
                // })
                // .catch ((error) => {
                //     console.log(" Video Mode change error / ", JSON.stringify(error));
                //     reject(error);
                // });
            }
            else {
                // this.httpService.post(this.videoModeUrl)
                // .then((videoStatus: any) => {
                //     console.log("\nVideo Mode change sucessfully", JSON.stringify(videoStatus));
                // Submit command to record video
                _this.httpService.post(recordVideoUrl)
                    .then(function (jsonData) {
                    console.log("\nVideo recording OFF Command successful", JSON.stringify(jsonData));
                    resolve(jsonData);
                    // if (parm == 0) {
                    //     // Change mode back to Live View if recording is complete
                    //     this.httpService.post(this.liveViewStartUrl)
                    //     .then((videoStatus: any) => {
                    //         console.log("\nLive Video Restored sucessfully",JSON.stringify(videoStatus))    
                    //         // Restore original live feed
                    //         this.changeFeedMode(0)               
                    //     }).catch ((error) => {
                    //         console.log("Live Video Change error / ",JSON.stringify(error)) 
                    //         reject(error);
                    //     });
                    // }
                }).catch(function (error) {
                    console.log("Video Recording error / ", JSON.stringify(error));
                    reject(error);
                });
                // })
                // .catch ((error) => {
                //     console.log(" Video Mode change error / ", JSON.stringify(error));
                //     reject(error);
                // });     
            }
        });
    };
    // public muteAudio(parm) {
    DashcamService.prototype.muteAudio = function (parm) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // Setup URL to change Audio
            var muteAudioUrl = _this.baseUrl + _this.muteAudioCommand + '&par=' + parm;
            console.log("muteAudioURL ->", muteAudioUrl);
            // Submit command to mute Audio 
            _this.httpService.post(muteAudioUrl)
                .then(function (jsonData) {
                console.log("\nMute Audio Command successful", JSON.stringify(jsonData));
                resolve(jsonData);
            }).catch(function (error) {
                console.log("Mute Audio error / ", JSON.stringify(error));
                reject(error);
            });
        });
    };
    DashcamService.prototype.qrScan = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log("Url ->", _this.formatCardUrl);
            // let url = 
            // Submit command to Format Card 
            _this.httpService.post(_this.formatCardUrl)
                .then(function (jsonData) {
                console.log("\nFormat Card Command successful", JSON.stringify(jsonData));
                resolve(jsonData);
            }).catch(function (error) {
                console.log("Format Card error / ", JSON.stringify(error));
                reject(error);
            });
        });
    };
    DashcamService.prototype.setResolution = function (resolution) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var recordUrl = 'http://192.168.1.254/?custom=1&cmd=2002&par=' + resolution;
            // let liveViewUrl = 'http://192.168.1.254/?custom=1&cmd=2010&par=' + resolution;
            console.log("Resolution url - ", recordUrl);
            // Submit http command 
            // this.httpService.post(liveViewUrl)
            // .then((jsonData: any) => {
            _this.httpService.post(recordUrl)
                .then(function (jsonData) {
                resolve(jsonData);
            }).catch(function (error) {
                reject(error);
            });
            // }).catch ((error) => {
            //     reject(error);
            // });
        });
    };
    DashcamService.prototype.setPhotoQuality = function (size) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = 'http://192.168.1.254/?custom=1&cmd=1002&par=' + size;
            // Submit http command 
            _this.httpService.post(url)
                .then(function (jsonData) {
                resolve(jsonData);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    DashcamService.prototype.setRecordingTime = function (parm) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = 'http://192.168.1.254/?custom=1&cmd=2003&par=' + parm;
            // Submit http command 
            _this.httpService.post(url)
                .then(function (jsonData) {
                resolve(jsonData);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    DashcamService.prototype.resetSystem = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = 'http://192.168.1.254/?custom=1&cmd=3011';
            // Submit command to Reset System
            _this.httpService.post(url)
                .then(function (jsonData) {
                console.log("\nReset System successful", JSON.stringify(jsonData));
                resolve(jsonData);
            }).catch(function (error) {
                console.log("Reset System error / ", JSON.stringify(error));
                reject(error);
            });
        });
    };
    DashcamService.prototype.movieDate = function (parm) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = 'http://192.168.1.254/?custom=1&cmd=2008&par=' + parm;
            console.log("movie date url - ", url);
            // Submit http command 
            _this.httpService.post(url)
                .then(function (jsonData) {
                resolve(jsonData);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    // public getStatus(command) {
    DashcamService.prototype.getSettings = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = 'http://192.168.1.254/?custom=1&cmd=3014';
            // Submit http command 
            _this.httpService.post(url)
                .then(function (jsonData) {
                resolve(jsonData);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    DashcamService.prototype.getRecordingTime = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = 'http://192.168.1.254/?custom=1&cmd=2009';
            // Submit http command 
            _this.httpService.post(url)
                .then(function (jsonData) {
                resolve(jsonData);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    DashcamService.prototype.setCollision = function (sensitivity) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = 'http://192.168.1.254/?custom=1&cmd=2011&par=' + sensitivity;
            console.log("Collision Url - ", url);
            // Submit http command 
            _this.httpService.post(url)
                .then(function (jsonData) {
                resolve(jsonData);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    DashcamService.prototype.download = function (allowDownload) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = '';
            // Submit http command 
            _this.httpService.post(url)
                .then(function (jsonData) {
                resolve(jsonData);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    DashcamService.prototype.formatCard = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log("formatCardURL ->", _this.formatCardUrl);
            // Submit command to Format Card 
            _this.httpService.post(_this.formatCardUrl)
                .then(function (jsonData) {
                console.log("\nFormat Card Command successful", JSON.stringify(jsonData));
                resolve(jsonData);
            }).catch(function (error) {
                console.log("Format Card error / ", JSON.stringify(error));
                reject(error);
            });
        });
    };
    DashcamService.prototype.cardCapacity = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = 'http://192.168.1.254/?custom=1&cmd=3017';
            // Submit http command 
            _this.httpService.post(url)
                .then(function (jsonData) {
                resolve(jsonData);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    DashcamService.prototype.movieCapacity = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = 'http://192.168.1.254/?custom=1&cmd=2009';
            // Submit http command 
            _this.httpService.post(url)
                .then(function (jsonData) {
                resolve(jsonData);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    DashcamService.prototype.getWIFI = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = 'http://192.168.1.254/?custom=1&cmd=3029';
            // console.log('SSID url ->', url);
            // Submit http command 
            _this.httpService.post(url)
                .then(function (jsonData) {
                resolve(jsonData);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    DashcamService.prototype.setSSID = function (parm) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = 'http://192.168.1.254/?custom=1&cmd=3003&str=' + parm;
            console.log('SSID url ->', url);
            // Submit http command 
            _this.httpService.post(url)
                .then(function (jsonData) {
                resolve(jsonData);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    DashcamService.prototype.deleteFile = function (file) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // let url = 'http://192.168.1.254/?custom=1&cmd=4003&str=' + fileName;
            // console.log('Delete File url ->', fileUrl);
            // Submit http command 
            _this.httpService.postDeleteFile(file)
                .then(function (jsonData) {
                resolve(jsonData);
            }).catch(function (error) {
                // console.log("delete error provider - ", JSON.stringify(error));
                reject(error);
            });
        });
    };
    DashcamService.prototype.setPassphrase = function (parm) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = 'http://192.168.1.254/?custom=1&cmd=3004&str=' + parm;
            // Submit http command 
            _this.httpService.post(url)
                .then(function (jsonData) {
                resolve(jsonData);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    DashcamService.prototype.reconnectWifi = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = 'http://192.168.1.254/?custom=1&cmd=3018';
            // Submit http command 
            _this.httpService.post(url)
                .then(function (jsonData) {
                resolve(jsonData);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    DashcamService.prototype.dvConnected = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = 'http://192.168.1.254/?custom=1&cmd=3016';
            // Submit http command 
            _this.httpService.post(url)
                .then(function (jsonData) {
                resolve(jsonData);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    DashcamService.prototype.startWifiMode = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = 'http://192.168.1.254/?custom=1&cmd=3035';
            // Submit http command 
            _this.httpService.post(url)
                .then(function (jsonData) {
                resolve(jsonData);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    DashcamService.prototype.stopWifiMode = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = 'http://192.168.1.254/?custom=1&cmd=3036';
            // Submit http command 
            _this.httpService.post(url)
                .then(function (jsonData) {
                resolve(jsonData);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    DashcamService.prototype.selectWifiStationMode = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = 'http://192.168.1.254/?custom=1&cmd=3033&par=1';
            // Submit http command 
            _this.httpService.post(url)
                .then(function (jsonData) {
                resolve(jsonData);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    DashcamService.prototype.sendWifiToDevice = function (ssid, passphrase) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = 'http://192.168.1.254/?custom=1&cmd=3032&str=' + ssid + ':' + passphrase;
            // Submit http command 
            _this.httpService.post(url)
                .then(function (jsonData) {
                resolve(jsonData);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    DashcamService.prototype.setupDate = function (date, time) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // Setup URL to setup Date
            var dateUrl = _this.setupDateUrl + date;
            var timeUrl = _this.setupTimeUrl + time;
            console.log("Date URL ->", dateUrl);
            console.log("Time URL ->", timeUrl);
            // Submit command to Setup Date 
            _this.httpService.post(dateUrl)
                .then(function (jsonData) {
                console.log("\nSetup Date Command successful", JSON.stringify(jsonData));
                resolve(jsonData);
                // Submit command to Setup Time 
                _this.httpService.post(timeUrl)
                    .then(function (jsonData) {
                    console.log("\nSetup Time Command successful", JSON.stringify(jsonData));
                    resolve(jsonData);
                }).catch(function (error) {
                    console.log("Setup Time error / ", JSON.stringify(error));
                    reject(error);
                });
            }).catch(function (error) {
                console.log("Setup Date error / ", JSON.stringify(error));
                reject(error);
            });
        });
    };
    DashcamService.prototype.fileList = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // Setup URL to take pictures
            console.log("fileListURL ->", _this.fileListUrl);
            // Submit command to file List 
            _this.httpService.post(_this.fileListUrl)
                .then(function (jsonData) {
                console.log("\nfile List Command successful");
                resolve(jsonData);
            }).catch(function (error) {
                console.log("file List error / ", JSON.stringify(error));
                reject(error);
            });
        });
    };
    DashcamService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__http_service__["a" /* HttpService */]])
    ], DashcamService);
    return DashcamService;
}());

// // Submit Dashcam command 
// private submitCommand(url, params){ 
//     return new Promise((resolve, reject) => {
//         let fullUrl = url + params;
//         console.log("URL ->", fullUrl);
//         this.httpService.post(fullUrl)
//         .then((status: any) => {
//             // console.log("Command successful");
//             resolve(status)
//         }).catch ((error) => {
//             // console.log("Error submitting Url / ", error);
//             reject(error);
//         });
//     }) 
// } 
//# sourceMappingURL=dashcam-service.js.map

/***/ }),

/***/ 454:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 233,
	"./af.js": 233,
	"./ar": 234,
	"./ar-dz": 235,
	"./ar-dz.js": 235,
	"./ar-kw": 236,
	"./ar-kw.js": 236,
	"./ar-ly": 237,
	"./ar-ly.js": 237,
	"./ar-ma": 238,
	"./ar-ma.js": 238,
	"./ar-sa": 239,
	"./ar-sa.js": 239,
	"./ar-tn": 240,
	"./ar-tn.js": 240,
	"./ar.js": 234,
	"./az": 241,
	"./az.js": 241,
	"./be": 242,
	"./be.js": 242,
	"./bg": 243,
	"./bg.js": 243,
	"./bm": 244,
	"./bm.js": 244,
	"./bn": 245,
	"./bn.js": 245,
	"./bo": 246,
	"./bo.js": 246,
	"./br": 247,
	"./br.js": 247,
	"./bs": 248,
	"./bs.js": 248,
	"./ca": 249,
	"./ca.js": 249,
	"./cs": 250,
	"./cs.js": 250,
	"./cv": 251,
	"./cv.js": 251,
	"./cy": 252,
	"./cy.js": 252,
	"./da": 253,
	"./da.js": 253,
	"./de": 254,
	"./de-at": 255,
	"./de-at.js": 255,
	"./de-ch": 256,
	"./de-ch.js": 256,
	"./de.js": 254,
	"./dv": 257,
	"./dv.js": 257,
	"./el": 258,
	"./el.js": 258,
	"./en-SG": 259,
	"./en-SG.js": 259,
	"./en-au": 260,
	"./en-au.js": 260,
	"./en-ca": 261,
	"./en-ca.js": 261,
	"./en-gb": 262,
	"./en-gb.js": 262,
	"./en-ie": 263,
	"./en-ie.js": 263,
	"./en-il": 264,
	"./en-il.js": 264,
	"./en-nz": 265,
	"./en-nz.js": 265,
	"./eo": 266,
	"./eo.js": 266,
	"./es": 267,
	"./es-do": 268,
	"./es-do.js": 268,
	"./es-us": 269,
	"./es-us.js": 269,
	"./es.js": 267,
	"./et": 270,
	"./et.js": 270,
	"./eu": 271,
	"./eu.js": 271,
	"./fa": 272,
	"./fa.js": 272,
	"./fi": 273,
	"./fi.js": 273,
	"./fo": 274,
	"./fo.js": 274,
	"./fr": 275,
	"./fr-ca": 276,
	"./fr-ca.js": 276,
	"./fr-ch": 277,
	"./fr-ch.js": 277,
	"./fr.js": 275,
	"./fy": 278,
	"./fy.js": 278,
	"./ga": 279,
	"./ga.js": 279,
	"./gd": 280,
	"./gd.js": 280,
	"./gl": 281,
	"./gl.js": 281,
	"./gom-latn": 282,
	"./gom-latn.js": 282,
	"./gu": 283,
	"./gu.js": 283,
	"./he": 284,
	"./he.js": 284,
	"./hi": 285,
	"./hi.js": 285,
	"./hr": 286,
	"./hr.js": 286,
	"./hu": 287,
	"./hu.js": 287,
	"./hy-am": 288,
	"./hy-am.js": 288,
	"./id": 289,
	"./id.js": 289,
	"./is": 290,
	"./is.js": 290,
	"./it": 291,
	"./it-ch": 292,
	"./it-ch.js": 292,
	"./it.js": 291,
	"./ja": 293,
	"./ja.js": 293,
	"./jv": 294,
	"./jv.js": 294,
	"./ka": 295,
	"./ka.js": 295,
	"./kk": 296,
	"./kk.js": 296,
	"./km": 297,
	"./km.js": 297,
	"./kn": 298,
	"./kn.js": 298,
	"./ko": 299,
	"./ko.js": 299,
	"./ku": 300,
	"./ku.js": 300,
	"./ky": 301,
	"./ky.js": 301,
	"./lb": 302,
	"./lb.js": 302,
	"./lo": 303,
	"./lo.js": 303,
	"./lt": 304,
	"./lt.js": 304,
	"./lv": 305,
	"./lv.js": 305,
	"./me": 306,
	"./me.js": 306,
	"./mi": 307,
	"./mi.js": 307,
	"./mk": 308,
	"./mk.js": 308,
	"./ml": 309,
	"./ml.js": 309,
	"./mn": 310,
	"./mn.js": 310,
	"./mr": 311,
	"./mr.js": 311,
	"./ms": 312,
	"./ms-my": 313,
	"./ms-my.js": 313,
	"./ms.js": 312,
	"./mt": 314,
	"./mt.js": 314,
	"./my": 315,
	"./my.js": 315,
	"./nb": 316,
	"./nb.js": 316,
	"./ne": 317,
	"./ne.js": 317,
	"./nl": 318,
	"./nl-be": 319,
	"./nl-be.js": 319,
	"./nl.js": 318,
	"./nn": 320,
	"./nn.js": 320,
	"./pa-in": 321,
	"./pa-in.js": 321,
	"./pl": 322,
	"./pl.js": 322,
	"./pt": 323,
	"./pt-br": 324,
	"./pt-br.js": 324,
	"./pt.js": 323,
	"./ro": 325,
	"./ro.js": 325,
	"./ru": 326,
	"./ru.js": 326,
	"./sd": 327,
	"./sd.js": 327,
	"./se": 328,
	"./se.js": 328,
	"./si": 329,
	"./si.js": 329,
	"./sk": 330,
	"./sk.js": 330,
	"./sl": 331,
	"./sl.js": 331,
	"./sq": 332,
	"./sq.js": 332,
	"./sr": 333,
	"./sr-cyrl": 334,
	"./sr-cyrl.js": 334,
	"./sr.js": 333,
	"./ss": 335,
	"./ss.js": 335,
	"./sv": 336,
	"./sv.js": 336,
	"./sw": 337,
	"./sw.js": 337,
	"./ta": 338,
	"./ta.js": 338,
	"./te": 339,
	"./te.js": 339,
	"./tet": 340,
	"./tet.js": 340,
	"./tg": 341,
	"./tg.js": 341,
	"./th": 342,
	"./th.js": 342,
	"./tl-ph": 343,
	"./tl-ph.js": 343,
	"./tlh": 344,
	"./tlh.js": 344,
	"./tr": 345,
	"./tr.js": 345,
	"./tzl": 346,
	"./tzl.js": 346,
	"./tzm": 347,
	"./tzm-latn": 348,
	"./tzm-latn.js": 348,
	"./tzm.js": 347,
	"./ug-cn": 349,
	"./ug-cn.js": 349,
	"./uk": 350,
	"./uk.js": 350,
	"./ur": 351,
	"./ur.js": 351,
	"./uz": 352,
	"./uz-latn": 353,
	"./uz-latn.js": 353,
	"./uz.js": 352,
	"./vi": 354,
	"./vi.js": 354,
	"./x-pseudo": 355,
	"./x-pseudo.js": 355,
	"./yo": 356,
	"./yo.js": 356,
	"./zh-cn": 357,
	"./zh-cn.js": 357,
	"./zh-hk": 358,
	"./zh-hk.js": 358,
	"./zh-tw": 359,
	"./zh-tw.js": 359
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 454;

/***/ }),

/***/ 465:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 467:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 477:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(379);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(80);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// import { CustomSplashPage } from './../pages/custom-splash/custom-splash';
// import { SettingsPage } from './../pages/settings/settings';
// import { FullscreenPicturePage } from './../pages/fullscreen-picture/fullscreen-picture';
// import { LibraryPage } from './../pages/library/library';





// import { VideoFeedPage} from '../pages/video-feed/video-feed'
// import { BackgroundMode } from '@ionic-native/background-mode';
var MyApp = (function () {
    // rootPage:any = CustomSplashPage;
    // rootPage:any = SettingsPage;
    // rootPage:any = FullscreenPicturePage;
    //  rootPage: any = LibraryPage;
    //  rootPage:any = VideoFeedPage;
    function MyApp(platform, statusBar, 
        // private backgroundMode: BackgroundMode,
        splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            // this.backgroundMode.enable();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"F:\01_Project\2019-04\04_YoSpot\03_Source\YoSpot\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"F:\01_Project\2019-04\04_YoSpot\03_Source\YoSpot\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 80:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_dashcam_service__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__video_feed_video_feed__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_barcode_scanner__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_geolocation__ = __webpack_require__(377);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






// import { NativeAudio } from '@ionic-native/native-audio';

// import './global';
var HomePage = (function () {
    // wifi: string;
    // password: string;
    function HomePage(navCtrl, platform, storage, barcode, dashcamService, geolocation, 
        //   private nativeAudio: NativeAudio,
        loadingCtrl) {
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.storage = storage;
        this.barcode = barcode;
        this.dashcamService = dashcamService;
        this.geolocation = geolocation;
        this.loadingCtrl = loadingCtrl;
        this.loaderIsPresent = false;
        this.scanInProgress = false;
        this.geolocation.getCurrentPosition().then(function (resp) {
            // resp.coords.latitude
            // resp.coords.longitude
        }).catch(function (error) {
            console.log('Error getting location', error);
        });
        // storage.get('wifi')
        // .then((data) => {
        //     this.wifi = data;
        // }).catch((err) => {
        //     console.log("error reading user name from storage");
        // });
        // storage.get('password')
        // .then((data) => {
        //     this.password = data;
        //     // If stored wifi & password found then connect automatically
        //     console.log("Wifi ->", this.wifi," / Password ->", this.password);
        //     // if (this.password != '' && this.password != undefined ) {
        //     //     this.connect();
        //     // }  
        // }).catch((err) => {
        //     console.log("error reading user name from storage");
        // });
    }
    HomePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.storage.get('wifi')
            .then(function (data) {
            _this.wifi = data;
        }).catch(function (err) {
            console.log("error reading user name from storage");
        });
        this.storage.get('password')
            .then(function (data) {
            _this.password = data;
            console.log("Wifi ->", _this.wifi, " / Password ->", _this.password);
        }).catch(function (err) {
            console.log("error reading user name from storage");
        });
    };
    HomePage.prototype.tutorial = function () {
    };
    HomePage.prototype.qrScan = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // await WifiWizard2.requestPermission();
                        if (this.scanInProgress) {
                            return [2 /*return*/];
                        }
                        else {
                            // var audio = new Audio('./assets/sounds/beep-07.wav');
                            // audio.play();
                            this.scanInProgress = true;
                        }
                        this.options = {
                            prompt: 'Scan code for info',
                            preferFrontCamera: false,
                            showTorchButton: true,
                            formats: "QR_CODE",
                            disableSuccessBeep: true,
                            disableAnimations: false
                        };
                        return [4 /*yield*/, this.barcode.scan(this.options).then(function (barcodeData) {
                                // Success! Barcode data is here
                                _this.scanInProgress = false;
                                console.log('barcodeData text - ', JSON.stringify(barcodeData.text));
                                if (barcodeData.text == null || barcodeData.text == "") {
                                }
                                else {
                                    console.log('Connect to WiFi - PLAY SOUND');
                                    // extract WIFI Network and Password
                                    // setup variables wifi and password
                                    // play sound
                                    //   this.nativeAudio.preloadSimple('uniqueId1', 'path/to/file.mp3').then(onSuccess, onError);
                                    //   this.nativeAudio.play('uniqueId1').then(onSuccess, onError);
                                    var audio = new Audio('./assets/sounds/beep-07.wav');
                                    audio.play();
                                    var pos = barcodeData.text.lastIndexOf('/');
                                    _this.wifi = barcodeData.text.substring(0, pos);
                                    _this.password = barcodeData.text.substring(pos + 1, barcodeData.text.length);
                                    console.log("WIFI - ", _this.wifi);
                                    console.log("Password - ", _this.password);
                                    //   this.wifi = 'WIFI_CARDV77fc';
                                    //   this.password = '12345678';
                                    _this.connect();
                                }
                            }, function (err) {
                                // An error occurred
                                _this.scanInProgress = false;
                                alert("Barcode Scanner Plugin Error - " + JSON.stringify(err));
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    // connect() {
    //     // Check WiFi code
    //     if (this.wifi == '' || this.wifi == null) {
    //         alert("Please enter WiFi Code");
    //         return;
    //     }
    //     // Check Password
    //     if (this.password == '' || this.password == null) {
    //         alert("Please enter WiFi Code");
    //         return;
    //     }
    //     this.connectToCamera();
    // }
    HomePage.prototype.scanNetwork = function () {
        // WifiWizard2.scan()
    };
    // async connectToCamera() {
    //     console.log('Connecting to WIFI')
    //     let loading = this.loadingCtrl.create({
    //         content: 'Connecting...'
    //     });
    //     loading.present().then((result) => {
    //         this.loaderIsPresent = true;
    //     });
    //     try {
    //         let wifiReturn = await WifiWizard2.iOSConnectNetworkAsync(this.wifi, this.password);
    //         console.log("WiFi Return ->", wifiReturn);
    //         console.log( "Successfully connected to " + this.wifi );
    //         // Store WiFi code and password in storage
    //         this.storage.set('wifi',this.wifi);
    //         this.storage.set('password',this.password);
    //         setTimeout(() => {
    //             if (this.loaderIsPresent) {
    //                 loading.dismiss();
    //             }
    //             this.navCtrl.push(VideoFeedPage);
    //         }, 12000);
    //     } catch( e ){
    //         throw new Error( "Failed to connect to device WiFi SSID " + this.wifi );
    //     }
    // }
    // ***************** New Connect Function **************** //
    // ios version
    // async connect() {
    //     console.log('Connecting to WIFI')
    //     let loading = this.loadingCtrl.create({
    //         content: 'Connecting...'
    //       });
    //     // Check WiFi code
    //     if (this.wifi == '' || this.wifi == null) {
    //         alert("Please enter WiFi Code");
    //         return;
    //     }
    //     // Check Password
    //     if (this.password == '' || this.password == null) {
    //         alert("Please enter WiFi Code");
    //         return;
    //     }
    //     // Present loader
    //     loading.present();
    //     try {
    //         let httpFailed = false;
    //         // Call Connection method
    //         await this.connectToCamera();
    //         await this.timeout(12000);
    //         // this.storage.set('wifi',this.wifi);
    //         // this.storage.set('password',this.password);
    //         // Check if connection established
    //         // loading.setContent("Checking Connection...");
    //         // await this.checkConnection();
    //         // await this.timeout(2000);
    //         // navigate to Video feed page
    //         // loading.dismiss();
    //         await this.dashcamService.dvConnected().then ((resp) =>{
    //             console.log("DV is ALIVE - ", JSON.stringify(resp));
    //             // this.navCtrl.push(VideoFeedPage);
    //           }).catch((err) => {
    //             console.log("DV ALIVE command failed - ", JSON.stringify(err));
    //             // this.connectToCamera();
    //             httpFailed = true;
    //         });
    //         if (httpFailed) {
    //             loading.setContent("Connection Taking Longer...")
    //             console.log("HTTP failed status - ", httpFailed);
    //             await this.timeout(10000);
    //             console.log("Timeout Over ");
    //         }
    //         loading.dismiss();
    //         this.navCtrl.push(VideoFeedPage);
    //     } catch ( error ){
    //         loading.dismiss();
    //         console.log( 'WiFi Error / ', error.message );
    //         alert("Wifi connection Error - " + error.message )
    //     }
    // }
    // iOS Version
    // async connectToCamera() {
    //     try {
    //         console.log("Connecting WiFi");
    //         // console.log(this.wifi);
    //         // console.log(this.password);
    //         // await this.timeout(2000); 
    //         let connectingWiFi = await WifiWizard2.iOSConnectNetworkAsync(this.wifi, this.password);
    //         console.log("Connecting WiFi - Success ->", connectingWiFi);
    //         return true;
    //     } catch( e ){
    //         throw new Error( "Connecting Wifi. Failed to connect " + this.wifi );
    //     }
    // }
    HomePage.prototype.connect = function () {
        return __awaiter(this, void 0, void 0, function () {
            var loading, httpFailed, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('Connecting to WIFI');
                        loading = this.loadingCtrl.create({
                            content: 'Connecting...'
                        });
                        // Check WiFi code
                        if (this.wifi == '' || this.wifi == null) {
                            alert("Please enter WiFi Code");
                            return [2 /*return*/];
                        }
                        // Check Password
                        if (this.password == '' || this.password == null) {
                            alert("Please enter WiFi password");
                            return [2 /*return*/];
                        }
                        // Present loader
                        loading.present();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 5, , 6]);
                        httpFailed = false;
                        // Call Connection method
                        return [4 /*yield*/, this.timeout(2000)];
                    case 2:
                        // Call Connection method
                        _a.sent();
                        // this.androidWifiConfig = WifiWizard2.formatWifiConfig('"'+this.wifi+'"', this.password,'WPA');
                        // this.androidWifiConfig = WifiWizard2.formatWifiConfig(this.wifi, this.password,'WPA');
                        this.androidWifiConfig = WifiWizard2.formatWifiConfig(this.wifi, this.password, 'WPA');
                        // alert(JSON.stringify(this.androidWifiConfig));
                        // alert(this.wifi);
                        // alert(this.password);
                        // await this.add();
                        return [4 /*yield*/, this.doConnect()];
                    case 3:
                        // alert(JSON.stringify(this.androidWifiConfig));
                        // alert(this.wifi);
                        // alert(this.password);
                        // await this.add();
                        _a.sent();
                        return [4 /*yield*/, this.timeout(5000)];
                    case 4:
                        _a.sent();
                        loading.dismiss();
                        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_1__video_feed_video_feed__["a" /* VideoFeedPage */]);
                        return [3 /*break*/, 6];
                    case 5:
                        error_1 = _a.sent();
                        loading.dismiss();
                        console.log('WiFi Error / ', error_1.message);
                        alert("Wifi connection Error - " + error_1.message);
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    // async add(){
    //     await this.timeout(10000);
    //     try {
    //         await WifiWizard2.addNetworkAsync( this.androidWifiConfig );
    //         // WifiWizard2.addNetwork(this.androidWifiConfig, null,null);
    //         return true;
    //     } catch( e ) {
    //         // throw new Error( JSON.stringify(e) );
    //         throw new Error( "Failed to add device WiFi network to your mobile device! Please try again, or manually connect to the device, disconnect, and then return here and try again. - " + JSON.stringify(e));
    //     }
    // }
    HomePage.prototype.add = function () {
        return __awaiter(this, void 0, void 0, function () {
            var e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.timeout(5000)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, WifiWizard2.add(this.androidWifiConfig)];
                    case 3:
                        _a.sent();
                        return [2 /*return*/, true];
                    case 4:
                        e_1 = _a.sent();
                        throw new Error('Add Error - ' + JSON.stringify(e_1));
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.doConnect = function () {
        return __awaiter(this, void 0, void 0, function () {
            var e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.timeout(5000)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        // await WifiWizard2.androidConnectNetworkAsync(this.wifi  );
                        return [4 /*yield*/, WifiWizard2.connect(this.wifi, true, this.password, 'WPA', false)];
                    case 3:
                        // await WifiWizard2.androidConnectNetworkAsync(this.wifi  );
                        _a.sent();
                        console.log("Successfully connected to " + this.wifi);
                        return [2 /*return*/, true];
                    case 4:
                        e_2 = _a.sent();
                        // throw new Error( "Failed to connect to device WiFi SSID " + this.wifi + " Error - " + JSON.stringify(e) );
                        throw new Error('Connect error -' + JSON.stringify(e_2));
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    // Android functions
    // connect() {
    //     console.log('Connecting to WIFI')
    //     let loading = this.loadingCtrl.create({
    //         content: 'Connecting...'
    //       });
    //     // Check WiFi code
    //     if (this.wifi == '' || this.wifi == null) {
    //         alert("Please enter WiFi Code");
    //         return;
    //     }
    //     // Check Password
    //     if (this.password == '' || this.password == null) {
    //         alert("Please enter WiFi Code");
    //         return;
    //     }
    //     // Present loader
    //     // loading.present();
    //      try {
    //         let config = WifiWizard2.formatWifiConfig(this.wifi, this.password,'WPA');
    //         console.log("Connecting WiFi");
    //         // alert('WiFi - '+ this.wifi);
    //         // alert('Password - '+ this.password);
    //         // let config = WifiWizard.formatWifiConfig('MyNetworkName', 'mynetworkpassword', 'WPA');
    //         // alert('addwifi config' + config);
    //         let ssid = '"'+this.wifi+'"';
    //         alert('SSID -> ' + ssid);
    //         // loading.present();
    //         WifiWizard2.addNetwork(
    //             config,
    //             () => {
    //                      alert('Network added sucessfully')
    //                      // connect to this network
    //                     // WifiWizard2.androidConnectNetworkAsync(this.wifi).try {
    //                     //     console.log
    //                     // } catch (error) {
    //                     // }
    //                      WifiWizard2.androidConnectNetwork (
    //                      ssid,
    //                      () => {
    //                             // loading.dismiss;
    //                             alert('Network connected sucessfully')
    //                             this.navCtrl.push(VideoFeedPage)
    //                            },
    //                      () => {
    //                             // loading.dismiss;
    //                             alert('Network Connection Error')
    //                             }
    //                      )
    //                 },
    //             () => alert('Network Add error')
    //         );
    //         // WifiWizard2.connect(this.wifi, true, this.password, {SSID: this.wifi, algorithm: 'WEP', password: this.password})
    //         // loading.dismiss();
    //         // alert('Connection sucessfull');
    //         // this.navCtrl.push(VideoFeedPage)
    //         // return true;
    //     } catch( e ){
    //         // loading.dismiss();
    //         alert('Failed to connect WiFi');
    //         throw new Error( "Connecting Wifi. Failed to connect " + this.wifi );
    //     }
    // }
    HomePage.prototype.checkConnection = function () {
        return __awaiter(this, void 0, void 0, function () {
            var connectedWiFi, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        console.log("Checking Connection");
                        return [4 /*yield*/, WifiWizard2.getConnectedSSIDAsync()];
                    case 1:
                        connectedWiFi = _a.sent();
                        console.log("Checking Connection - Success ->", connectedWiFi);
                        return [2 /*return*/, true];
                    case 2:
                        e_3 = _a.sent();
                        throw new Error("Checking Connection. Failed to connect " + this.wifi);
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.timeout = function (delay) {
        return new Promise(function (resolve, reject) {
            setTimeout(resolve, delay);
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"F:\01_Project\2019-04\04_YoSpot\03_Source\YoSpot\src\pages\home\home.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    \n    <ion-title text-center>\n     Connect to Wifi\n    </ion-title> \n  </ion-navbar>\n</ion-header>\n\n<ion-content style="background-color: #2e2e2e" padding>\n  <ion-slides no-padding pager >\n    <ion-slide style="background-color: #2e2e2e" >\n      <ion-card class="card" >\n          <h2 style="margin-top: 1.5em;text-align: center;">Scan QR Code in the back of your device to Connect Wi-Fi</h2>\n          <img src="./assets/imgs/QR_image.png" class="qrimg"/>\n          <div class="divider"></div>\n          <button ion-button class="connectBtn" large style="margin-top:1em;" (tap)="qrScan()">\n            <ion-icon name="md-checkmark" item-end>&nbsp;&nbsp;SCAN</ion-icon>          \n          </button>\n      </ion-card>\n    </ion-slide>\n    <ion-slide style="background-color: #2e2e2e">\n        <ion-card class="cardTut" >\n            <h1 style="margin-top: .5em;text-align: center; font-weight: bold;">Tutorial</h1>\n            <video controls="controls"  poster="./assets/imgs/poster.png" class="videoPlayer">\n                <source src="./assets/videos/YoSpotVideo.mov" type="video/mp4" />\n            </video>\n\n        </ion-card>\n    </ion-slide>\n  </ion-slides> \n</ion-content>\n\n  <!-- <div class="divider"></div>\n  <button ion-button class="connectBtn" large style="margin-top:1em;" (tap)="qrScan()">\n    <ion-icon name="md-checkmark" item-end>&nbsp;&nbsp;START</ion-icon>          \n  </button> -->\n\n  <!-- <ion-card class="card" >\n    <ion-list style="width:80%; margin: auto;">\n      <h2 style="font-weight: bold;margin-top:1em; margin-bottom:2em;text-align: center;">WiFi Network and Password</h2>\n      <ion-item class="item">\n        <ion-input [disabled]=true placeholder="WiFi Network" [(ngModel)]="wifi"></ion-input>\n      </ion-item>\n      <ion-item class="item">\n          <ion-input [disabled]=true placeholder="Password" [(ngModel)]="password"></ion-input>\n        </ion-item>\n    </ion-list>\n\n    <button ion-button [disabled]="wifi == null" class="connectBtn" large style="margin-top:1em;" (tap)="connect()">\n      Connect Camera\n    </button>\n  </ion-card> -->\n\n  <!-- <button ion-button block class="tutorialBtn" (tap)="tutorial()">\n    Tutorial\n  </button> -->'/*ion-inline-end:"F:\01_Project\2019-04\04_YoSpot\03_Source\YoSpot\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
            __WEBPACK_IMPORTED_MODULE_0__providers_dashcam_service__["a" /* DashcamService */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["e" /* LoadingController */]])
    ], HomePage);
    return HomePage;
}());

// async connectToCamera() {
//     console.log('Connecting to WIFI')
//     let loading = this.loadingCtrl.create({
//         content: 'Connecting...'
//       });
//     loading.present().then((result) => {
//     this.loaderIsPresent = true;
//     });
//     try {
//         let wifiReturn = await WifiWizard2.iOSConnectNetworkAsync(this.wifi, this.password);
//         console.log("WiFi Return ->", wifiReturn);
//         if (wifiReturn == undefined){
//             console.log("Could not connect to Wi Fi");
//             loading.dismiss();
//             return;
//         }
//         // await WifiWizard2.getConnectedSSIDAsync()
//         console.log( "Successfully connected to " + this.wifi );
//         // return true;
//         // async setInterval(() => {
//         //    let lssid = await WifiWizard2.getConnectedSSIDAsync()
//         //    console.log("SSID found - ", lssid );
//         //    setInterval(() => {
//         //         if (lssid != null) {
//         //             console.log("Got SSID - ", lssid );
//         //             if (this.loaderIsPresent) {
//         //                 loading.dismiss();
//         //             }
//         //             this.navCtrl.push(VideoFeedPage);  
//         //         } else {
//         //             console.log("NO SSID yet - ", lssid );
//         //         }
//         //     }, 1000)
//         // Store WiFi code and password in storage
//         this.storage.set('wifi',this.wifi);
//         this.storage.set('password',this.password);
//         setTimeout(() => {
//             if (this.loaderIsPresent) {
//                 loading.dismiss();
//             }
//             this.navCtrl.push(VideoFeedPage);
//         }, 12000);
//     } catch( e ){
//         throw new Error( "Failed to connect to device WiFi SSID " + this.wifi );
//     }
//   }
// this.platform.ready().then(() => {
// WifiWizard2.iOSConnectNetwork(this.wifi, this.password, success, fail);
// let success = () => {
//     console.log("Connection sucessful");
//     setTimeout(() => {
//         this.navCtrl.push(VideoFeedPage);
//     }, 5000);
// }
// if (typeof WifiWizard !== 'undefined') {
//         console.log("WifiWizard loaded: ");
//         // console.log(WifiWizard);
// } else {
//     console.warn('WifiWizard not loaded.');
// }  
// let success = () => {
//     console.log("Connection sucessful");
//     setTimeout(() => {
//         this.navCtrl.push(VideoFeedPage);
//     }, 5000);
// }
// let fail = () => {
//     // console.log("" + e);
//     console.log("Connection failed");
// }
// let listHandler = (a: any) => {
//     this.networks = [];
//     for (let x in a) {                    
//         console.log(a[x].SSID + ", " + a[x].BSSID + ", " + a[x].level);  
//         this.networks.push({
//             ssid: a[x].SSID,
//             bssid: a[x].BSSID,
//             level: a[x].level});                
//     }  
// }
// WifiWizard2.scan(successNetwork, failNetwork);
//  startScan(successNetwork, failNetwork);
// <ion-list>
//     <ion-item *ngFor = "let network of networks">
//       {{network.ssid}}
//     </ion-item>
//   </ion-list>
//# sourceMappingURL=home.js.map

/***/ })

},[380]);
//# sourceMappingURL=main.js.map