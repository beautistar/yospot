import { AlertController } from 'ionic-angular';
import { Injectable} from '@angular/core';


@Injectable()
export class UtilityService {
  constructor( private alertCtrl: AlertController) {
      
  }

  showErrorAlert(header = null, msg = null) {
    let title = 'Error';
    let subTitle = 'Something happened. Please try again';
    if (header) {
      title = header
    }
    if (msg) {
      subTitle = msg;
    }
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

  showAlert(header = null, msg = null) {
    let title = 'Success';
    let subTitle = 'Successfully completed';
    if (header) {
      title = header
    }
    if (msg) {
      subTitle = msg;
    }
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();

  }

  

}