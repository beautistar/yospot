import { UtilityService } from './../../providers/utility-service';
import { Component } from '@angular/core';
import {  NavController, NavParams, ViewController, AlertController, LoadingController} from 'ionic-angular';
import { DashcamService } from '../../providers/dashcam-service';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
declare let WifiWizard2: WifiWizard2;
import { Storage } from '@ionic/storage';
// import { VideoFeedPage } from './../video-feed/video-feed';
import * as moment from 'moment';

import xml2js from 'xml2js';
/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  recordingResolution;
  pictureQuality;
  scanInProgress: boolean = false;
  options: BarcodeScannerOptions;

  // Wi Fi setting
  wifi: string = ''; // 'WIFI_CARDV77fc';
  password: string = ''; //'12345678';
  loaderIsPresent = false;

  // Camera Settings
  camSettings = {
      photo: ' ',
      resolution: ' ',
      audio: 0,
      collision: ' ',
      download: false,
      capacity: 0,
      recordingTime: ' ',
      movieDate: 0,
  }
  // camSettings: {
        // photo: String = '';
        // resolution: String = '';
        // audio: Number = 0;
        // collision: String = '';
        // downloadFlag = false;
        // capacity = 0;
    // }

  
  constructor(public navCtrl: NavController, 
    public viewCtrl: ViewController, 
    public navParams: NavParams,
    public dashcamService: DashcamService,
    public utilityService: UtilityService,
    private barcode: BarcodeScanner,
    private storage: Storage,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController) {
      

    }

  ionViewWillEnter(){
    console.log("Inside Setting Page");
    // console.log("Settings Object BEFORE -> ", JSON.stringify(this.camSettings));
    // console.log("BEFORE this.wifi ->", this.wifi);
    // console.log("BEFORE this.password ->", this.password);  

   this.viewCtrl.setBackButtonText('');
   // Get current Camera Settings
   this.dashcamService.getSettings().then ((data) => {
      console.log("Settings Data ->", JSON.stringify(data));

      this.parseSettingsXML(data).then((cmdArray: any) => {
      console.log("PARSE ARRAY ->", JSON.stringify(cmdArray));

      cmdArray.forEach((item: any) => {
        switch (item.cmd) {
          case '1002':
          console.log("1002 photo  - ", item.status);
          
            this.camSettings.photo = item.status;
            break;
          case '2002':
          console.log("2002 resolution - ", item.status);
          
            this.camSettings.resolution = item.status;
            break; 
          case '2011':
          console.log("2011 collision status - ", item.status);
          
            this.camSettings.collision = item.status;
            break;       
          case '2007':
            console.log("2007 audio status - ", item.status);
          
            this.camSettings.audio = Number(item.status);
            break;  
          case '2003':
            console.log("2003 recording status - ", item.status);
            this.camSettings.recordingTime = item.status;
            break; 
          case '2008':
            console.log("2008 movie date - ", item.status);
            this.camSettings.movieDate = Number(item.status);
            break;                        
          default:
            break;
        }
        // console.log("Settings Object AFTER -> ", JSON.stringify(this.camSettings));
      }).catch((err) => console.log("XML error /", err));

      // console.log("Settings Object AFTER -> ", JSON.stringify(this.camSettings));
      
    });
  }).catch ((err) => console.log("Get Settings Error / ", err));

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

  async qrScan(){
    if (this.scanInProgress) {
      return;
    } else {
      this.scanInProgress = true;
    }
    this.options = {
      prompt: 'Scan code for info',
      preferFrontCamera: false,
      showTorchButton: true,
      disableSuccessBeep: true,
      disableAnimations: false
    }
   await this.barcode.scan(this.options).then((barcodeData) => {
          // Success! Barcode data is here
          this.scanInProgress = false;
          console.log('barcodeData text - ',JSON.stringify(barcodeData.text));
          if (barcodeData.text == null || barcodeData.text == "" ) {        
          } else {
              console.log('Connect to WiFi');
              // this.connect();
              alert(barcodeData.text)
          }
      }, (err) => {
          // An error occurred
          this.scanInProgress = false;
          alert(JSON.stringify(err));
      });
  }

  async connect() {
    // console.log('Connecting to WIFI')
    let loading = this.loadingCtrl.create({
        content: 'Connecting...'
      });

    // Present loader
    loading.present();

    try {
        // Call Connection method
        await this.connectToCamera();
        await this.timeout(12000);

        // this.storage.set('wifi',this.wifi);
        // this.storage.set('password',this.password);

        // Check if connection established
        loading.setContent("Checking Connection...");

        // navigate to Video feed page
        loading.dismiss();
        // this.navCtrl.push(VideoFeedPage);
        this.navCtrl.pop();
    } catch ( error ){
        loading.dismiss();
        console.log( 'WiFi Error / ', error.message );
    }
}

async connectToCamera() {
  try {
      console.log("Connecting WiFi");
      let connectingWiFi = await WifiWizard2.iOSConnectNetworkAsync(this.wifi, this.password);
      console.log("Connecting WiFi - Success ->", connectingWiFi);
      return true;
  } catch( e ){
      throw new Error( "Connecting Wifi. Failed to connect " + this.wifi );
  }
}

  setResolution() {
 
    console.log('setResolution triggered - ', this.camSettings.resolution);
    // let resolution: any;

    let alert = this.alertCtrl.create();
    alert.setTitle('Resolution Setting');
    let checked0 = false, checked1 = false, checked2=false, checked3 = false;

    switch (this.camSettings.resolution) {
      case '0':
        checked0 = true;
        break;
      case '1':
        checked1 = true;
        break;
      case '2':
        checked2 = true;
        break;
      case '3':
        checked3 = true;
        break;   
      default:
        break;
    }

    alert.addInput({
      type: 'radio',
      label: '1920 x 1080',
      value: '0',
      checked: checked0
    });
    alert.addInput({
      type: 'radio',
      label: '1280 x 720',
      value: '1',
      checked: checked1
    });
    alert.addInput({
      type: 'radio',
      label: '854 x 480',
      value: '2',
      checked: checked2
    });
    alert.addInput({
      type: 'radio',
      label: '640 x 480',
      value: '3',
      checked: checked3
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        // this.testRadioOpen = false;
        console.log("Resolution Ok -> ", data)
        if (data != '') {
          this.camSettings.resolution = data;
          this.dashcamService.setResolution(data).then ((resp) =>{
            console.log("Resolution updated - ", JSON.stringify(resp));
          }).catch((err) => {
            console.log("Resolution update error - ", JSON.stringify(err));
        });
        }
      }
    });
    alert.present();
  }

  setPhotoQuality() {
    console.log('setPhotoQuality triggered, photo -> ', this.camSettings.photo);
    let alert = this.alertCtrl.create();
    alert.setTitle('Picture Quality');

    let checked0 = false, checked1 = false, checked2=false, checked3 = false, checked4 = false, checked5 = false;
    
    switch (this.camSettings.photo) {
      case '0':
        checked0 = true;
        break;
      case '1':
        checked1 = true;
        break;
      case '2':
        checked2 = true;
        break;
      case '3':
        checked3 = true;
        break;
      case '4':
        checked4 = true;
        break;  
      case '5':
        checked5 = true;
        break;     
      default:
        break;
    }

    alert.addInput({
      type: 'radio',
      label: '12M',
      value: '0',
      checked: checked0
    });
    alert.addInput({
      type: 'radio',
      label: '10M',
      value: '1',
      checked: checked1
    });
    alert.addInput({
      type: 'radio',
      label: '8M',
      value: '2',
      checked: checked2
    });
    alert.addInput({
      type: 'radio',
      label: '5M',
      value: '3',
      checked: checked3
    });
    alert.addInput({
      type: 'radio',
      label: '3M',
      value: '4',
      checked: checked4
    });
    alert.addInput({
      type: 'radio',
      label: 'VGA',
      value: '5',
      checked: checked5
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        // this.testRadioOpen = false;
        console.log("Picture Quality Ok -> ", data)
        if (data != '') {
          this.camSettings.photo = data;
          this.dashcamService.setPhotoQuality(data).then ((resp) =>{
            console.log("Photo Quality updated - ", JSON.stringify(resp));
          }).catch((err) => {
            console.log("Photo Quality update error - ", JSON.stringify(err));
        });
        }
      }
    });
    alert.present();

  }

  // setRecordingTime() {
  //   console.log('setRecordingTime triggered');
  //   this.dashcamService.movieCapacity()
  //   .then((movieCapacityData) => {
  //     console.log('Video Capacity -> ', movieCapacityData);
  //     this.parseXML(movieCapacityData).then((movieCapacity: any)=> {
  //       let mcap = movieCapacity / 3600;
  //       let mcapStr = mcap.toFixed(2);
  //       let msg = mcapStr + ' Hrs';
  //       alert(msg)
  //     }).catch ((error) => {
  //       console.log('Video Capacity Parse Error / ', error);      
  //     })
  //   }).catch ((error) => {
  //     console.log('Movie Capacity Error / ', error); 
  //   })
  // }

  setRecordingTime(parm) {
    console.log('setRecordingTime triggered');

    let alert = this.alertCtrl.create();
    alert.setTitle('Recording Time');
    let checked0 = false, checked1 = false, checked2=false, checked3 = false, checked4 = false;

    switch (this.camSettings.recordingTime) {
      case '0':
        checked0 = true;
        break;
      case '1':
        checked1 = true;
        break;
      case '2':
        checked2 = true;
        break;
      case '3':
        checked3 = true;
        break;   
      case '4':
        checked4 = true;
        break;   
      default:
        break;
    }

    alert.addInput({
      type: 'radio',
      label: 'Off',
      value: '0',
      checked: checked0
    });
    alert.addInput({
      type: 'radio',
      label: '3 Min',
      value: '1',
      checked: checked1
    });
    alert.addInput({
      type: 'radio',
      label: '5 Min',
      value: '2',
      checked: checked2
    });
    alert.addInput({
      type: 'radio',
      label: '10 Min',
      value: '3',
      checked: checked3
    });
    alert.addInput({
      type: 'radio',
      label: 'Max',
      value: '4',
      checked: checked4
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        console.log("Recording Time Ok -> ", data)
        if (data != '') {
          this.camSettings.recordingTime = data;
          this.dashcamService.setRecordingTime(data).then ((resp) =>{
            console.log("Recording Time updated - ", JSON.stringify(resp));
          }).catch((err) => {
            console.log("Recording Time update error - ", JSON.stringify(err));
        });
        }
      }
    });
    alert.present();
  }

  setCollision() {
    console.log('setCollision triggered - ', this.camSettings.collision);
    let alert = this.alertCtrl.create();
    alert.setTitle('Colission Sensitivity');
    let checked0 = false, checked1 = false, checked2=false, checked3 = false, checked4 = false;
    
    switch (this.camSettings.collision) {
      case '0':
        checked0 = true;
        break;
      case '1':
        checked1 = true;
        break;
      case '2':
        checked2 = true;
        break;
      case '3':
        checked3 = true;
        break;
      case '4':
        checked4 = true;
        break;     
      default:
        break;
    }
    alert.addInput({
      type: 'radio',
      label: 'Off',
      value: '0',
      checked: checked0
    });
    alert.addInput({
      type: 'radio',
      label: 'Low',
      value: '1',
      checked: checked1
    });
    alert.addInput({
      type: 'radio',
      label: 'Medium',
      value: '2',
      checked: checked2
    });
    alert.addInput({
      type: 'radio',
      label: 'High',
      value: '3',
      checked: checked3
    });
    alert.addInput({
      type: 'radio',
      label: 'Maximum',
      value: '4',
      checked: checked4
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        // this.testRadioOpen = false;
        console.log("Collision Ok -> ", data)
        if (data != '' && data != null) {
          this.camSettings.collision = data;
          this.dashcamService.setCollision(Number(data)).then ((resp) =>{
            console.log("Collision updated - ", JSON.stringify(resp));
          }).catch((err) => {
            console.log("Collision update error - ", JSON.stringify(err));
        });
        }
      }
    });
    alert.present();
  }

  /* own */
  muteAudio1(){
    console.log('toggleAudio triggered audio -> ', this.camSettings.audio);
      let data;
      if (this.camSettings.audio == 0 ) {
          data = 1;
      }
      else{
        data = 0;
      }
      this.camSettings.audio = data;
      this.dashcamService.muteAudio(data).then ((resp) =>{
        console.log("Audio updated - ", JSON.stringify(resp));
      }).catch((err) => {
        console.log("Audio update error - ", JSON.stringify(err));
    });
  }

  muteAudio() {
    console.log('toggleAudio triggered audio -> ', this.camSettings.audio);
    // if (this.camSettings.audio == 0 ) {
    //   this.dashcamService.muteAudio(1);
    // } else {
    //   this.dashcamService.muteAudio(0);
    // }

    let alert = this.alertCtrl.create();
    alert.setTitle('Audio Setting');
    let checkedMute = false, checkedUnmute = false;

    switch (this.camSettings.audio) {
      case 0:
        checkedUnmute = true;
        break;
      case 1:
        checkedMute = true;
        console.log("CheckedMute in Switch = ", checkedMute);
        break;  
      default:
        break;
    }

    console.log("CheckMute = ", checkedMute);

    alert.addInput({
      type: 'radio',
      label: 'Mute',
      value: '1',
      checked: checkedMute
    });
    alert.addInput({
      type: 'radio',
      label: 'Unmute',
      value: '0',
      checked: checkedUnmute
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        console.log("Pressed Ok -> ", data)
        if (data != '' && data != null) {
          this.camSettings.audio = Number(data);
          this.dashcamService.muteAudio(data).then ((resp) =>{
            console.log("Audio updated - ", JSON.stringify(resp));
          }).catch((err) => {
            console.log("Audio update error - ", JSON.stringify(err));
        });
        }
      }
    });
    alert.present();
  }

  movieDate() {
    console.log('movieDate triggered -> ', this.camSettings.movieDate);

    let alert = this.alertCtrl.create();
    alert.setTitle('Movie Date Setting');
    let enableDate = false, disableDate = false;

    switch (this.camSettings.movieDate) {
      case 0:
        disableDate = true;
        break;
      case 1:
        enableDate = true;
        console.log("enableDate in Switch = ", enableDate);
        break;  
      default:
        break;
    }

    console.log("EnableDate = ", enableDate);

    alert.addInput({
      type: 'radio',
      label: 'Enable',
      value: '1',
      checked: enableDate
    });
    alert.addInput({
      type: 'radio',
      label: 'Disable',
      value: '0',
      checked: disableDate
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        console.log("Pressed Ok -> ", data)
        if (data != '' && data != null) {
          console.log('update movie date status to ', data);
          this.camSettings.movieDate = Number(data);
          this.dashcamService.movieDate(this.camSettings.movieDate).then ((resp) =>{
            console.log("movie date updated - ", JSON.stringify(resp));
          }).catch((err) => {
            console.log("movie date update error - ", JSON.stringify(err));
        });
        }
      }
    });
    alert.present();
  }

  changeWifiNetwork() {

    // console.log('Connecting to WIFI')
    let loading = this.loadingCtrl.create({
      content: 'Changing WiFi...'
    });

    // get current wifi / password
    this.storage.get('wifi')
    .then((wdata) => {
        this.wifi = wdata;

        this.storage.get('password')
        .then((pdata) => {
            this.password = pdata;
        // this.dashcamService.getWIFI().then ((data) => {
        //   // parse XML
        //   console.log("getWIFI data ->", JSON.stringify(data));
        //   this.parseXMLWifi(data).then ((key) => {
        //     console.log("AFTER this.wifi ->", this.wifi);
        //     console.log("AFTER this.password ->", this.password);  
        
            let alert = this.alertCtrl.create({
              title: 'WiFi Network and Password',
              inputs: [
                {
                  name: 'wifiname',
                  placeholder: 'Network Name',
                  value: this.wifi
                },
                {
                  name: 'wifipassword',
                  placeholder: 'Password',
                  value: this.password
                }
              ],
              buttons: [
                {
                  text: 'Cancel',
                  role: 'cancel',
                  handler: data => {
                    console.log('Cancel clicked');
                  }
                },
                {
                  text: 'Change',
                  handler: data => {
                    console.log('data - ',data.wifiname, ' / ', data.wifipassword);
                    // Check if thre is any change in network or password
                    if (this.wifi == data.wifiname && this.password == data.wifipassword) {

                      return;
                    }

                    this.wifi = data.wifiname;
                    this.password = data.wifipassword;
                    console.log('this.wifi',this.wifi, ' | this.password',this.password);

                    let confMsg = '<p>Network: ' + this.wifi + '</p><p>Password: ' + this.password + '</p>';
                    let calert = this.alertCtrl.create({
                      title: 'Do you want to change?',
                      message: confMsg,
                      buttons: [
                        {
                          text: 'No',
                          role: 'cancel',
                          handler: () => {
                            console.log('Cancel clicked');
                          }
                        },
                        {
                          text: 'Yes',
                          handler: () => {

                            // Present loader
                            loading.present();

                            // Change WIFI network and Password then reconnect
                            this.dashcamService.setSSID(this.wifi).then((data) => {
                              console.log("Network change sucessfull",JSON.stringify(data));
                              this.dashcamService.setPassphrase(this.password).then((data) => {
                                console.log("Password change sucessfull",JSON.stringify(data));

                                // this.dashcamService.startWifiMode().then((data) => {
                                //   console.log("WIFI mode started",JSON.stringify(data));
                                  // this.dashcamService.sendWifiToDevice(this.wifi,this.password).then((data) => {
                                  //   console.log("Sent Wifi and password to device",JSON.stringify(data));
                                    // this.dashcamService.selectWifiStationMode().then((data) => {
                                    //   console.log("WIFI station mode selected ",JSON.stringify(data));   
                                      this.dashcamService.reconnectWifi().then((data) => {
                                        console.log("Re-connect sucessfull",JSON.stringify(data));
                                        // this.dashcamService.stopWifiMode().then((data) => {
                                        //   console.log("WIFI mode stopped and normal operation started",JSON.stringify(data));
                                          // Store wifi and pwd in Storage
                                          this.storage.set('wifi',this.wifi);
                                          this.storage.set('password',this.password);
                                          this.dashcamService.videoEntry = 'new';
                                          this.timeout(10000).then (() => {
                                            loading.dismiss();
                                            this.navCtrl.popToRoot(); 
                                          })

                                          // Now connect using the new wifi and password
                                          // this.connect();
                                          
                                          // this.navCtrl.pop();
                                        // }).catch ((err) => {
                                        //   console.log("Stop WIFI error", JSON.stringify(err));
                                        // })
                                      }).catch ((err) => {
                                        console.log("Reconnect Error", JSON.stringify(err));
                                      })
                                    // }).catch ((err) => {
                                    //   console.log("WIFI Station Error", JSON.stringify(err));
                                    // })
                                  // }).catch ((err) => {
                                  //   console.log("Wifi and pass Sent Error", JSON.stringify(err));
                                  // })
                                // }).catch ((err) => {
                                //   console.log("Wifi mode start error", JSON.stringify(err));
                                // })
                              }).catch ((err) => {
                                console.log("Change Password Error", JSON.stringify(err));
                              })
                            }).catch ((err) => {
                              console.log("Network Change Error", JSON.stringify(err));
                            })
                          }
                        }
                      ]
                    });
                    calert.present();
                  }
                }
              ]
            });
            alert.present();
        //   }).catch ((err) => {
        //     console.log("error parsing XML ", JSON.stringify(err));
        //   });
        // }).catch((err) => {
        //     console.log("error reading current WIFI ", JSON.stringify(err));
        // });
        }).catch((err) => {
            console.log("error reading password from storage");
        });
    }).catch((err) => {
        console.log("error reading wifi from storage");
    });

    // this.storage.get('password')
    // .then((data) => {
    //     this.password = data;
    //     console.log("Wifi ->", this.wifi," / Password ->", this.password);
    // }).catch((err) => {
    //     console.log("error reading user name from storage");
    // });

    
  }

  changeWifiPassword() {


  }
  download1(){
    let downloadchk;
     if(this.camSettings.download){
       downloadchk = true;
     }else{
      downloadchk = false;
     }
     this.storage.set('download', downloadchk);
    console.log('download triggered' + this.camSettings.download);
  }

  download() {
    console.log('download triggered');

    let alert = this.alertCtrl.create({
      title: 'File Download',
      message: 'Allow Downloads?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            this.camSettings.download = false;
            this.storage.set('download',false);
            console.log('No clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.camSettings.download = true;
            this.storage.set('download',true);
            console.log('Yes clicked');
          }
        }
      ]
    });
    alert.present();
  }

  formatCard() {
    console.log("format Card triggered");
    let alert = this.alertCtrl.create({
      title: 'Format Card',
      message: 'Are you sure? This will delete all files.',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.dashcamService.formatCard();
          }
        }
      ]
    });
    alert.present();
  }

  resetSystem() {
    console.log("System reset triggered");
    let alert = this.alertCtrl.create({
      title: 'System Reset',
      message: 'Are you sure? This will reset All Settings.',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.dashcamService.resetSystem().then ((resp) =>{
              console.log("System Reset Sucessfull - ", JSON.stringify(resp));
            }).catch((err) => {
              console.log("System Reset error - ", JSON.stringify(err));
          });;
          }
        }
      ]
    });
    alert.present();
  }

  cardCapacity(){
    console.log("card capacity clicked");
    this.dashcamService.cardCapacity()
    .then((capacityData) => {
      console.log('Capacity -> ', capacityData);
      this.parseXML(capacityData).then((capacity: any)=> {
        let cap = capacity / 1024 / 1024 /1024;
        let capStr = cap.toFixed(2) + ' GB';

        // get the video time 
        this.dashcamService.movieCapacity()
        .then((movieCapacityData) => {
          console.log('Video Capacity -> ', movieCapacityData);
          this.parseXML(movieCapacityData).then((movieCapacity: any)=> {
            // let mcap = movieCapacity / 3600;
            // let mcapStr = mcap.toFixed(2);
            // let msg = capStr +' / '+ mcapStr + ' Hrs';
            let mcapHrs = Math.floor(movieCapacity / 3600);
            let mcapMin = Math.floor((movieCapacity - (mcapHrs * 3600)) / 60);
            let mcapSec = movieCapacity - (mcapHrs * 3600) - (mcapMin * 60)
            // let mcapStr = mcap.toFixed(2);
            let msg = capStr +' / '+ mcapHrs + ' Hrs, ' + mcapMin + ' Mins, ' + mcapSec + ' Secs';

            alert(msg)
          }).catch ((error) => {
            console.log('Video Capacity Parse Error / ', error);      
          })

          
        }).catch ((error) => {
          console.log('Movie Capacity Error / ', error); 
        })

       
      }).catch ((error) => {
        console.log('Disk Capacity Parse Error / ', error);      
      })
    }).catch ((error) => console.log("Disk Capacity Error / ", error))
  }

  setupDateTime(){
    console.log("setup date triggered")
    let alert = this.alertCtrl.create({
      title: 'Reset Camera Date/Time',
      message: 'Would you like to set Current Date?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            let ldate = moment(new Date()).format('YYYY-MM-DD');
            let ltime =  moment(new Date()).format('hh:mm:ss');
            console.log("Date ->", ldate);
            console.log("Time ->", ltime);
            this.dashcamService.setupDate(ldate,ltime);
          }
        }
      ]
    });
    alert.present();
  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Confirm purchase',
      message: 'Do you want to buy this book?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Buy',
          handler: () => {
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  }

  parseXML(data)
  {
     return new Promise(resolve =>
     {
        var parser = new xml2js.Parser(
            {
               trim: true,
               explicitArray: true
            });

        parser.parseString(data, function (err, result)
        {
          var obj = result.Function;
          var val = obj.Value[0];
          console.log("Object -> ", JSON.stringify(obj));
          console.log("value ->", obj.Value[0]);
          resolve(val);
        });
     });
  }

  parseXMLWifi(data)
  {
     return new Promise(resolve =>
     {
        console.log("inside parseXMLWifi");
        var parser = new xml2js.Parser(
            {
               trim: true,
               explicitArray: true
            });

        parser.parseString(data, function (err, result)
        {
          var obj = result.LIST;

          this.wifi = obj.SSID;
          this.password = obj.PASSPHRASE;
          console.log("Object -> ", JSON.stringify(obj));
          // console.log("ssid ->", JSON.stringify(obj.SSID[0]));
          // console.log("passphrase ->", JSON.stringify(obj.PASSPHRASE[0]));
          console.log("n ssid ->", JSON.stringify(obj.SSID));
          console.log("n passphrase ->", JSON.stringify(obj.PASSPHRASE));
          resolve(true);
        });
     });
  }

  timeout(delay) {
    return new Promise(function(resolve, reject) {
        setTimeout(resolve, delay);
    });
}


  // Parse CML data for Files and convert into an Array
  parseSettingsXML(data)
  {
     return new Promise(resolve =>
     {
        // var k,l,
        var 
        arr    = [],
        parser = new xml2js.Parser(
        {
            trim: true,
            explicitArray: true
        });

        parser.parseString(data, function (err, result)
        {
          var obj = result.Function;
          // console.log("XML Object -> ", JSON.stringify(obj));

          arr.push({
            cmd           : obj.Cmd[0],
            status        : obj.Status[0],
          });
    
          arr.push({
            cmd           : obj.Cmd[3],
            status        : obj.Status[3],
          });

          arr.push({
            cmd           : obj.Cmd[4],
            status        : obj.Status[4],
          });

          arr.push({
            cmd           : obj.Cmd[13],
            status        : obj.Status[13],
          });

          arr.push({
            cmd           : obj.Cmd[14],
            status        : obj.Status[14],
          });

          arr.push({
            cmd           : obj.Cmd[15],
            status        : obj.Status[15],
          });
          // console.log("Array ", JSON.stringify(arr));
          resolve(arr);
        });
            
     });
  }


}
