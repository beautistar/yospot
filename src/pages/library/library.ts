import { FullscreenPicturePage } from './../fullscreen-picture/fullscreen-picture';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController  } from 'ionic-angular';
import { DashcamService } from '../../providers/dashcam-service'
import {  LoadingController } from 'ionic-angular';
import * as moment from 'moment';
// import { IonicSwipeAllModule } from 'ionic-swipe-all';
import { PhotoLibrary } from '@ionic-native/photo-library';
// import { HTTP } from '@ionic-native/http';
import {File} from '@ionic-native/file';
import xml2js from 'xml2js';
// import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the LibraryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-library',
  templateUrl: 'library.html',
})
export class LibraryPage {
   currVideoIndex;
   currPictureIndex;
   videoCount;
   pictureCount;
   videoFiles = [];
   videoFilesSource = [];
   filterVideoFilesSource = [];
   pictureFiles = [];
   pictureFilesSource = [];
   filterPictureFilesSource = [];
   filesType: any;
   loaderIsPresent = false;
   loading: any;
   searchDate: any;


  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public dashcamService: DashcamService, 
    private loadingCtrl: LoadingController,  
    private modalCtrl: ModalController, 
    private photoLibrary: PhotoLibrary,  
    // private http: HTTP,
    private transfer: FileTransfer, 
    private file: File,
    private sanitizer: DomSanitizer,
    public viewCtrl: ViewController) {
      
      this.filesType = 'videos';
      console.log("Inside Library Page");

      this.photoLibrary.requestAuthorization().then(() => {
        console.log("Got photo access");
      }).catch(err => console.log('Photo Gallery permissions weren\'t granted'));

  }


  ionViewWillEnter(){
    this.viewCtrl.setBackButtonText('');
    this.loading = this.loadingCtrl.create({
      content: 'Loading Videos...'
    });
    // Android
    this.loading.present().then((result) => {
      this.loaderIsPresent = true;
    });

    // change mode to playback and get files
    this.dashcamService.changeCameraMode('PLAYBACK')
    .then ((data) => {
      console.log("Camera Mode Changed to PLAYBACK - ",JSON.stringify(data));
      // alert("camera mode changed to PLAYBACK. Getting Videos");
      this.getVideoFiles();
    }).catch (error => console.log("Playback Mode Change Error / ", error)); 


  }

  // ionViewDidEnter(){
  //   this.getPictureFiles();
  // }



  openCalender(){
    
  }

  filter(date) {
    // if (this.filesType == 'videos') {
      // Filter Videos
      this.videoFiles = [];
      console.log("Date selected ->", moment(date).format('DD/MM/YYYY'));
      if (date == null || date == '') {
        this.videoFiles = this.videoFilesSource.slice(0, 5);
        this.currVideoIndex = 5;
        this.videoCount = this.videoFilesSource.length;
      } else {
        // console.log(JSON.stringify(this.videoFilesSource));
        this.videoFilesSource = [];
        this.filterVideoFilesSource.forEach(video => {
          if (moment(date).format('DD/MM/YYYY') == moment(video.dateTime).format('DD/MM/YYYY')) {
            console.log('\nDate Match in File ->',String(moment(video.dateTime).format('DD/MM/YYYY')));
            this.videoFilesSource.push(this.sanitizer.bypassSecurityTrustResourceUrl(video));
          }
          // console.log('\nDate in File ->',String(moment(file.dateTime).format('DD/MM/YYYY')));
        });
        this.videoFiles = this.videoFilesSource.slice(0, 5);
        this.currVideoIndex = 5;
        this.videoCount = this.videoFilesSource.length;
        console.log("Found ", this.videoFilesSource.length, " MOVIES");
      
        // console.log("VIDEO FILES ----->> ", JSON.stringify(this.videoFiles));
      }
    // } else {

      // Filter Pictures
      this.pictureFiles = [];
      console.log("Date selected ->", moment(date).format('DD/MM/YYYY'));
      if (date == null || date == '') {
        this.pictureFiles = this.pictureFilesSource.slice(0, 5);
        this.currPictureIndex = 5;
        this.pictureCount = this.pictureFilesSource.length;
      } else {
        this.pictureFilesSource = [];
        this.filterPictureFilesSource.forEach(picture => {
          if (moment(date).format('DD/MM/YYYY') == moment(picture.dateTime).format('DD/MM/YYYY')) {
            console.log('\nDate Match in File ->',String(moment(picture.dateTime).format('DD/MM/YYYY')));
            this.pictureFilesSource.push(picture);
          }
          this.pictureFiles = this.pictureFilesSource.slice(0, 5);
          this.currPictureIndex = 5;
          this.pictureCount = this.pictureFilesSource.length;
          console.log("Found ", this.pictureFilesSource.length, " PICTURES");
          // console.log("PICTURE FILES ----->> ", this.videoFiles);
          // console.log('\nDate in File ->',String(moment(file.dateTime).format('DD/MM/YYYY')));
        });
      }
    // }
  }

  showPicture(filename,picturepath) {
    console.log("show picture triggered - ", filename, ' / ', picturepath);
    let modal = this.modalCtrl.create(FullscreenPicturePage, {filename: filename, picturepath: picturepath});
    modal.present();
  }


  prev5 (){
    console.log("Prev 5 called");
    // If date search is active then disable PREV button
    // if (this.searchDate != null && this.searchDate != '') {
    //   return
    // }

    if (this.filesType == 'videos') {
      console.log("Curr Index - ",this.currVideoIndex);
      console.log("Total Video Files - ",this.videoCount);   
      if (this.currVideoIndex > 5 ) {
        let newIndex = this.currVideoIndex - 5;
        let fromIndex = newIndex - 5;
        if (fromIndex < 0 ) {
          fromIndex = 0;
          newIndex = 5;
        }
        this.videoFiles = this.videoFilesSource.slice(fromIndex, newIndex);
        this.currVideoIndex = newIndex;
        console.log("New Curr Index - ",this.currVideoIndex, "\n\n");        
      } else {
        alert ("Reached Beginning of Videos")
      }
    } else {
      console.log("Curr Index - ",this.currPictureIndex);
      console.log("Total Picture Files - ",this.pictureCount); 
      if (this.currPictureIndex > 5) {
        let newIndex = this.currPictureIndex - 5;
        let fromIndex = newIndex - 5;
        if (fromIndex < 0 ) {
          fromIndex = 0;
          newIndex = 5;
        }
        this.pictureFiles = this.pictureFilesSource.slice(fromIndex, newIndex);
        this.currPictureIndex = newIndex;
        console.log("New Curr Index - ",this.currPictureIndex, "\n\n");                
      } else {
        alert ("Reached Beginning of Pictures")
      }  
    }
  }
  
  // resetSearchBar() {

  // }

  next5 (){ 
    console.log("Next 5 called");
    // If date search is active then disable NEXT button
    // if (this.searchDate != null && this.searchDate != '') {
    //   return
    // }
    if (this.filesType == 'videos') {
      console.log("Curr Index - ",this.currVideoIndex);
      console.log("Total Video Files - ",this.videoCount);  
      if (this.currVideoIndex < this.videoCount) {
        let newIndex = this.currVideoIndex + 5;
        this.videoFiles = this.videoFilesSource.slice(this.currVideoIndex, newIndex);
        this.currVideoIndex = newIndex;
        console.log("New Curr Index - ",this.currVideoIndex, "\n\n");
      } else {
        alert ("Reached End of Videos")
      }
    } else {
      console.log("Curr Index - ",this.currPictureIndex);
      console.log("Total Picture Files - ",this.pictureCount); 
      if (this.currPictureIndex < this.pictureCount) {
        let newIndex = this.currPictureIndex + 5;
        this.pictureFiles = this.pictureFilesSource.slice(this.currPictureIndex, newIndex);
        this.currPictureIndex = newIndex;
        console.log("New Curr Index - ",this.currPictureIndex, "\n\n");
        
      } else {
        alert ("Reached End of Pictures")
      }  
    }
  }

  getVideoFiles() {
    // Get an XML list of files
    // Android
    if (!this.loaderIsPresent) {
      this.loading.present().then((result) => {
        this.loaderIsPresent = true;
      });
    }

    this.videoFilesSource = [];
    this.videoFiles = [];
    this.dashcamService.fileList().then((mediaFilesXml: any) =>{
      this.parseXML(mediaFilesXml)
      .then((filesArray: any)=>
      {          
        filesArray.forEach(file => {
          // console.log(file, '  ||  ');
          let thumbPath = 'http://192.168.1.254/NOVATEK/MOVIE/' + file.name + '?custom=1&cmd=4001';
          let picturePath = 'http://192.168.1.254/NOVATEK/MOVIE/' + file.name + '?custom=1&cmd=4002';
          let fileName = file.name;
          // let filePath = file.path;
          let dateTime = file.time;
          let downloadPath = 'http://192.168.1.254/NOVATEK/MOVIE/' + file.name;
          if (fileName.search("MP4") > 0) {
            // console.log("MOVIE file - ", fileName, " | Date Time - ", dateTime);            
            this.videoFilesSource.push({"name":fileName, "downloadPath": downloadPath, "thumbPath": thumbPath, "picturePath": picturePath, "dateTime": dateTime, "filePath": file.path});    
           }      
        });
        // console.log('<-------------- Library Video File List Success BEFORE SORT -------------->');
        // console.log('BEFORE',JSON.stringify(this.videoFilesSource));

        this.videoFilesSource.sort( function(a, b) {
          if ( a.dateTime > b.dateTime  ){
            return -1;
          }else if( a.dateTime < b.dateTime  ){
              return 1;
          }else{
            return 0;	
          }
        })

        // console.log('<-------------- Library Video File List Success AFTER SORT -------------->');
        // console.log('AFTER',JSON.stringify(this.videoFilesSource));
        this.videoCount = this.videoFilesSource.length;
        // this.videoFiles = this.videoFilesSource.slice(0, 50);
        // console.log('Display MOVIE Count',this.videoFiles.length);
        this.filterVideoFilesSource = this.videoFilesSource;
        this.videoFiles = this.videoFilesSource.slice(0, 5);
        console.log('Souce MOVIE Count',this.videoFilesSource.length);
        console.log('Filter MOVIE Count',this.filterVideoFilesSource.length);
        console.log('Display MOVIE Count',this.videoFiles.length);
        // alert("Display Movie Count " + this.videoFiles.length);
        // alert("VIDEOS -->" + JSON.stringify(this.videoFiles));

        this.currVideoIndex = 5;
        

        this.getPictureFiles();

      });

    }).catch ((error) => {
      console.log("Library File List Error / ", error)
    }) ;
  }

  getPictureFiles() {
    // Get an XML list of files
    this.searchDate = '';
    this.pictureFilesSource = [];
    this.pictureFiles = [];
    this.dashcamService.fileList().then((mediaFilesXml: any) =>{
      this.parseXML(mediaFilesXml)
      .then((filesArray: any)=>
      {          
        filesArray.forEach(file => {
          let thumbPath = 'http://192.168.1.254/NOVATEK/PHOTO/' + file.name + '?custom=1&cmd=4001';
          let picturePath = 'http://192.168.1.254/NOVATEK/PHOTO/' + file.name + '?custom=1&cmd=4002';

          let fileName = file.name;
          let dateTime = file.time;
          if (fileName.search("JPG") > 0) {
            // console.log("PICTURE file - ", fileName, " | Date Time - ", dateTime);
            this.pictureFilesSource.push({"name":fileName, "thumbPath": thumbPath, "picturePath": picturePath, "dateTime": dateTime,  "filePath": file.path});           
            // this.pictureFiles.push({"name":fileName, "downloadPath": downloadPath, "thumbPath": thumbPath, "picturePath": picturePath, "dateTime": dateTime});    
          }      
        });
        console.log('<-------------- Library Picture File List Success -------------->');
        console.log('Picture Source ----->',this.pictureFilesSource.length);
        this.pictureFilesSource.sort( function(a, b) {
          if ( a.dateTime > b.dateTime  ){
            return -1;
          }else if( a.dateTime < b.dateTime  ){
              return 1;
          }else{
            return 0;	
          }
        })
        this.filterPictureFilesSource = this.pictureFilesSource;
        this.pictureFiles = this.pictureFilesSource.slice(0, 5)
        this.currPictureIndex = 5;
        this.pictureCount = this.pictureFilesSource.length;
        // alert("Display Picture Count " + this.videoFiles.length);
        if (this.loaderIsPresent) {
          this.loading.dismiss();
          this.loaderIsPresent = false;
        }
      });

    }).catch ((error) => {
      console.log("Library File List Error / ", error)
    }) ;
  }

  deleteFile (mode,filePath) {
    console.log("Delete File ->", filePath);

    // let fileUrl =  "http://192.168.1.254/?custom=1&cmd=4003&str=" + filePath;
    // A:\\Novatek\\Movie\\" + fileName;
    // console.log("FILE TO DELETE -> ", fileUrl);
    this.dashcamService.deleteFile(filePath).then((data: any) =>{
    // this.post2(fileUrl).then((data: any) =>{
        
      console.log("File deleted sucessfully - ", JSON.stringify(data));

      if (mode == "video") {
        this.getVideoFiles();
      } else if (mode == "picture") {
        this.getPictureFiles();
      }
    }).catch ((error) => {
      console.log("File DELETE Error / ", JSON.stringify(error))
      // alert(JSON.stringify(error))
    }) ;
  }

  downloadFile (mode,fileName, fileUrl) {
    let fileTransfer: FileTransferObject;
    let downloading = this.loadingCtrl.create({
      content: 'Downloading Please wait...'
    });

    downloading.present();
    // console.log("mode -> ", mode);
    console.log("Download File Name ->", fileName);
    console.log("Download File URL ->", fileUrl);
    fileTransfer = this.transfer.create();
    let newFile = this.file.dataDirectory + fileName;
    console.log("new file -> ", newFile);
    fileTransfer.download(fileUrl, newFile).then((entry) => {
      console.log('download complete: ' + entry.toURL());
      if (mode == "video") {
        this.photoLibrary.saveVideo(entry.toURL(), 'YoSpot').then ((data) => {
          console.log("Video download to Album successfull - ", JSON.stringify(data));
          downloading.dismiss();
        }).catch ((error) => {
          console.log("Video download to Album error / ", JSON.stringify(error))
          downloading.dismiss();
        });
      } else {
        this.photoLibrary.saveImage(entry.toURL(), 'YoSpot').then ((data) => {
          console.log("Picture download to Album successfull - ", JSON.stringify(data));
          downloading.dismiss();
        }).catch ((error) => {
          console.log("Picture download to Album error / ", JSON.stringify(error))
          downloading.dismiss();
        });  
      }    
    }, (error) => {
      // handle error
      downloading.dismiss();
      console.log("File download error", JSON.stringify(error));
    });
  }

  // Parse CML data for Files and convert into an Array
  parseXML(data)
  {
     return new Promise(resolve =>
     {
        var k,l,
            arr    = [],
            parser = new xml2js.Parser(
            {
               trim: true,
               explicitArray: true
            });

        parser.parseString(data, function (err, result)
        {
           var obj = result.LIST;
           for(k in obj.ALLFile)
           {
              var item = obj.ALLFile[k];
              // console.log("Item ->", item);
              for (l in item.File) {

                var fl = item.File[l];

                // console.log("FL ->", JSON.stringify(fl.NAME[0]));
                // console.log("FL ->", JSON.stringify(fl.FPATH[0]));
                arr.push({
                  name           : fl.NAME[0],
                  path           : fl.FPATH[0],
                  time           : fl.TIME[0]
                });
              }
           }

           resolve(arr);
        });
     });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LibraryPage');
  }

  // post2(file) {
  //   return new Promise((resolve, reject) => {
  //     // this.http.setDataSerializer('utf8');
      
  //     this.http.get(url, {custom: '1', cmd: '4003', str: 'A:\\Novatek\\Movie\\2018_0322_222139_661A.MP4' },{}).then((result: any) => {
  //         resolve(result.data)
  //     }).catch( (error: any) => {
  //         reject(error)
  //     });
  //   }) 
  // }


}
