import { DashcamService } from './../../providers/dashcam-service';
import { VideoFeedPage } from './../video-feed/video-feed';
import { Component } from '@angular/core';
import { NavController, Platform, LoadingController } from 'ionic-angular';
declare let WifiWizard2: WifiWizard2;
import { Storage } from '@ionic/storage';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
// import { NativeAudio } from '@ionic-native/native-audio';
import { Geolocation } from '@ionic-native/geolocation';

// import './global';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  networks;
  wifi: string; // = 'WIFI_CARDV77fc';
  password: string; // = '12345678';
  loaderIsPresent = false;
  scanInProgress: boolean = false;
  options: BarcodeScannerOptions;
  androidWifiConfig: any;
  SSID: any;
  // wifi: string;
  // password: string;

  constructor(public navCtrl: NavController,
              public platform: Platform,
              private storage: Storage,
              private barcode: BarcodeScanner,
              public dashcamService: DashcamService,
              private geolocation: Geolocation,
            //   private nativeAudio: NativeAudio,
              public loadingCtrl: LoadingController
        ) {

            this.geolocation.getCurrentPosition().then((resp) => {
                // resp.coords.latitude
                // resp.coords.longitude
               }).catch((error) => {
                 console.log('Error getting location', error);
               });
        // storage.get('wifi')
        // .then((data) => {
        //     this.wifi = data;
        // }).catch((err) => {
        //     console.log("error reading user name from storage");
        // });

        // storage.get('password')
        // .then((data) => {
        //     this.password = data;

        //     // If stored wifi & password found then connect automatically
        //     console.log("Wifi ->", this.wifi," / Password ->", this.password);
        //     // if (this.password != '' && this.password != undefined ) {
        //     //     this.connect();
        //     // }  

        // }).catch((err) => {
        //     console.log("error reading user name from storage");
        // });
  }

  ionViewWillEnter(){
    
    this.storage.get('wifi')
    .then((data) => {
        this.wifi = data;
    }).catch((err) => {
        console.log("error reading user name from storage");
    });

    this.storage.get('password')
    .then((data) => {
        this.password = data;
        console.log("Wifi ->", this.wifi," / Password ->", this.password);
    }).catch((err) => {
        console.log("error reading user name from storage");
    }); 
  }

  tutorial() {
    
    }

  async qrScan(){
    // await WifiWizard2.requestPermission();
    if (this.scanInProgress) {
      return;
    } else {
        // var audio = new Audio('./assets/sounds/beep-07.wav');
        // audio.play();
      this.scanInProgress = true;
    }
    this.options = {
      prompt: 'Scan code for info',
      preferFrontCamera: false,
      showTorchButton: true,
      formats : "QR_CODE",
      disableSuccessBeep: true,
      disableAnimations: false
    }
   await this.barcode.scan(this.options).then((barcodeData) => {
          // Success! Barcode data is here
          this.scanInProgress = false;
          console.log('barcodeData text - ',JSON.stringify(barcodeData.text));
          if (barcodeData.text == null || barcodeData.text == "" ) {        
          } else {
              console.log('Connect to WiFi - PLAY SOUND');
              // extract WIFI Network and Password
              // setup variables wifi and password
              // play sound
              //   this.nativeAudio.preloadSimple('uniqueId1', 'path/to/file.mp3').then(onSuccess, onError);
              //   this.nativeAudio.play('uniqueId1').then(onSuccess, onError);
              var audio = new Audio('./assets/sounds/beep-07.wav');
              audio.play();
              
              var pos = barcodeData.text.lastIndexOf('/');
              this.wifi = barcodeData.text.substring(0, pos);
              this.password = barcodeData.text.substring(pos + 1, barcodeData.text.length);
              console.log("WIFI - ", this.wifi);
              console.log("Password - ", this.password);
            //   this.wifi = 'WIFI_CARDV77fc';
            //   this.password = '12345678';
              this.connect();
          }
      }, (err) => {
          // An error occurred
          this.scanInProgress = false;
          alert("Barcode Scanner Plugin Error - " + JSON.stringify(err));
      });
  }

    // connect() {
    //     // Check WiFi code
    //     if (this.wifi == '' || this.wifi == null) {
    //         alert("Please enter WiFi Code");
    //         return;
    //     }

    //     // Check Password
    //     if (this.password == '' || this.password == null) {
    //         alert("Please enter WiFi Code");
    //         return;
    //     }

    //     this.connectToCamera();
    // }

    scanNetwork() {
        // WifiWizard2.scan()
    }

    // async connectToCamera() {
    //     console.log('Connecting to WIFI')
    //     let loading = this.loadingCtrl.create({
    //         content: 'Connecting...'
    //     });
    
    //     loading.present().then((result) => {
    //         this.loaderIsPresent = true;
    //     });

    //     try {
    //         let wifiReturn = await WifiWizard2.iOSConnectNetworkAsync(this.wifi, this.password);

    //         console.log("WiFi Return ->", wifiReturn);
    //         console.log( "Successfully connected to " + this.wifi );

    //         // Store WiFi code and password in storage
    //         this.storage.set('wifi',this.wifi);
    //         this.storage.set('password',this.password);

    //         setTimeout(() => {
    //             if (this.loaderIsPresent) {
    //                 loading.dismiss();
    //             }
    //             this.navCtrl.push(VideoFeedPage);
    //         }, 12000);

    //     } catch( e ){
    //         throw new Error( "Failed to connect to device WiFi SSID " + this.wifi );
    //     }
    // }

// ***************** New Connect Function **************** //


    // ios version
    // async connect() {
    //     console.log('Connecting to WIFI')

    //     let loading = this.loadingCtrl.create({
    //         content: 'Connecting...'
    //       });

    //     // Check WiFi code
    //     if (this.wifi == '' || this.wifi == null) {
    //         alert("Please enter WiFi Code");
    //         return;
    //     }

    //     // Check Password
    //     if (this.password == '' || this.password == null) {
    //         alert("Please enter WiFi Code");
    //         return;
    //     }

    //     // Present loader
    //     loading.present();

    //     try {
    //         let httpFailed = false;

    //         // Call Connection method
    //         await this.connectToCamera();
    //         await this.timeout(12000);

    //         // this.storage.set('wifi',this.wifi);
    //         // this.storage.set('password',this.password);

    //         // Check if connection established
    //         // loading.setContent("Checking Connection...");
    //         // await this.checkConnection();
    //         // await this.timeout(2000);

    //         // navigate to Video feed page
            
    //         // loading.dismiss();
    //         await this.dashcamService.dvConnected().then ((resp) =>{
    //             console.log("DV is ALIVE - ", JSON.stringify(resp));
    //             // this.navCtrl.push(VideoFeedPage);
    //           }).catch((err) => {
    //             console.log("DV ALIVE command failed - ", JSON.stringify(err));
    //             // this.connectToCamera();
    //             httpFailed = true;
    //         });

    //         if (httpFailed) {
    //             loading.setContent("Connection Taking Longer...")
    //             console.log("HTTP failed status - ", httpFailed);
    //             await this.timeout(10000);
    //             console.log("Timeout Over ");
    //         }

    //         loading.dismiss();
    //         this.navCtrl.push(VideoFeedPage);


    //     } catch ( error ){
    //         loading.dismiss();
    //         console.log( 'WiFi Error / ', error.message );
    //         alert("Wifi connection Error - " + error.message )
    //     }
    // }

    // iOS Version
    // async connectToCamera() {
    //     try {
    //         console.log("Connecting WiFi");
    //         // console.log(this.wifi);
    //         // console.log(this.password);
    //         // await this.timeout(2000); 
    //         let connectingWiFi = await WifiWizard2.iOSConnectNetworkAsync(this.wifi, this.password);
    //         console.log("Connecting WiFi - Success ->", connectingWiFi);
    //         return true;
    //     } catch( e ){
    //         throw new Error( "Connecting Wifi. Failed to connect " + this.wifi );
    //     }
    // }


    async connect() {
        console.log('Connecting to WIFI')

        let loading = this.loadingCtrl.create({
            content: 'Connecting...'
          });

        // Check WiFi code
        if (this.wifi == '' || this.wifi == null) {
            alert("Please enter WiFi Code");
            return;
        }

        // Check Password
        if (this.password == '' || this.password == null) {
            alert("Please enter WiFi password");
            return;
        }

        // Present loader
        loading.present();
        // WifiWizard2.timeout(4000)

        try {
          //  let httpFailed = false;

            // Call Connection method
            await this.timeout(2000);
            // this.androidWifiConfig = WifiWizard2.formatWifiConfig('"'+this.wifi+'"', this.password,'WPA');
            // this.androidWifiConfig = WifiWizard2.formatWifiConfig(this.wifi, this.password,'WPA');
            this.androidWifiConfig = WifiWizard2.formatWifiConfig(this.wifi, this.password,'WPA');
            
            // alert(JSON.stringify(this.androidWifiConfig));
            // alert(this.wifi);
            // alert(this.password);

            // await this.add();
            await this.doConnect();

            await this.timeout(5000);
          

            loading.dismiss();
            this.navCtrl.push(VideoFeedPage);

        } catch ( error ){
            loading.dismiss();
            console.log( 'WiFi Error / ', error.message );
            alert("Wifi connection Error - " + error.message )
        }
    }

    // async add(){

    //     await this.timeout(10000);

    //     try {
    //         await WifiWizard2.addNetworkAsync( this.androidWifiConfig );
    //         // WifiWizard2.addNetwork(this.androidWifiConfig, null,null);
    //         return true;

    //     } catch( e ) {

    //         // throw new Error( JSON.stringify(e) );
    //         throw new Error( "Failed to add device WiFi network to your mobile device! Please try again, or manually connect to the device, disconnect, and then return here and try again. - " + JSON.stringify(e));

    //     }
    // }

    async add(){
        
                await this.timeout(5000);
        
                try {
                    await WifiWizard2.add( this.androidWifiConfig );
                    return true;
        
                } catch( e ) {
        
                    throw new Error( 'Add Error - ' + JSON.stringify(e) );
                    // throw new Error( "Failed to add device WiFi network to your mobile device! Please try again, or manually connect to the device, disconnect, and then return here and try again. - " + JSON.stringify(e));
        
                }
            }

    async doConnect(){
                
        await this.timeout(5000);

        try {

            // await WifiWizard2.androidConnectNetworkAsync(this.wifi  );
            await WifiWizard2.connect(this.wifi, true , this.password,'WPA', false)
            console.log( "Successfully connected to " + this.wifi);
            return true;

        } catch( e ){

            // throw new Error( "Failed to connect to device WiFi SSID " + this.wifi + " Error - " + JSON.stringify(e) );
            throw new Error( 'Connect error -' + JSON.stringify(e) );
            
        }
    }

    // Android functions
    // connect() {

    //     console.log('Connecting to WIFI')

    //     let loading = this.loadingCtrl.create({
    //         content: 'Connecting...'
    //       });

    //     // Check WiFi code
    //     if (this.wifi == '' || this.wifi == null) {
    //         alert("Please enter WiFi Code");
    //         return;
    //     }

    //     // Check Password
    //     if (this.password == '' || this.password == null) {
    //         alert("Please enter WiFi Code");
    //         return;
    //     }

    //     // Present loader
    //     // loading.present();

    //      try {

    //         let config = WifiWizard2.formatWifiConfig(this.wifi, this.password,'WPA');

    //         console.log("Connecting WiFi");
    //         // alert('WiFi - '+ this.wifi);
    //         // alert('Password - '+ this.password);

    //         // let config = WifiWizard.formatWifiConfig('MyNetworkName', 'mynetworkpassword', 'WPA');
    //         // alert('addwifi config' + config);
    //         let ssid = '"'+this.wifi+'"';
    //         alert('SSID -> ' + ssid);
    //         // loading.present();

    //         WifiWizard2.addNetwork(
    //             config,
    //             () => {
    //                      alert('Network added sucessfully')
    //                      // connect to this network
    //                     // WifiWizard2.androidConnectNetworkAsync(this.wifi).try {
    //                     //     console.log
    //                     // } catch (error) {
                            
    //                     // }

    //                      WifiWizard2.androidConnectNetwork (
    //                      ssid,
    //                      () => {
    //                             // loading.dismiss;
    //                             alert('Network connected sucessfully')
    //                             this.navCtrl.push(VideoFeedPage)
    //                            },
    //                      () => {
    //                             // loading.dismiss;
    //                             alert('Network Connection Error')
    //                             }
    //                      )
                         
    //                 },
    //             () => alert('Network Add error')
    //         );

    //         // WifiWizard2.connect(this.wifi, true, this.password, {SSID: this.wifi, algorithm: 'WEP', password: this.password})
    //         // loading.dismiss();
    //         // alert('Connection sucessfull');
    //         // this.navCtrl.push(VideoFeedPage)
    //         // return true;
    //     } catch( e ){
    //         // loading.dismiss();
    //         alert('Failed to connect WiFi');
    //         throw new Error( "Connecting Wifi. Failed to connect " + this.wifi );
    //     }
    // }


    async checkConnection() {
        try {
            console.log("Checking Connection");            
            // await this.timeout(2000); 
            let connectedWiFi = await WifiWizard2.getConnectedSSIDAsync();
            console.log("Checking Connection - Success ->", connectedWiFi);
            return true;
        } catch( e ){
            throw new Error( "Checking Connection. Failed to connect " + this.wifi );
        }
    }

    timeout(delay) {
        return new Promise(function(resolve, reject) {
            setTimeout(resolve, delay);
        });
    }



}



// async connectToCamera() {
//     console.log('Connecting to WIFI')
//     let loading = this.loadingCtrl.create({
//         content: 'Connecting...'
//       });
  
//     loading.present().then((result) => {
//     this.loaderIsPresent = true;
//     });

//     try {
//         let wifiReturn = await WifiWizard2.iOSConnectNetworkAsync(this.wifi, this.password);

//         console.log("WiFi Return ->", wifiReturn);
//         if (wifiReturn == undefined){
//             console.log("Could not connect to Wi Fi");
//             loading.dismiss();
//             return;
//         }
//         // await WifiWizard2.getConnectedSSIDAsync()
//         console.log( "Successfully connected to " + this.wifi );
//         // return true;

//         // async setInterval(() => {
//         //    let lssid = await WifiWizard2.getConnectedSSIDAsync()
//         //    console.log("SSID found - ", lssid );

//         //    setInterval(() => {
//         //         if (lssid != null) {
//         //             console.log("Got SSID - ", lssid );
//         //             if (this.loaderIsPresent) {
//         //                 loading.dismiss();
//         //             }
//         //             this.navCtrl.push(VideoFeedPage);  
//         //         } else {
//         //             console.log("NO SSID yet - ", lssid );
//         //         }
//         //     }, 1000)

//         // Store WiFi code and password in storage
//         this.storage.set('wifi',this.wifi);
//         this.storage.set('password',this.password);

//         setTimeout(() => {
//             if (this.loaderIsPresent) {
//                 loading.dismiss();
//             }
//             this.navCtrl.push(VideoFeedPage);
//         }, 12000);

//     } catch( e ){
//         throw new Error( "Failed to connect to device WiFi SSID " + this.wifi );
//     }
//   }

    // this.platform.ready().then(() => {
        // WifiWizard2.iOSConnectNetwork(this.wifi, this.password, success, fail);

    // let success = () => {
    //     console.log("Connection sucessful");
        
    //     setTimeout(() => {
    //         this.navCtrl.push(VideoFeedPage);
    //     }, 5000);
    // }

    // if (typeof WifiWizard !== 'undefined') {
    //         console.log("WifiWizard loaded: ");
    //         // console.log(WifiWizard);
    // } else {
    //     console.warn('WifiWizard not loaded.');
    // }  

    // let success = () => {
    //     console.log("Connection sucessful");
        
    //     setTimeout(() => {
    //         this.navCtrl.push(VideoFeedPage);
    //     }, 5000);

    // }

    // let fail = () => {
    //     // console.log("" + e);
    //     console.log("Connection failed");
    // }



    // let listHandler = (a: any) => {
    //     this.networks = [];
    //     for (let x in a) {                    
    //         console.log(a[x].SSID + ", " + a[x].BSSID + ", " + a[x].level);  
    //         this.networks.push({
    //             ssid: a[x].SSID,
    //             bssid: a[x].BSSID,
    //             level: a[x].level});                
    //     }  
    // }
    // WifiWizard2.scan(successNetwork, failNetwork);
    //  startScan(successNetwork, failNetwork);
// <ion-list>
//     <ion-item *ngFor = "let network of networks">
//       {{network.ssid}}
//     </ion-item>
  
//   </ion-list>
