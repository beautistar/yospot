// import { CustomSplashPage } from './../pages/custom-splash/custom-splash';
// import { SettingsPage } from './../pages/settings/settings';
// import { FullscreenPicturePage } from './../pages/fullscreen-picture/fullscreen-picture';
// import { LibraryPage } from './../pages/library/library';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
// import { VideoFeedPage} from '../pages/video-feed/video-feed'
// import { BackgroundMode } from '@ionic-native/background-mode';

@Component({
  templateUrl: 'app.html'
})


export class MyApp {
   rootPage:any = HomePage;
  // rootPage:any = CustomSplashPage;
  // rootPage:any = SettingsPage;
  // rootPage:any = FullscreenPicturePage;
  // rootPage: any = LibraryPage;
 // rootPage:any = VideoFeedPage;

    constructor(platform: Platform, 
    statusBar: StatusBar, 
    // private backgroundMode: BackgroundMode,
    splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      // this.backgroundMode.enable();
    });

  }
}


