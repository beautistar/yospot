// import { UtilityService } from './utility-service';
import { Injectable } from '@angular/core';
import { HttpService } from './http-service'
// import { UtilityService } from './utility-service'

@Injectable()
export class DashcamService {

    // Camera Settings
    // public camSetting = {
    //     photo: '',
    //     resolution: '',
    //     audio: 0,
    //     collision: '',
    //     download: false,
    //     capacity: 0
    // }

    public liveFeedUrl = 'http://192.168.1.254:8192';
    public videoRecording: boolean = false;
    public videoEntry = 'new';
    private feedModeUrl = 'http://192.168.1.254/?custom=1&cmd=3028&par=';  
    private baseUrl = 'http://192.168.1.254/?custom=1&cmd=';

    // Status Commands
    private currStatusUrl =  'http://192.168.1.254/?custom=1&cmd=3037';
    private systemResetUrl = 'http://192.168.1.254/?custom=1&cmd=3011';

    // Video & Photo Commands to change mode to Picture, Video, Live View or File download
    private liveViewStartUrl =  'http://192.168.1.254/?custom=1&cmd=2015&par=1'
    private liveViewStopUrl =  'http://192.168.1.254/?custom=1&cmd=2015&par=0'
    private pictureModeUrl = 'http://192.168.1.254/?custom=1&cmd=3001&par=0';
    private videoModeUrl =  'http://192.168.1.254/?custom=1&cmd=3001&par=1';
    private takePictureCommand = '1001';  // No parm
    private recordVideoCommand = '2001';  // parm: 1 = start, 0 = stop
    private muteAudioCommand = '2007';    // parm: 1 = ON, 0 = OFF

    // Librabry URLs and commands
    private playbackModeUrl = 'http://192.168.1.254/?custom=1&cmd=3001&par=2';
    private fileListUrl = 'http://192.168.1.254/?custom=1&cmd=3015';

    // Settings functions
    private formatCardUrl =  'http://192.168.1.254/?custom=1&cmd=3010&par=1';
    private setupDateUrl = 'http://192.168.1.254/?custom=1&cmd=3005&str=';  // parm = yyyy-mm-dd
    private setupTimeUrl = 'http://192.168.1.254/?custom=1&cmd=3006&str=';  // parm = hh:mm:ss
    // private resetWifiUrl = 'http://192.168.1.254/?custom=1&cmd=3018';  // parm = hh:mm:ss

    constructor ( private httpService: HttpService,
                    // private utilityService: UtilityService
                ) {

    }

    public liveFeedToggle(mode) {
        return new Promise((resolve, reject) => {
        if (mode == 'start') {
            // Start Live Video
            this.httpService.post(this.liveViewStartUrl)
            .then((videoStatus: any) => {
                resolve(videoStatus);                     
            }).catch ((error) => {
                reject(error);
            });
        } else if (mode == 'stop') {
            // Stop Live Video
            this.httpService.post(this.liveViewStopUrl)
            .then((videoStatus: any) => {
                resolve(videoStatus);                          
            }).catch ((error) => {
                reject(error);
            });
        }
        });  
    }
    public changeCameraMode(mode) {
        return new Promise((resolve, reject) => {
            let url: any;
            switch (mode) {
                case 'PLAYBACK':
                    url = this.playbackModeUrl;
                    break;
                case 'VIDEO':
                    url = this.videoModeUrl;  
                    break;  
                case 'PHOTO':
                    url = this.pictureModeUrl;
                    break;           
                default:
                    break;
            }
            this.httpService.post(url)
            .then((status: any) => {
                resolve(status);
            }).catch ((error) => {
                reject(error);
            });
        });   
    }

    // Submit Video Feed commands with 4 different modes 0-Front, 1-Back, 2-BackOnFront, 3-FrontOnBack
    public changeFeedMode(mode) {
        return new Promise((resolve, reject) => {
            let fullUrl = this.feedModeUrl + mode;
            // console.log("Change Feed Mode URL ->", fullUrl);
            this.httpService.post(fullUrl)
            .then((status: any) => {
                // console.log("Change Feed successful");
                resolve(status);
            }).catch ((error) => {
                // console.log("Change Feed error / ", error);
                reject(error);
            });
        });   
    }

    // System reset
    public systemReset() {
        return new Promise((resolve, reject) => {
            this.httpService.post(this.systemResetUrl)
            .then((status: any) => {
                // console.log("Change Feed successful");
                resolve(status);
            }).catch ((error) => {
                // console.log("Change Feed error / ", error);
                reject(error);
            });
        });   
    }

    // Check the current mode Video, Photo or Playback
    public currStatus() {
        return new Promise((resolve, reject) => {
            // console.log("currStatusURL ->", this.currStatusUrl);

            // Get the current status
            this.httpService.post(this.currStatusUrl)
            .then((status: any) => {
                // console.log("\nstatus received", JSON.stringify(status));
                resolve(status);
            })
            .catch ((error) => {
                // console.log(" status recevie error / ", JSON.stringify(error));
                reject(error);
            });
        });   
    }

    public takePicture(cameraMode) {
        return new Promise((resolve, reject) => {
            // Setup URL to take pictures
            let takePictureUrl = this.baseUrl + this.takePictureCommand ;

            // Change the mode to picture mode 
            this.httpService.post(this.pictureModeUrl)
            .then((pictureStatus: any) => {
                console.log("Picture Mode changed successfully", JSON.stringify(pictureStatus));
                // PIP MODE
                if (cameraMode == 2 || cameraMode == 3) {
                    console.log("PIP Mode");
                    console.log("change Picture mode");
                    this.changeFeedMode(0).then((jsonData: any) => {
                        console.log("Picture mode changed to 0");
                        this.httpService.post(takePictureUrl).then((jsonData: any) => {
                            console.log("Take Picture Command successful");
                            this.changeFeedMode(1).then((jsonData: any) => {
                                console.log("Picture mode changed to 1");
                                this.httpService.post(takePictureUrl).then((jsonData: any) => {
                                    console.log("Take Picture Command successful");
                                    this.changeFeedMode(cameraMode).then((jsonData: any) => {
                                        console.log("Restore camera mode / ", cameraMode);
                                        resolve(jsonData);
                                    }).catch ((error) => {
                                        console.log("Restore camera mode error  / ", JSON.stringify(error));
                                        reject(error);
                                    });
                                }).catch ((error) => {
                                    console.log("Take Picture Command error  / ", JSON.stringify(error));
                                    reject(error);
                                });
                            }).catch ((error) => {
                                console.log("Picture mode 1 change error  / ", JSON.stringify(error));
                                reject(error);
                            });
                        }).catch ((error) => {
                            console.log("Take Picture error / ", JSON.stringify(error));
                            reject(error);
                        });
                    }).catch ((error) => {
                        console.log("Picture mode 0 change error / ", JSON.stringify(error));
                        reject(error);
                    });
                }
                // No PIP MODE
                else {
                    console.log("NO PIP Mode / ", cameraMode);
                    this.httpService.post(takePictureUrl)
                    .then((jsonData: any) => {
                        console.log("Take Picture Command successful", JSON.stringify(jsonData));
                        resolve(jsonData);
                    }).catch ((error) => {
                        console.log("Take Picture error / ", JSON.stringify(error));
                        reject(error);
                    });
                }
            })
            .catch ((error) => {
                console.log("Picture Mode change error / ", JSON.stringify(error));
                reject(error);
            });
        });   
    }

    public recordVideo(parm) {
        return new Promise((resolve, reject) => {
            // Setup URL to take video
            let recordVideoUrl = this.baseUrl + this.recordVideoCommand + '&par=' + parm ;
            // console.log("videoModeURL ->", this.videoModeUrl);
            // console.log("recordVideoURL ->", recordVideoUrl);

            // Change the mode to video mode first
            if (parm == 1){ 
                // this.httpService.post(this.videoModeUrl)
                // .then((videoStatus: any) => {
                //     console.log("\nVideo Mode change sucessfully", JSON.stringify(videoStatus));

                    // Submit command to record video
                    this.httpService.post(recordVideoUrl)
                    .then((jsonData: any) => {
                        console.log("Video recording ON Command successful", JSON.stringify(jsonData));
                        resolve(jsonData);
                    }).catch ((error) => {
                        console.log("Video Recording error / ", JSON.stringify(error));
                        reject(error);
                    });
                // })
                // .catch ((error) => {
                //     console.log(" Video Mode change error / ", JSON.stringify(error));
                //     reject(error);
                // });
            } else {
                // this.httpService.post(this.videoModeUrl)
                // .then((videoStatus: any) => {
                //     console.log("\nVideo Mode change sucessfully", JSON.stringify(videoStatus));

                    // Submit command to record video
                    this.httpService.post(recordVideoUrl)
                    .then((jsonData: any) => {
                        console.log("\nVideo recording OFF Command successful", JSON.stringify(jsonData));
                        resolve(jsonData);
                        // if (parm == 0) {
                        //     // Change mode back to Live View if recording is complete
                        //     this.httpService.post(this.liveViewStartUrl)
                        //     .then((videoStatus: any) => {
                        //         console.log("\nLive Video Restored sucessfully",JSON.stringify(videoStatus))    
                        //         // Restore original live feed
                        //         this.changeFeedMode(0)               
                        //     }).catch ((error) => {
                        //         console.log("Live Video Change error / ",JSON.stringify(error)) 
                        //         reject(error);
                        //     });
                        // }
                    }).catch ((error) => {
                        console.log("Video Recording error / ", JSON.stringify(error));
                        reject(error);
                    });
                // })
                // .catch ((error) => {
                //     console.log(" Video Mode change error / ", JSON.stringify(error));
                //     reject(error);
                // });     
            }
        });   
    }

    // public muteAudio(parm) {
    public muteAudio(parm) {
            
        return new Promise((resolve, reject) => {

            // Setup URL to change Audio
            let muteAudioUrl = this.baseUrl + this.muteAudioCommand + '&par=' + parm ; 
            console.log("muteAudioURL ->", muteAudioUrl);

                // Submit command to mute Audio 
                this.httpService.post(muteAudioUrl)
                .then((jsonData: any) => {
                    console.log("\nMute Audio Command successful", JSON.stringify(jsonData));
                    resolve(jsonData);
                }).catch ((error) => {
                    console.log("Mute Audio error / ", JSON.stringify(error));
                    reject(error);
                });
        });   
    }

    public qrScan() {
        return new Promise((resolve, reject) => {
            console.log("Url ->", this.formatCardUrl);
            // let url = 

            // Submit command to Format Card 
            this.httpService.post(this.formatCardUrl)
            .then((jsonData: any) => {
                console.log("\nFormat Card Command successful", JSON.stringify(jsonData));
                resolve(jsonData);
            }).catch ((error) => {
                console.log("Format Card error / ", JSON.stringify(error));
                reject(error);
            });
        });   
    }


    public setResolution(resolution) {
        return new Promise((resolve, reject) => {
            let recordUrl = 'http://192.168.1.254/?custom=1&cmd=2002&par=' + resolution;
            // let liveViewUrl = 'http://192.168.1.254/?custom=1&cmd=2010&par=' + resolution;
            console.log("Resolution url - ", recordUrl);
            // Submit http command 
            // this.httpService.post(liveViewUrl)
            // .then((jsonData: any) => {
                this.httpService.post(recordUrl)
                .then((jsonData: any) => {
                    resolve(jsonData);
                }).catch ((error) => {
                    reject(error);
                });
            // }).catch ((error) => {
            //     reject(error);
            // });
        });   
    }

    public setPhotoQuality(size) {
        return new Promise((resolve, reject) => {
            let url = 'http://192.168.1.254/?custom=1&cmd=1002&par=' + size;
            // Submit http command 
            this.httpService.post(url)
            .then((jsonData: any) => {
                resolve(jsonData);
            }).catch ((error) => {
                reject(error);
            });
        });   
    }

    public setRecordingTime(parm) {
        return new Promise((resolve, reject) => {
            let url = 'http://192.168.1.254/?custom=1&cmd=2003&par=' + parm;
            // Submit http command 
            this.httpService.post(url)
            .then((jsonData: any) => {
                resolve(jsonData);
            }).catch ((error) => {
                reject(error);
            });
        });   
    }

    public resetSystem() {
        return new Promise((resolve, reject) => {
            let url = 'http://192.168.1.254/?custom=1&cmd=3011';
            // Submit command to Reset System
            this.httpService.post(url)
            .then((jsonData: any) => {
                console.log("\nReset System successful", JSON.stringify(jsonData));
                resolve(jsonData);
            }).catch ((error) => {
                console.log("Reset System error / ", JSON.stringify(error));
                reject(error);
            });
        });   
    }

    public movieDate(parm) {
        return new Promise((resolve, reject) => {
            let url = 'http://192.168.1.254/?custom=1&cmd=2008&par=' + parm;
            console.log("movie date url - ", url);
            // Submit http command 
            this.httpService.post(url)
            .then((jsonData: any) => {
                resolve(jsonData);
            }).catch ((error) => {
                reject(error);
            });
        });   
    }

    // public getStatus(command) {
    public getSettings() {
        return new Promise((resolve, reject) => {
            let url = 'http://192.168.1.254/?custom=1&cmd=3014'
            // Submit http command 
            this.httpService.post(url)
            .then((jsonData: any) => {
                
                resolve(jsonData);
            }).catch ((error) => {
                reject(error);
            });
        });   
    }
    public getRecordingTime() {
        return new Promise((resolve, reject) => {
            let url = 'http://192.168.1.254/?custom=1&cmd=2009';
            // Submit http command 
            this.httpService.post(url)
            .then((jsonData: any) => {
                resolve(jsonData);
            }).catch ((error) => {
                reject(error);
            });
        });   
    }

    public setCollision(sensitivity) {
        return new Promise((resolve, reject) => {
            let url = 'http://192.168.1.254/?custom=1&cmd=2011&par=' + sensitivity;
            console.log("Collision Url - ", url);
            // Submit http command 
            this.httpService.post(url)
            .then((jsonData: any) => {
                resolve(jsonData);
            }).catch ((error) => {
                reject(error);
            });
        });   
    }

    public download(allowDownload) {
        return new Promise((resolve, reject) => {
            let url = '';
            // Submit http command 
            this.httpService.post(url)
            .then((jsonData: any) => {
                resolve(jsonData);
            }).catch ((error) => {
                reject(error);
            });
        });   
    }

    public formatCard() {
        return new Promise((resolve, reject) => {
            console.log("formatCardURL ->", this.formatCardUrl);

            // Submit command to Format Card 
            this.httpService.post(this.formatCardUrl)
            .then((jsonData: any) => {
                console.log("\nFormat Card Command successful", JSON.stringify(jsonData));
                resolve(jsonData);
            }).catch ((error) => {
                console.log("Format Card error / ", JSON.stringify(error));
                reject(error);
            });
        });   
    }



    public cardCapacity() {
        return new Promise((resolve, reject) => {
            let url = 'http://192.168.1.254/?custom=1&cmd=3017';
            // Submit http command 
            this.httpService.post(url)
            .then((jsonData: any) => {
                resolve(jsonData);
            }).catch ((error) => {
                reject(error);
            });  
        });   
    }
    

    public movieCapacity() {
        return new Promise((resolve, reject) => {
            let url = 'http://192.168.1.254/?custom=1&cmd=2009';
            // Submit http command 
            this.httpService.post(url)
            .then((jsonData: any) => {
                resolve(jsonData);
            }).catch ((error) => {
                reject(error);
            });  
        });   
    }

    public getWIFI() {
        return new Promise((resolve, reject) => {
            let url = 'http://192.168.1.254/?custom=1&cmd=3029';
            // console.log('SSID url ->', url);
            // Submit http command 
            this.httpService.post(url)
            .then((jsonData: any) => {
                resolve(jsonData);
            }).catch ((error) => {
                reject(error);
            });  
        });   
    }

    public setSSID(parm) {
        return new Promise((resolve, reject) => {
            let url = 'http://192.168.1.254/?custom=1&cmd=3003&str=' + parm;
            console.log('SSID url ->', url);
            // Submit http command 
            this.httpService.post(url)
            .then((jsonData: any) => {
                resolve(jsonData);
            }).catch ((error) => {
                reject(error);
            });  
        });   
    }

    public deleteFile(file) {
        return new Promise((resolve, reject) => {
            // let url = 'http://192.168.1.254/?custom=1&cmd=4003&str=' + fileName;
            // console.log('Delete File url ->', fileUrl);
            // Submit http command 
            this.httpService.postDeleteFile(file)
            .then((jsonData: any) => {
                resolve(jsonData);
            }).catch ((error) => {
                // console.log("delete error provider - ", JSON.stringify(error));
                reject(error);
            });  
        });   
    }

    public setPassphrase(parm) {
        return new Promise((resolve, reject) => {
            let url = 'http://192.168.1.254/?custom=1&cmd=3004&str=' + parm;
            // Submit http command 
            this.httpService.post(url)
            .then((jsonData: any) => {
                resolve(jsonData);
            }).catch ((error) => {
                reject(error);
            });  
        });   
    }

    public reconnectWifi() {
        return new Promise((resolve, reject) => {
            let url = 'http://192.168.1.254/?custom=1&cmd=3018';
            // Submit http command 
            this.httpService.post(url)
            .then((jsonData: any) => {
                resolve(jsonData);
            }).catch ((error) => {
                reject(error);
            });  
        });   
    }

    public dvConnected() {
        return new Promise((resolve, reject) => {
            let url = 'http://192.168.1.254/?custom=1&cmd=3016';
            // Submit http command 
            this.httpService.post(url)
            .then((jsonData: any) => {
                resolve(jsonData);
            }).catch ((error) => {
                reject(error);
            });  
        });   
    }

    public startWifiMode() {
        return new Promise((resolve, reject) => {
            let url = 'http://192.168.1.254/?custom=1&cmd=3035';
            // Submit http command 
            this.httpService.post(url)
            .then((jsonData: any) => {
                resolve(jsonData);
            }).catch ((error) => {
                reject(error);
            });  
        });   
    }

    public stopWifiMode() {
        return new Promise((resolve, reject) => {
            let url = 'http://192.168.1.254/?custom=1&cmd=3036';
            // Submit http command 
            this.httpService.post(url)
            .then((jsonData: any) => {
                resolve(jsonData);
            }).catch ((error) => {
                reject(error);
            });  
        });   
    }

    public selectWifiStationMode() {
        return new Promise((resolve, reject) => {
            let url = 'http://192.168.1.254/?custom=1&cmd=3033&par=1';
            // Submit http command 
            this.httpService.post(url)
            .then((jsonData: any) => {
                resolve(jsonData);
            }).catch ((error) => {
                reject(error);
            });  
        });   
    }

    public sendWifiToDevice(ssid,passphrase) {
        return new Promise((resolve, reject) => {
            let url = 'http://192.168.1.254/?custom=1&cmd=3032&str=' + ssid + ':' + passphrase;
            // Submit http command 
            this.httpService.post(url)
            .then((jsonData: any) => {
                resolve(jsonData);
            }).catch ((error) => {
                reject(error);
            });  
        });   
    }
    public setupDate(date,time) {
        return new Promise((resolve, reject) => {
            // Setup URL to setup Date
            let dateUrl = this.setupDateUrl + date;
            let timeUrl = this.setupTimeUrl + time;

            console.log("Date URL ->", dateUrl);
            console.log("Time URL ->", timeUrl);

            // Submit command to Setup Date 
            this.httpService.post(dateUrl)
            .then((jsonData: any) => {
                console.log("\nSetup Date Command successful", JSON.stringify(jsonData));
                resolve(jsonData);

                // Submit command to Setup Time 
                this.httpService.post(timeUrl)
                .then((jsonData: any) => {
                    console.log("\nSetup Time Command successful", JSON.stringify(jsonData));
                    resolve(jsonData);
    
                    
                }).catch ((error) => {
                    console.log("Setup Time error / ", JSON.stringify(error));
                    reject(error);
                });

            }).catch ((error) => {
                console.log("Setup Date error / ", JSON.stringify(error));
                reject(error);
            });
        });   
    }

    public fileList() {
        return new Promise((resolve, reject) => {

            // Setup URL to take pictures
            console.log("fileListURL ->", this.fileListUrl);

                // Submit command to file List 
                this.httpService.post(this.fileListUrl)
                .then((jsonData: any) => {
                    console.log("\nfile List Command successful");
                    resolve(jsonData);
                }).catch ((error) => {
                    console.log("file List error / ", JSON.stringify(error));
                    reject(error);
                });
        });   
    }

    // // Submit General commands with 4 different modes 0-Front, 1-Back, 2-BackOnFront, 3-FrontOnBack
    // public submitCommand(command, parm) {
    //     return new Promise((resolve, reject) => {
    //         let modeUrl: string;
    //         let commandUrl: string;
    //         // switch (command) {
    //         //     case 'recordVideo':
    //         //         modeUrl = this.baseUrl + this.videoModeChange;
    //         //         commandUrl = this.baseUrl + this.recordVideoCommand + '&par=' + parm ;
    //         //         break;
    //         //     case 'takePicture':
    //         //         modeUrl = this.baseUrl + this.pictureModeChange;
    //         //         commandUrl = this.baseUrl + this.takePictureCommand ;
    //         //         break;                 
    //         //     case 'muteAudio':
    //         //         modeUrl = this.baseUrl + this.videoModeChange;
    //         //         commandUrl = this.baseUrl + this.muteAudioCommand + '&par=' + parm ;                    
    //         //         break; 
    //         //     case 'fileList':
    //         //         modeUrl = this.baseUrl + this.fileModeChange;
    //         //         commandUrl = this.baseUrl + this.fileListCommand;                    
    //         //         break;   
    //         //     default:
    //         // }

    //         console.log("ModeURL ->", modeUrl);
    //         console.log("Command URL ->", commandUrl);

    //         // Change the mode first
    //         this.httpService.post(modeUrl)
    //         .then((status: any) => {
    //             console.log(command, " - Mode changed sucessfully", JSON.stringify(status));
    //             // Mode change sucessfull so submit the Command
    //             this.httpService.post(commandUrl)
    //             .then((jsonData: any) => {
    //                 console.log(command, " - Command successful", JSON.stringify(jsonData));
    //                 resolve(jsonData);
    //             }).catch ((error) => {
    //                 console.log(command, " - Command error / ", error);
    //                 reject(error);
    //             });
    //         })
    //         .catch ((error) => {
    //             console.log(command, " - Mode change error / ", error);
    //             reject(error);
    //         });
    //     });   
    // }
}


// // Submit Dashcam command 
// private submitCommand(url, params){ 
//     return new Promise((resolve, reject) => {
//         let fullUrl = url + params;
//         console.log("URL ->", fullUrl);
//         this.httpService.post(fullUrl)
//         .then((status: any) => {
//             // console.log("Command successful");
//             resolve(status)
//         }).catch ((error) => {
//             // console.log("Error submitting Url / ", error);
//             reject(error);
//         });
//     }) 
// }