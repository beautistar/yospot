import { LibraryPage } from './../library/library';
import { SettingsPage } from './../settings/settings';
import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { DashcamService } from '../../providers/dashcam-service'
import xml2js from 'xml2js';
// import { ChangeDetectorRef } from '@angular/core';
import {  LoadingController } from 'ionic-angular';
// import { BackgroundMode } from '@ionic-native/background-mode';

/**
 * Generated class for the VideoFeedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-video-feed',
  templateUrl: 'video-feed.html',
})
export class VideoFeedPage {
  liveFeedUrl: string;
  result: any;
  mainCamera: string;
  pip: boolean;
  videoRecording: boolean = false;
  muteAudio: boolean = false;
  cameraMode = 2;
  cameraModeText = "Front / Back";
  loaderIsPresent = false;
  recBlink: boolean = true;
    // Camera Settings
    camSettings = {
      photo: ' ',
      resolution: ' ',
      audio: 0,
      collision: ' ',
      download: false,
      capacity: 0
  }

  myDate: String = new Date().toISOString();
  mydatTime: any;
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              // private cdRef: ChangeDetectorRef, 
              private loadingCtrl: LoadingController, 
              private screenOrientation: ScreenOrientation,
              // private backgroundMode: BackgroundMode,       
              public platform: Platform,       
              public dashcamService: DashcamService) {
              setInterval(() => {
                // console.log("rec Blink = ", this.recBlink); 
                if (this.recBlink == true) {
                  this.recBlink = false;
                } else {
                  this.recBlink = true;
                }
               }, 750);

               // detect orientation changes
              this.screenOrientation.onChange().subscribe(
                () => {
                    console.log("Orientation Changed");
                }
              );

              // this.dashcamService.reconnectWifi();

  }

  ionViewDidLeave(){
   console.log("view did leave");
  }

  ionViewWillEnter(){
    this.startTime()
      // Enable Background mode so App is not paused
      // this.backgroundMode.enable;

      //Get camera settings including audio
      this.getCameraSettings();
      // Change Camera Mode to Movie
      console.log("view will enter");
      console.log("ionViewWillEnter - recording status - ", this.dashcamService.videoRecording);
      
      // if (this.backgroundMode.isActive()) {
      //   console.log("background mode is active")
      // } else {
      //   console.log("background mode inactive");
      // }
      switch (this.dashcamService.videoEntry) {
        case 'new':
          console.log("NEW VIDEO ENTRY --------->", this.dashcamService.videoEntry);
          this.initialize();
          break;
        case 'settings':
          console.log("SETTINGS VIDEO ENTRY --------->", this.dashcamService.videoEntry);  
          let load = this.loadingCtrl.create({
            content: 'Restarting Live Feed...'
          });
          load.present();
          this.dashcamService.liveFeedToggle('start').then(() => {
            console.log("Livefeed restarted");
            this.goToToggleRecording();
            load.dismiss();
          }).catch((err) => {
            load.dismiss();
            console.log("Live feed start error", JSON.stringify(err))
          })
          break;   
        case 'library':
          this.initialize();  
          break;    
        default:
          this.initialize();
          break;
      }
  }

  startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    // add a zero in front of numbers<10
    m = this.checkTime(m);
    s = this.checkTime(s);
    this.mydatTime =  h + ":" + m + ":" + s;
   this.setTm();
  }

  setTm(){
    setTimeout(()=> { this.startTime() }, 500);
  }

  checkTime(i) {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  }




  getCameraSettings() {
    this.dashcamService.getSettings().then ((data) => {
      // console.log("Settings Data ->", JSON.stringify(data));

      this.parseSettingsXML(data).then((cmdArray: any) => {
      // console.log("PARSE ARRAY ->", JSON.stringify(cmdArray));

      cmdArray.forEach((item: any) => {
        switch (item.cmd) {
          case '1002':
            this.camSettings.photo = item.status;
            break;
          case '2002':
            this.camSettings.resolution = item.status;
            break; 
          case '2011':
            this.camSettings.collision = item.status;
            break;       
          case '2007':
            console.log("item status - ", item.status);
            if (Number(item.status) == 1) {
              this.muteAudio = false;
              console.log("muteAudio is FALSE")
            } else {
              this.muteAudio = true;
              console.log("muteAudio is TRUE")
            }
            break;  
          default:
            break;
        }
        // console.log("Settings Object AFTER -> ", JSON.stringify(this.camSettings));
      }).catch((err) => console.log("XML error /", err));

      // console.log("Settings Object AFTER -> ", JSON.stringify(this.camSettings));
      
    });
  }).catch ((err) => console.log("Get Settings Error / ", err));
  }

  initialize() {
    // Get the current Video Recording status
    this.dashcamService.currStatus().then ((data: any) => {
      console.log("Status XML Data -> ", JSON.stringify(data));
      this.parseXML(data).then((status)=>
      {  console.log("Status ->", JSON.stringify(status));
        // If current mode is Movie then check recording status
        if (status == '1') {
          this.dashcamService.videoRecording = true;
        } else {
          this.dashcamService.videoRecording = false;
        }

        if (status == '0') {
          this.dashcamService.videoRecording = true;
          this.dashcamService.recordVideo(1);
        }

        // If current mode is playback then switch to Movie
        if (status == '3' ) {
          let loading = this.loadingCtrl.create({
            content: 'Restarting Video...'
          });
          loading.present();

          this.liveFeedUrl = "";
          this.dashcamService.changeCameraMode('VIDEO')
          .then ((data) => {
            this.liveFeedUrl = this.dashcamService.liveFeedUrl;   
            this.dashcamService.videoRecording = true;
            this.dashcamService.recordVideo(1);
            loading.dismiss(); 
          }).catch (error => console.log("Camera Mode Change Error / ", error));
        }
      });
    }).catch ((error) => console.log("Status Error / ", error))

    this.dashcamService.changeFeedMode(this.cameraMode);
    this.liveFeedUrl = this.dashcamService.liveFeedUrl;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VideoFeedPage');
    this.platform.ready().then(() => {
      this.platform.resume.subscribe((e) => {
        console.log("Resuming in foreground. Restart video.")
        this.initialize();
      });

      this.platform.pause.subscribe((e) => {
        console.log("Going in background. Change to playback mode")
        this.dashcamService.changeCameraMode('PLAYBACK')
      });

    });

  }

  goToSettings(){
  
    // this is old code

   this.dashcamService.videoEntry = 'settings';
   this.dashcamService.recordVideo(0).then(() => {    // stop recording
      this.dashcamService.videoRecording = false;
      this.dashcamService.liveFeedToggle('stop').then(() => {        
        console.log("Live View stopped so call Settings page");
        this.navCtrl.push(SettingsPage);
      }).catch((err) => {
        console.log("Live View STOP error", JSON.stringify(err))
      })
    }).catch((err) => {
      console.log("Recording STOP error", JSON.stringify(err))
    })


  // it own
    console.log("Call Settings Page");
    //this.dashcamService.videoEntry = 'settings'
    //this.navCtrl.push(SettingsPage);
    

  }

  goToCameraMode() {
    
    if (this.cameraMode == 3) {
      this.cameraMode = 0
    } else {
      this.cameraMode = this.cameraMode + 1;
    } 

    switch (this.cameraMode) {
      case 0:
        this.cameraModeText = 'Front'
        break;
      case 1:
        this.cameraModeText = 'Back'
        break;
      case 2:
        this.cameraModeText = 'Front / Back'
        break;
      case 3:
        this.cameraModeText = 'Back / Front'
        break;
      default:
        break;
    }
    this.dashcamService.changeFeedMode(this.cameraMode);
  }

  goToLibrary(){
    
    console.log("Call Library Page");
    this.dashcamService.videoEntry = 'library'
    this.navCtrl.push(LibraryPage);
  }

  goToTakePicture() {
    
    // Show the loading indicator
    this.dashcamService.videoEntry = 'picture';
    let loading = this.loadingCtrl.create({
      content: 'Taking Picture...'
    });
    loading.present().then((result) => {
      this.loaderIsPresent = true;
    });

    // Call the Picture API
    this.dashcamService.takePicture(this.cameraMode).then((data) => {
      loading.setContent("Saving File...")
      this.liveFeedUrl = "" ;      
      this.dashcamService.changeCameraMode('VIDEO')
      .then ((data) => {
        this.liveFeedUrl = this.dashcamService.liveFeedUrl;    
        
        // console.log("Camera Mode Changed to VIDEO sucessfully- ", JSON.stringify(data));
        if (this.dashcamService.videoRecording) {
          this.dashcamService.recordVideo(1).then((data) => {
            if (this.loaderIsPresent) {
              loading.dismiss();
              this.loaderIsPresent = false;
            }
          });
        } else {
          if (this.loaderIsPresent) {
            loading.dismiss();
            this.loaderIsPresent = false;
          }
        }    
      }).catch (error => console.log("Camera Mode VIDEO Change Error / ", error));
    })
  }

  goToFullScreen() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
  }

  goToRemoveFullScreen(){
    // Change Orientation
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
 
  }

  gotToNormalScreen() {
    this.screenOrientation.unlock();
  }

  goToToggleRecording() {
    if (this.dashcamService.videoRecording) {
      this.dashcamService.recordVideo(0);   // Stop Recording
      this.dashcamService.videoRecording = false;
    } else {
      this.dashcamService.recordVideo(1);   // Start Recording
      this.dashcamService.videoRecording = true;
    }
  }

  goToToggleAudio() {
    
    if (this.muteAudio) {
      this.dashcamService.muteAudio(1);   // UnMute
      this.muteAudio = false;
    } else {
      this.dashcamService.muteAudio(0);  // Mute
      this.muteAudio = true;
    }
  }



  parseXML(data)
  {
     return new Promise(resolve =>
     {
        // var k,l,
        var 
            // arr    = [],
            parser = new xml2js.Parser(
            {
               trim: true,
               explicitArray: true
            });

        parser.parseString(data, function (err, result)
        {
          var obj = result.Function;
          var val = obj.Value[0];
          console.log("Object -> ", JSON.stringify(obj));
          console.log("value ->", obj.Value[0]);
          resolve(val);
        });
     });
  }

    // Parse CML data for Files and convert into an Array
    parseSettingsXML(data)
    {
       return new Promise(resolve =>
       {
          // var k,l,
          var
          arr    = [],
          parser = new xml2js.Parser(
          {
              trim: true,
              explicitArray: true
          });
  
          parser.parseString(data, function (err, result)
          {
            var obj = result.Function;
            // console.log("XML Object -> ", JSON.stringify(obj));
  
            arr.push({
              cmd           : obj.Cmd[0],
              status        : obj.Status[0],
            });
      
            arr.push({
              cmd           : obj.Cmd[3],
              status        : obj.Status[3],
            });
  
            arr.push({
              cmd           : obj.Cmd[13],
              status        : obj.Status[13],
            });
  
            arr.push({
              cmd           : obj.Cmd[15],
              status        : obj.Status[15],
            });
            // console.log("Array ", JSON.stringify(arr));
            resolve(arr);
          });
              
       });
    }

}

  