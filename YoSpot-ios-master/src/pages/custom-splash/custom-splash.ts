import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CustomSplashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-custom-splash',
  templateUrl: 'custom-splash.html',
})
export class CustomSplashPage {
  videoUrl: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CustomSplashPage');
    this.timeout(6000).then (() => {
      this.playVideo();     
    })
  }

  playVideo(){
    this.videoUrl = true;
    
    this.timeout(1000).then (() => {
      this.navCtrl.setRoot(HomePage);
    })
  }

  timeout(delay) {
    return new Promise(function(resolve, reject) {
        setTimeout(resolve, delay);
    });
  }
}
