import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FullscreenPicturePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-fullscreen-picture',
  templateUrl: 'fullscreen-picture.html',
})
export class FullscreenPicturePage {
  picturePath: any;
  fileName: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.picturePath = this.navParams.get('picturepath');
    this.fileName = this.navParams.get('filename');
    // this.fileName = '2018_200_12_233.jpg';
    // this.picturePath = '/assets/icon/icon.png';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FullscreenPicturePage');
  }

  cancel() {
    this.navCtrl.pop();
  }

}
