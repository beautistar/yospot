interface Navigator {
    wifiwizard2: WifiWizard2
}

interface WifiWizard2 {

    formatWifiConfig(
        SSID: string,
        password: string,
        algorithm: string
    ): wifiConfig;

    formatWPAConfig(
        SSID: string,
        password: string,
    ): void;

    formatWifiString(
        ssid: string
    ): void;

    
    addNetwork(
        wifi: object,
        win: () => void,
        fail: () => void
    ): void;

    androidConnectNetwork (
        wifi: string,
        win: () => void,
        fail: () => void
    ): void;

    androidConnectNetworkAsync(
        ssid: string
    ): void;

    connect(
        ssid: string,
        bind: boolean,
        password: string,
        algorithm: string,
        isHiddenSSID: boolean
    ): void;

    addNetworkAsync(
        wifi: object,
    ): void;
    
    add(
        wifi: object,
    ): void;

    removeNetwork(
        SSID: string,
        win: () => void,
        fail: () => void
    )

    // connectNetwork(
    //     SSID: string,
    //     win: () => void,
    //     fail: () => void
    // ): void;

    connectNetwork(
        SSID: string,
        bindAll: boolean,
        password: string,
        algorithm: string
    ): void;

    getConnectedSSID(
        win: () => void,
        fail: () => void
    ): void;

    requestPermission (
    ):void

    iOSConnectNetwork(
        ssid: string,
        ssidPassword: string,
        win: () => void,
        fail: () => void
    ): void;

    iOSConnectNetworkAsync(
        ssid: string,
        ssidPassword: string,
    ): void;


    getConnectedSSID(
        win: () => void,
        fail: () => void
    ): void;

    getConnectedSSIDAsync(): void;

    disconnectNetwork(
        SSID: string,
        win: () => void,
        fail: () => void
    ): void;

    listNetworks(
        win: () => void,
        fail: () => void
    ): void;

    getCurrentSSID(
        win: () => void,
        fail: () => void
    ): void;

    setWifiEnabled(
        enabled: boolean,
        win: () => void,
        fail: () => void
    ): void;

    scan(
        win: () => void,
        fail: () => void
    ): void;
}

interface wifiConfig {

}