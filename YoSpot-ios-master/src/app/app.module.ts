import { CustomSplashPage } from './../pages/custom-splash/custom-splash';
import { FullscreenPicturePage } from './../pages/fullscreen-picture/fullscreen-picture';
import { LibraryPage } from './../pages/library/library';
import { SettingsPage } from './../pages/settings/settings';
import { VideoFeedPage } from './../pages/video-feed/video-feed';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
// import { StreamingMedia } from '@ionic-native/streaming-media';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { HttpClientModule } from '@angular/common/http'; 
import { ScreenOrientation } from '@ionic-native/screen-orientation';

import { UtilityService } from '../providers/utility-service';
import { HttpService } from '../providers/http-service';
import { DashcamService } from '../providers/dashcam-service';
import { HTTP } from '@ionic-native/http';
import { IonicStorageModule } from '@ionic/storage';
import { IonicSwipeAllModule } from 'ionic-swipe-all';
import { BackgroundMode } from '@ionic-native/background-mode';
import { PhotoLibrary } from '@ionic-native/photo-library';
import {File} from '@ionic-native/file';
// import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
// import { Geolocation } from '@ionic-native/geolocation';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    VideoFeedPage,
    SettingsPage,
    FullscreenPicturePage,
    CustomSplashPage,
    LibraryPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    IonicSwipeAllModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    VideoFeedPage,
    SettingsPage,
    FullscreenPicturePage,
    CustomSplashPage,
    LibraryPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    // StreamingMedia,
    InAppBrowser,
    ScreenOrientation,
    UtilityService,
    HttpService,
    HTTP,
    BarcodeScanner, 
    BackgroundMode,
    DashcamService,
    PhotoLibrary,
    File,
    FileTransfer,
    FileTransferObject, 
    //Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
